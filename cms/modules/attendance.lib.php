<?php
if(!defined('__PRAGYAN_CMS'))
{ 
	header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
	echo "<h1>403 Forbidden<h1><h4>You are not authorized to access the page.</h4>";
	echo '<hr/>'.$_SERVER['SERVER_SIGNATURE'];
	exit(1);
}
/**
 * @package pragyan
 * @copyright (c) 2008 Pragyan Team
 * @license http://www.gnu.org/licenses/ GNU Public License
 * For more details, see README
 */
global $pageId;
$basefolder=dirname($_SERVER[SCRIPT_NAME]);

class attendance implements module {
	private $userId;
	private $moduleComponentId;
	private $action;
	public function getHtml($gotuid, $gotmoduleComponentId, $gotaction) {

		$this->userId = $gotuid;
		$this->moduleComponentId = $gotmoduleComponentId;
		$this->action = $gotaction;
		$basefolder=dirname($_SERVER[SCRIPT_NAME]);
		$footer.="<script src='$basefolder/cms/modules/smarttable/js/jquery.dataTables.min.js'></script>";
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/modules/smarttable/css/demo_table_jui.css' />";		
//		$footer.="<link  rel='stylesheet' href='$basefolder/cms/$moduleFolder/smarttable/css/jquery-ui-1.10.3.custom.min.css' />";				
		$footer.="<link  rel='stylesheet' href='$basefolder/cms/modules/smarttable/themes/redmond/jquery-ui-1.10.3.custom.min.css' />";				
		$footer.="
		<script>$(function(){
			var tables=[];
		$('.datatable').each(function(){
		var oTable=$(this).dataTable({
			\"sPaginationType\":\"full_numbers\",
			\"bJQueryUI\": true,
			\"fnDestroy\":'true',
			\"iDisplayLength\":100
			});
		$('td', oTable.fnGetNodes()).hover( function() {
		var iCol = $('td').index(this) % 5;
		var nTrs = oTable.fnGetNodes();
		$('td:nth-child('+(iCol+1)+')', nTrs).addClass( 'highlighted' );
		}, function() {
		$('td.highlighted', oTable.fnGetNodes()).removeClass('highlighted');
		} );
		tables.push(oTable);
		});
		//console.log(tables[0]);
		
		$.each(tables,function(idx,object){
		
		});

		});
		</script>";
		
		if ($this->action == "view")
			return $this->actionView().$footer;
		if ($this->action == "edit")
			return $this->actionEdit().$footer;
		if ($this->action == "add")
			return $this->actionAdd().$footer;
		if ($this->action == "attendance")
			return $this->actionAttendance().$footer;
		if ($this->action == "change")
			return $this->actionChange().$footer;
/*		
		if ($this->action == "delete")
			return $this->deleteModule($this->moduleComponentId);
		if ($this->action == "copy")
			return $this->copyModule($this->moduleComponentId);
		if ($this->action == "score")
			return $this->actionScore($this->moduleComponentId);
		if ($this->action == "qaosadmin")
			return $this->actionQaosadmin();
*/
	}
/**
*  attendance_students  
*  id 	page 	name 	type 	join	
*
*
*
*/
	public function getIdFromRegno($regno){
		$q="SELECT id from attendance_students WHERE `regno` = '$regno'";
		$r=mysql_query($q);
		$row=mysql_fetch_assoc($r);
		return $row['id'];
	}
	public function actionChange(){
		global $pageId;
		$html="<br /><hr/>Change Attendance of Students<hr/>";
		$basefolder=dirname($_SERVER[SCRIPT_NAME]);
		global $moduleFolder;
		if(isset($_POST['change'])){
			$val=$_POST['checked'];
			$id=$_POST['id'];
			$date=$_POST['date'];
			$session=$_POST['session'];
			if($val=="true")$query="INSERT INTO attendance_table(page,id,att_date,session,comment) VALUES('$pageId','$id','$date','$session','')";
			else $query="DELETE FROM attendance_table WHERE id='$id' AND page = '$pageId' AND att_date = '$date' AND session='$session'";
			mysql_query($query);
			if(mysql_error())$html="<div id='error'>".mysql_error()."</div>";
			return $html;
		}

		if(!isset($_COOKIE['month']))$_COOKIE['month']=date('m');
		$mon=$_COOKIE['month'];
		if(!isset($_COOKIE['year']))$_COOKIE['year']=date('Y');
		$yr=$_COOKIE['year'];
		if(!isset($_COOKIE['session']))$_COOKIE['session']=1;
		$session=$_COOKIE['session'];
		$monthselected[$mon]="selected";
		$yearselected[$yr]="selected";
		$sessselected[$session]="selected";
		$no_days=cal_days_in_month(CAL_JULIAN,$mon,$yr);
		$html.="

		Month:<select class='group' name='month'>
		<option $monthselected[01] value = '01'>January</option>
		<option $monthselected[02] value = '02'>February</option>
		<option $monthselected[03] value = '03'>March</option>
		<option $monthselected[04] value = '04'>April</option>
		<option $monthselected[05] value = '05'>May</option>
		<option $monthselected[06] value = '06'>June</option>
		<option $monthselected[07] value = '07'>July</option>
		<option $monthselected[08] value = '08'>August</option>
		<option $monthselected[09] value = '09'>September</option>
		<option $monthselected[10] value = '10'>October</option>
		<option $monthselected[11] value = '11'>November</option>
		<option $monthselected[12] value = '12'>December</option> 
		</select>
		Year:<select class='group' name='year'>
		<option $yearselected[2010] value = '2010'>2010</option>
		<option $yearselected[2011] value = '2011'>2011</option>
		<option $yearselected[2012] value = '2012'>2012</option>
		<option $yearselected[2013] value = '2013'>2013</option>
		</select>
		Session:<select class='group' name='session'>
		<option $sessselected[1] value = '1'>Morning</option>
		<option $sessselected[2] value = '2'>Evening</option>
		</select>
		<br />	
		<br />
		Monday<input type='checkbox' class='daysselect' val='Monday' checked>
		Tuesday<input type='checkbox' class='daysselect' val='Tuesday' checked>
		Wednesday<input type='checkbox' class='daysselect' val='Wednesday' checked>
		Thursday<input type='checkbox' class='daysselect' val='Thursday' checked>
		Friday<input type='checkbox' class='daysselect' val='Friday' checked>
		Saturday<input type='checkbox' class='daysselect' val='Saturday' checked>
		Sunday<input type='checkbox' class='daysselect' val='Sunday' checked>
		<br />
		<br />
		<div id='printcontent'>
		<h2>Month ".date('F', mktime(0, 0, 0, $mon, 1, 2000))." of Year $yr</h2>
		<style>
		.Saturday,.tdSaturday{background:rgba(200,200,150,.7);}
		.Sunday,.tdSunday{background:rgba(200,150,200,.7);}
		.printtable{border:1px solid black;padding:0px;}
		.printtable td,.printtable th{text-align:center;}
		.dayheader{width:15px;}
		.regnoheader{width:25px;}
		.nameheader{width:200px;}
		.printtable .tdname{text-align:left;padding-left:20px;}
		input[type=checkbox].checkbox{height:12px!important;width:12px!important;}
		</style>

		<script type='text/javascript' src='$basefolder/cms/$moduleFolder/tournament/jquery.cookie.js'></script>
		<script>
		$(function(){
			$('.group').change(function(){
				$.cookie($(this).attr('name'),$(this).attr('value'));
				window.location=window.location;
			});
			$('.daysselect').change(function(){
				$('.dayheader.'+$(this).attr('val')+',.td'+$(this).attr('val')).toggle();
			});
			$('.checkbox').change(function(){
				var val=$(this).is(':checked');
				var regid=$(this).attr('data-id');
				var date=$(this).attr('data-date');
				var session=$(this).attr('data-session');
				var parent=$(this).closest('td');
				$.ajax({
					url:'./+change',
					type:'post',
					data:{
						change:'1',
						checked:val,
						regid:regid,
						date:date,
						session:session
					},
					success:function(msg){
						//console.log(msg);
						if($('#error',msg).html())alert('ERROR:  '+$('#error',msg).html());
						else {
							var bg=$(parent).css('background');
							$(parent).css('background','green');
							 setTimeout(function(){
							 	$(parent).css('background',bg);},1000);
							}
					},
					error:function(){
						alert('Sorry there was an error');
					}
				});
			});
		});
		</script>
		";

$q="SELECT * FROM attendance_students ORDER BY regno";
		$res=mysql_query($q);
		echo $q;
$qq="SELECT * FROM attendance_table WHERE `att_date` BETWEEN '$yr-$mon-1' AND '$yr-$mon-$no_days'AND page = '$pageId' ";//" AND `session` = '$session'";
$rr=mysql_query($qq);
$att=array();

while($rrr=mysql_fetch_assoc($rr))
$att[$rrr['id']][$rrr['att_date']."_"."$rrr[session]"]=1;


$tablearr=array(1+mysql_num_rows($res));
		$html.="<table border='1px solid black' cellspacing='0' class='datatable printtable'><thead><tr><th class='regnoheader'>Reg No</th><th class='nameheader'>Name</th>";
		$tablearr[0]=array();
		$tablearr[0][]="Reg.No";
		$tablearr[0][]="Name";
		for($i=1;$i<=$no_days;$i++)
		{
			$html.="<th class='dayheader ".date("l",mktime(0,0,0,$mon,$i,$yr))."'>$i</th>";
			$tablearr[0][]="$i";
		}
		$html.="</tr></thead><tbody>";
$tablerow=1;	
		while($row=mysql_fetch_assoc($res)){
			$tablearr[$tablerow][]=$row[id];
			$tablearr[$tablerow][]=$row[name];
		$html.="<tr><td class='tdregno'>$row[id]</td><td class='tdname'>$row[name]</td>";		
		for($j=1;$j<=$no_days;$j++){
		$yes=($att[$row[id]]["$yr-$mon-".($j>9?$j:"0$j")."_".$session]);
		$chk="<input type='checkbox' ".($yes?"checked":"")." class='dontprint blankcheckbox checkbox' data-id='$row[id]' data-date='"."$yr-$mon-".($j>9?$j:"0$j")."' data-session='$session' >";		
		$html.="<td class='td".date("l",mktime(0,0,0,$mon,$j,$yr))."' >".$chk."</td>";
			$tablearr[$tablerow][]=($yes?"x":" ");
		}
		$html.="</tr>";
		$tablerow++;
		}
		//print_r($tablearr);
		$_SESSION['changetable']=$tablearr;
		//print_r($_SESSION['changetable']);
		$html.="</tbody></table>

		<a class='push_button' id='printtoexcel'>Download</a>
		<script>
		$('#printtoexcel').click(function(){
		window.location='./+view&tabletoexcel=changetable';
		});
		</script>
		</div>";
		return $html;
	}
	public function actionAttendance(){
		global $pageId;
		$html="<br /><hr/>Attendance of Students<hr/>";
		if(isset($_POST['att'])){
					
					$regnos=$_POST['num'];
					$date=$_POST['date'];
					$session=$_POST['session'];
					$comment=$_POST['comment'];
					$regids=array();
					foreach ($regnos as $r) {
						$regids[]=$this->getIdFromRegno($r);
					}
/*
					foreach($regnos as $r){
					$query="INSERT INTO `attendance_table`(id,att_date,session,comment) VALUES ('".$this->getIdFromRegno($r)."','$date','$session','$comment')";
					
					//('$r','$date','$session','$comment')";
*/
					$query="SELECT numbers FROM `attendance_table` WHERE `att_date` = '$date'AND page = '$pageId'  AND `session` = '$session' ";
					$res=mysql_query($query);

					if(mysql_num_rows($res)>0){
						while($row=mysql_fetch_assoc($res))
						$oldnum=$row['numbers'];
					
					$num=explode(",",$oldnum);
					//print_r($num);

					foreach($regids as $r){
							$flag=1;
							foreach($num as $nn)if($nn==$r){$flag=0;break;}
							if($flag)$oldnum.=",".$r;
						}
						$query="UPDATE `attendance_table` SET `numbers` = '$oldnum' WHERE `att_date` = '$date' AND `session` = '$session' AND page = '$pageId' ";
					}else{
						$numbers="";
						foreach($regids as $r)$numbers.=$r.",";
						$query="INSERT INTO `attendance_table`(page,att_date,session,comment,numbers) VALUES('$pageId','$date','$session','','$numbers')";
					}

					mysql_query($query);
					if(mysql_error())
						$html.="<div id='error'>".$query.mysql_error()."</div>";
//					}
					return $html;
				}
		$html.="
		<div id='printcontent'>
		<style>.daybefore{border:1px solid grey;margin-left:30px;cursor:pointer;}</style>
		<script>
		$(function(){
			$('.daybefore').click(function(){
				$('.daybefore').each(function(){
					$(this).css('background','none');});
				$(this).css('background','rgba(156,9,9,.8)');
				var todayTimeStamp = +new Date; 
			var oneDayTimeStamp = 1000 * 60 * 60 * 24; 
			var minus=parseInt($(this).attr('val'));
			var diff = todayTimeStamp - minus*oneDayTimeStamp;
			var yesterdayDate = new Date(diff);
			var yesterdayString = yesterdayDate.getFullYear() + '-' + (yesterdayDate.getMonth() + 1>9?'':'0')+(yesterdayDate.getMonth() + 1) + '-' + (yesterdayDate.getDate() + 1>9?'':'0') +yesterdayDate.getDate();
			$('#attendance_date').attr('value',yesterdayString);
			//alert($('#attendance_date').attr('value'));
			console.log(yesterdayString);
			});
			$('#attendance').click(function(){
				var date=$('#attendance_date').attr('value');
				var nos=$('#regnos').val();
				var session=$('#session').val();
				var comment=$('#comment').val();
				nos=nos.replace('<br />',',');nos=nos.replace('<br>',',');
				nos=nos.replace(/\\n/g,\",\");
				nos=nos.replace(/ /g,',')
				var num=nos.split(',');
				//console.log(num);
				num=$.map(num,function(el){return el !== '' ? el : null;});
				if(date==''){alert('PLease Select the date or enter the date in the input');return false;}
				if(num.length==0){alert('PLease Enter the Register no\'s in the area');$('#regnos').focus();return false;}
				if(num.length>0)
				$.ajax({
					url:'./+attendance',
					type:'post',
					data:{
						att:'1',
						date:date,
						num:num,
						session:session,
						comment:comment
					},
					success:function(msg){
						console.log(msg);
						if($('#error',msg).html())alert('ERROR:  '+$('#error',msg).html());
						else $('table').after('Successfully added '+name);
					},
					error:function(){
						alert('Sorry there was an error');
					}
				})
			});
		});
		</script>
		<input type='date' id='attendance_date'>
		<span id='daybefore3' val='3' class='daybefore'>3 Days ago</span>
		<span id='daybefore2' val='2' class='daybefore'>Day Before Yesterday</span>
		<span id='daybefore1' val='1' class='daybefore'>Yesterday</span>
		<span id='daybefore' val='0' class='daybefore'>Today</span><br/>
		<input type='text' value='1' id='session'><input type='text' placeholder='comments' value='' id='comment'>
		<textarea id='regnos' style='background:white;width:100%;height:200px;font-size:20px' placeholder='ENTER REG numbers separated by comma HERE'></textarea>
		<button id='attendance'>Add</button>
		";
		$html.="<br /><br /><br /><br /><table class='datatable'>
		<thead><tr><th>Reg.No</th><th>Name</th><th>Gender</th><th>DOB</th><th>Type</th><th>Father Cell</th><th>Mother Cell</th><th>Home</th><th>Join Date</th></tr></thead>
		<tbody>
		";
		$query="SELECT * FROM `attendance_students`";
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res)){
$t=<<<ABC
	<tr data-regno='$row[regno]' data-regid='$row[id]' class='$row[regno] $row[name] $row[gender] dob_$row[dob] join_$row[joindate]' >
	<td><input type='text' value='$row[regno]' class='newinp regno' /></td>
	<td><input type='text' value='$row[name]' class='newinp name' /></td>
	<td><input type='text' value='$row[gender]' class='newinp gender' /></td>
	<td><input type='date' value='$row[dob]' class='newinp dob' /></td>
	<td><input type='text' value='$row[type]' class='newinp type' /></td>
	<td><input type='number' value='$row[phone1]' class='newinp phone1' /></td>
	<td><input type='number' value='$row[phone2]' class='newinp phone2' /></td>
	<td><input type='number' value='$row[phone3]' class='newinp phone3' /></td>
	<td><input type='date' value='$row[joindate]' class='newinp joindate' /></td>
	</tr>
ABC;
		$html.=$t;	
		}
		$html.="</tbody></table></div>";
		return $html;
	}
	function outputCSV($data) {
		$basefolder=dirname($_SERVER[SCRIPT_NAME]);
			global $moduleFolder;
    $outstream = fopen("attendance/upload/temp.csv", "w");
    function __outputCSV(&$vals, $key, $filehandler) {
        fputcsv($filehandler, $vals); // add parameters if you want
    }
    array_walk($data, "__outputCSV", $outstream);
    fclose($outstream);
	}
	public function actionView(){
		global $pageId;
			$html="<br /><hr/>Attendance of Students<hr/>";
			$basefolder=dirname($_SERVER[SCRIPT_NAME]);
			global $moduleFolder;
			if(isset($_GET['tabletoexcel'])){
				$cook=$_GET['tabletoexcel'];
				//print_r($_SESSION);
				$arr=$_SESSION["$cook"];
				//print_r($arr.$cook);
				$this->outputCSV($arr);
				return "Has it downloaded ?";
			}
			if(!isset($_COOKIE['month']))$_COOKIE['month']=date('m');
			$mon=$_COOKIE['month'];
			if(!isset($_COOKIE['year']))$_COOKIE['year']=date('Y');
			$yr=$_COOKIE['year'];
			if(!isset($_COOKIE['session']))$_COOKIE['session']=1;
			$session=$_COOKIE['session'];
			$monthselected[$mon]="selected";
			$yearselected[$yr]="selected";
			$sessselected[$session]="selected";
			$no_days=cal_days_in_month(CAL_JULIAN,$mon,$yr);
			$html.="
			Month:<select class='group' name='month'>
			<option $monthselected[01] value = '01'>January</option>
			<option $monthselected[02] value = '02'>February</option>
			<option $monthselected[03] value = '03'>March</option>
			<option $monthselected[04] value = '04'>April</option>
			<option $monthselected[05] value = '05'>May</option>
			<option $monthselected[06] value = '06'>June</option>
			<option $monthselected[07] value = '07'>July</option>
			<option $monthselected[08] value = '08'>August</option>
			<option $monthselected[09] value = '09'>September</option>
			<option $monthselected[10] value = '10'>October</option>
			<option $monthselected[11] value = '11'>November</option>
			<option $monthselected[12] value = '12'>December</option> 
			</select>
			Year:<select class='group' name='year'>
			<option $yearselected[2010] value = '2010'>2010</option>
			<option $yearselected[2011] value = '2011'>2011</option>
			<option $yearselected[2012] value = '2012'>2012</option>
			<option $yearselected[2013] value = '2013'>2013</option>
			</select>
			Session:<select class='group' name='session'>
			<option $sessselected[1] value = '1'>Morning</option>
			<option $sessselected[2] value = '2'>Evening</option>
			</select>
			<br />	
			<br />
			Monday<input type='checkbox' class='daysselect' val='Monday' checked>
			Tuesday<input type='checkbox' class='daysselect' val='Tuesday' checked>
			Wednesday<input type='checkbox' class='daysselect' val='Wednesday' checked>
			Thursday<input type='checkbox' class='daysselect' val='Thursday' checked>
			Friday<input type='checkbox' class='daysselect' val='Friday' checked>
			Saturday<input type='checkbox' class='daysselect' val='Saturday' checked>
			Sunday<input type='checkbox' class='daysselect' val='Sunday' checked>
			<br />
			<br />
			<div id='printcontent'>
			<h2>Month ".date('F', mktime(0, 0, 0, $mon, 1, 2000))." of Year $yr</h2>
			<style>
			.Saturday,.tdSaturday{background:rgba(200,200,150,.7);}
			.Sunday,.tdSunday{background:rgba(200,150,200,.7);}
			.printtable{border:1px solid black;padding:0px;}
			.printtable td,.printtable th{text-align:center;}
			.dayheader{width:15px;}
			.regnoheader{width:25px;}
			.nameheader{width:200px;}
			.printtable .tdname{text-align:left;padding-left:20px;}
			input[type=checkbox].checkbox{height:12px!important;width:12px!important;}
			</style>

			<script type='text/javascript' src='$basefolder/cms/$moduleFolder/tournament/jquery.cookie.js'></script>
			<script>
			$(function(){
				$('.group').change(function(){
					$.cookie($(this).attr('name'),$(this).attr('value'));
					window.location=window.location;
				});
				$('.daysselect').change(function(){
					$('.dayheader.'+$(this).attr('val')+',.td'+$(this).attr('val')).toggle();
				});
			});
			</script>
			";

	$q="SELECT * FROM attendance_students WHERE `page` = '$pageId' ORDER BY regno";
			$res=mysql_query($q);
			
			$html.="<table border='1px solid black' cellspacing='0' class='datatable printtable'><thead><tr><th class='regnoheader'>Reg No</th><th class='nameheader'>Name</th>";
			for($i=1;$i<=$no_days;$i++)
			$html.="<th class='dayheader ".date("l",mktime(0,0,0,$mon,$i,$yr))."'>$i</th>";
			$html.="</tr></thead><tbody>";

	$qq="SELECT * FROM attendance_table WHERE `att_date` BETWEEN '$yr-$mon-1' AND '$yr-$mon-$no_days' AND page = '$pageId' ";//AND `session` = '$session'";
	$rr=mysql_query($qq);

	$att=array();
	$numbers=array();
	if(mysql_num_rows($rr)>0)
	while($rrr=mysql_fetch_assoc($rr)){
	$nos=explode(",",$rrr['numbers']);
	//print_r($nos);
	foreach($nos as $nn)
	if($nn!="")
	$numbers[$rrr['att_date']."_"."$rrr[session]"][$nn]=1;
	//$att[$rrr['id']][$rrr['att_date']."_"."$rrr[session]"]=1;
	
	}
	print_r($numbers);
			while($row=mysql_fetch_assoc($res)){
			$html.="<tr><td class='tdregno'>$row[regno]</td><td class='tdname'>$row[name]</td>";		
			for($j=1;$j<=$no_days;$j++){
			$yes1=($numbers["$yr-$mon-".($j>9?$j:"0$j")."_1"][$row[id]]);
			$yes2=($numbers["$yr-$mon-".($j>9?$j:"0$j")."_2"][$row[id]]);
			//$yes=($att[$row[id]]["$yr-$mon-".($j>9?$j:"0$j")."_".$session]);
			if($yes1=="1")$chk="<span>\</span>";
			if($yes2=="1")$chk="<span>/</span>";
			if($yes1=="1" && $yes2=="1")$chk="<span>x</span>";
			if($yes1!="1" && $yes2!="1")$chk="<span> </span>";
			if(date("Y-m-d")<"$yr-$mon-".($j>9?$j:"0$j"))$chk="<span> </span>";
			$html.="<td class='td".date("l",mktime(0,0,0,$mon,$j,$yr))."' >".$chk."</td>";
			}
			$html.="</tr>";
			}
			$html.="</tbody></table></div>";
			return $html;
	}
	public function actionEdit(){
		global $pageId;
		$html="<br /><hr/>Edit Students<hr/>";
		if(isset($_POST['edit'])){
			$id=$_POST['regid'];
			$regno=$_POST['regno'];
			$name=$_POST['name'];
			$gender=$_POST['gender'];
			$dob=$_POST['dob'];
			$phone1=$_POST['phone1'];
			$phone2=$_POST['phone2'];
			$phone3=$_POST['phone3'];
			$joindate=$_POST['joindate'];
			$type=$_POST['type'];
$query="UPDATE `attendance_students` SET page = '$pageId' , regno = '$regno' , name = '$name', gender = '$gender', dob = '$dob', phone1 = '$phone1', phone2 = '$phone2' , phone3 = '$phone3', type = '$type', joindate = '$joindate' WHERE `id` = '$id' ";
			mysql_query($query);
			if(mysql_error())return "<div id='error'>".$query.mysql_error()."</div>";
		}
		$html.=<<<SCRIPT
		<script>
		$(function(){
			$('.newinp').change(function(){
				var row=$(this).closest('tr');
				//console.log($(row).attr('data-regno'));
				var regno=$(row).attr('data-regno');
				var regid=$(row).attr('data-regid');
				var name=$(row).find('.newinp.name').val();
				var gender=$(row).find('.newinp.gender').val();
				var dob=$(row).find('.newinp.dob').val();
				var phone1=$(row).find('.newinp.phone1').val();
				var phone2=$(row).find('.newinp.phone2').val();
				var phone3=$(row).find('.newinp.phone3').val();
				var joindate=$(row).find('.newinp.joindate').val();
				var type=$(row).find('.newinp.type').val();
				console.log(regid);
				$.ajax({
					url:'./+edit',
					type:'post',
					data:{
						edit:'1',
					    regid:regid,
						regno:regno,
						name:name,
						gender:gender,
						dob:dob,
						phone1:phone1,phone2:phone2,phone3:phone3,
						joindate:joindate,
						type:type
					},
					success:function(msg){
						if($('#error',msg).html())alert('ERROR:  '+$('#error',msg).html());
						else $('table').after('Successfully added '+name);
					},
					error:function(){
						alert('Sorry there was an error');
					}
				});
			});
		});
		</script>
SCRIPT;
		$html.="<table class='datatable'>
		<thead><tr><th>Reg.No</th><th>Name</th><th>Gender</th><th>DOB</th><th>Type</th><th>Father Cell</th><th>Mother Cell</th><th>Home</th><th>Join Date</th></tr></thead>
		<tbody>
		";
		$query="SELECT * FROM `attendance_students`";
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res)){
$t=<<<ABC
	<tr data-regno='$row[regno]' data-regid='$row[id]' class='$row[regno] $row[name] $row[gender] dob_$row[dob] join_$row[joindate]' >
	<td><input type='text' value='$row[regno]' class='newinp regno' /></td>
	<td><input type='text' value='$row[name]' class='newinp name' /></td>
	<td><input type='text' value='$row[gender]' class='newinp gender' /></td>
	<td><input type='date' value='$row[dob]' class='newinp dob' /></td>
	<td><input type='text' value='$row[type]' class='newinp type' /></td>
	<td><input type='number' value='$row[phone1]' class='newinp phone1' /></td>
	<td><input type='number' value='$row[phone2]' class='newinp phone2' /></td>
	<td><input type='number' value='$row[phone3]' class='newinp phone3' /></td>
	<td><input type='date' value='$row[joindate]' class='newinp joindate' /></td>
	</tr>
ABC;
		$html.=$t;	
		}
		$html.="</tbody></table>";
		return $html;
	}
	public function actionAdd(){
		global $pageId,$moduleFolder,$basefolder;
		$basefolder=dirname($_SERVER[SCRIPT_NAME]);
		//echo $pageId;
		$html="<br /><hr/>Add New Student<hr/>";
		if(isset($_FILES["file"]["name"])){

			$allowedExts = array("csv");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if (in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
    /*if (file_exists("$basefolder/cms/$moduleFolder/upload/" . $_FILES["file"]["name"]))
      {
      echo $_FILES["file"]["name"] . " already exists. ";
      }
    else*/
      {

      //move_uploaded_file($_FILES["file"]["tmp_name"],"$basefolder/cms/$moduleFolder/attendance/upload/" . $_FILES["file"]["name"]);
		$row = 1;
		//echo "$basefolder/cms/$moduleFolder/attendance/upload/" . $_FILES["file"]["name"];
		if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $num = count($data);
		        $html.= "<p> $num fields in line $row: <br /></p>\n";
		        $row++;
		        if($row>2)
		        {	for ($c=0; $c < $num; $c++) {
		            	$html.= $data[$c] . "<br />\n";
		        	}
		        	
		        	$regno=$data[1];
					$name=$data[2];
					$joindate=$data[3];
					$gender=$data[4];
					$dob=$data[5];
					$phone1=$data[6];
					$phone2=$data[7];
					$phone3=$data[8];
					$type=$_POST['type'];
					$query="INSERT INTO `attendance_students`(page,regno,name,gender,dob,phone1,phone2,phone3,type,joindate) 			VALUES('$pageId','$regno','$name','$gender','$dob','$phone1','$phone2','$phone3','$type','$joindate')";
					mysql_query($query);
					//$html.=$query;
					if(mysql_error())return "<div id='error'>".$query.mysql_error()."</div>";
					
		    	}
		    }
		    fclose($handle);      

		    return $html;
      }
    }
  }
}
else
  {
  echo "Invalid file";
  }
		}
		else if(isset($_POST['add'])){
			$regno=$_POST['regno'];
			$name=$_POST['name'];
			$gender=$_POST['gender'];
			$dob=$_POST['dob'];
			$phone1=$_POST['phone1'];
			$phone2=$_POST['phone2'];
			$phone3=$_POST['phone3'];
			$joindate=$_POST['joindate'];
			$type=$_POST['type'];
			$query="INSERT INTO `attendance_students`(page,regno,name,gender,dob,phone1,phone2,phone3,type,joindate) 			VALUES('$pageId','$regno','$name','$gender','$dob','$phone1','$phone2','$phone3','$type','$joindate')";
			mysql_query($query);
			if(mysql_error())return "<div id='error'>".$query.mysql_error()."</div>";
		}
		$html.=<<<HTML
		<script>
		$(function(){
			$("#addit").click(function(){
			var regno=$(".newinp.regno").val();
			var name=$(".newinp.name").val();
			var gender=$(".newinp.gender:checked").val();
			var dob=$(".newinp.dob").val();
			var phone1=$(".newinp.phone1").val();
			var phone2=$(".newinp.phone2").val();
			var phone3=$(".newinp.phone3").val();
			var joindate=$(".newinp.joindate").val();
			var type=$(".newinp.type").val();
			//alert(name+gender+dob+phone1+phone2+phone3+joindate+type);
			if(name!='' && dob!='' )
			$.ajax({
				url:"./+add",
				type:"post",
				data:{
					add:'1',
					regno:regno,
					name:name,
					gender:gender,
					dob:dob,
					phone1:phone1,phone2:phone2,phone3:phone3,
					joindate:joindate,
					type:type
				},
				success:function(msg){
					if($('#error',msg).html())alert('ERROR:  '+$('#error',msg).html());
					else $('table').after('Successfully added '+name);
				},
				error:function(){
					alert('Sorry there was an error');
				}
			});
			});
		});
		</script>
		<table>
		<tr><td><label>Reg.No.</label></td><td><input required type='text' name='' class='newinp regno' /></td><tr>		
		<tr><td><label>Name</label></td><td><input required type='text' name='' class='newinp name' /></td><tr>
		<tr><td><label>Gender</label></td><td><label for='maleradio'>Male</label><input required id='maleradio' type='radio' value='male' name='gender' class='newinp gender' /><label for='femaleradio'>Female</label><input id='femaleradio' type='radio' value='female' name='gender' class='newinp gender' /></td><tr>
		<tr><td><label>D O B</label></td><td><input required type='date' name='' class='newinp dob' /></td><tr>
		<tr><td><label>Father Cell</label></td><td><input type='number' name='' class='newinp phone1' /></td><tr>
		<tr><td><label>Mother Cell</label></td><td><input type='number' name='' class='newinp phone2' /></td><tr>
		<tr><td><label>Home</label></td><td><input type='number' name='' class='newinp phone3' /></td><tr>
		<tr><td><label>Join Date</label></td><td><input type='date' name='' class='newinp joindate' /></td><tr>
		<tr><td><label>Type</label></td><td><input type='text' value='adjustable' name='' class='newinp type' /></td><tr>
		<tr><td></td><td><button class='push_button' id='addit' value='ADD'>ADD</button></td><tr>
		</table>
		<br />
		OR 
		<br />
		<form action="./+add" method="post"
		enctype="multipart/form-data">
		<table>
		<tr><td colspan=2><a href='$basefolder/modules/attendance/sample.csv'>Download &amp; edit this</a></td></tr>
		<tr><td>
		<label for="file">Upload:</label></td><td>
		<input type="file" name="file" id="file"></tr>
		<tr><td>Type of skater</td><td><select name='type'>
		<option value='adjustable'>Adjustable</option>
		<option value='shoe'>Shoe</option>
		</select></td></tr>
		<tr><td></td><td><input type="submit" name="submit" value="Submit"></td></tr>
		</table>
		</form>

HTML;
		return $html;
	}
	public function createModule($moduleComponentId){
		return true;
	}
	public function deleteModule($moduleComponentId){
		return true;
	}
	public function copyModule($moduleComponentId,$newId){
		return true;
	}
}
