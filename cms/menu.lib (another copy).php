<?php
/**
 * @package pragyan
 * @copyright (c) 2008 Pragyan Team
 * @license http://www.gnu.org/licenses/ GNU Public License
 * For more details, see README
 */

/*
 * DONE : Add a parent sibling menu which will be displayed in place of child menu if there are no children (or no
 *	permissions to view children)
 * DONE : dont show anything in child menu in case the user has permission to 0 child pages.
 * In its  place show parent sibling menu.
 *
 */

function findMenuIndex($menuArray, $pageId) {
	for ($i = 0; $i < count($menuArray); ++$i)
		if ($menuArray[$i][0] == $pageId)
			return $i;
	return -1;
}

function getMenu($userId, $pageIdArray, $linkPrefix = '') {
	$pageId = $pageIdArray[count($pageIdArray) - 1];
	$pageRow = getPageInfo($pageId);

	if ($pageRow['page_displaymenu'] == 0) return '';

	$menuHtml = '<div id="menubar">';

if (count($pageIdArray) > 1)
	$parentRow = getPageInfo($pageIdArray[count($pageIdArray) - 2]);
else
	$parentRow = getPageInfo(0);

$menuHtml .= '<h2><a href="' . ($parentRow['page_id'] == $pageId ? './' : '../') . $linkPrefix . '">' . $parentRow['page_title'] . '</a></h2>';

	$childMenuItems = getChildren($pageId, $userId);

	if ($pageId == 0) {
		// home page: return just the child menu
		$menuHtml .= htmlMenuRenderer($childMenuItems, -1, $linkPrefix);
	}
	else if (count($childMenuItems) == 0) {
		array_pop($pageIdArray);
		/* if ($pageRow['page_displaysiblingmenu']) {
		
			$siblingMenuItems = getChildren($pageIdArray[count($pageIdArray) - 1], $userId);
			$menuHtml .= htmlMenuRenderer($siblingMenuItems, findMenuIndex($siblingMenuItems, $pageId), '../' . $linkPrefix);
		}
		*/

		return getMenu($userId, $pageIdArray, '../' . $linkPrefix);
	}
	else {
		$childMenuHtml = htmlMenuRenderer($childMenuItems, -1, $linkPrefix);
		if ($pageRow['page_displaysiblingmenu']) {
			$siblingMenuItems = getChildren($pageIdArray[count($pageIdArray) - 2], $userId);
			$menuHtml .= accordionMenuRenderer($siblingMenuItems, findMenuIndex($siblingMenuItems, $pageId), $childMenuHtml, '../' . $linkPrefix);
		}
		else
			$menuHtml .= $childMenuHtml;
	}
	$menuHtml .= '</div>';

	return $menuHtml;
}


/*
function getMenu($userId, $pageIdArray) {
	$pageId = $pageIdArray[count($pageIdArray) - 1];
	$pageRow = getPageInfo($pageId);

	if ($pageRow['page_displaymenu'] == 0)
		return '';

	$menuHtml =<<<MENUHTML
		<div id="menubar">
			<!-- <div id="menubarcontent"> -->
MENUHTML;

	$childMenu = getChildren($pageId, $userId);

	if ($pageId == 0) {
		$menuHtml .= '<h2><a href="./">' . $pageRow['page_title'] . '</a></h2>';
		$menuHtml .= htmlMenuRenderer($childMenu);
	}
	else if (count($childMenu) == 0) {
		if ($pageRow['page_displaysiblingmenu']) {
			$siblingMenu = getChildren($pageIdArray[count($pageIdArray) - 2], $userId);
			$parentPageRow = getPageInfo($pageIdArray[count($pageIdArray) - 2]);
			$menuHtml .= '<h2><a href="../">' . $parentPageRow['page_title'] . '</a></h2>';
			$menuHtml .= htmlMenuRenderer($siblingMenu, findMenuIndex($siblingMenu, $pageId), '../');
		}
	}
	else {
		if ($pageRow['page_displaysiblingmenu']) {
			$siblingMenu = getChildren($pageIdArray[count($pageIdArray) - 2], $userId);
			$parentPageRow = getPageInfo($pageIdArray[count($pageIdArray) - 2]);
			$menuHtml .= '<h2><a href="../">' . $parentPageRow['page_title'] . '</a></h2>';
			$menuHtml .= htmlMenuRenderer($siblingMenu, findMenuIndex($siblingMenu, $pageId), '../');
		}

		$menuHtml .= '<h2><a href="./">' . $pageRow['page_title'] . '</a></h2>';
		$menuHtml .= htmlMenuRenderer($childMenu);
	}

	// $menuHtml .= '</div></div>';
	$menuHtml .= '</div>';

	return $menuHtml;
}

*/

function htmlMenuRenderer($menuArray, $currentIndex = -1, $linkPrefix = '') {
	$menuHtml = '<ul>';
	for ($i = 0; $i < count($menuArray); ++$i) {
		$menuHtml .= "<li";
		if ($i == $currentIndex) 
			$menuHtml .= ' class="currentpage"';
		$menuHtml .= "><a href=\"{$linkPrefix}{$menuArray[$i][1]}\"> {$menuArray[$i][2]} </a></li>\n";
	}
	$menuHtml .= "</ul>";

	return $menuHtml;
}


/*
 * Generates a list out of menuArray, with childMenuHtml added under the given parentIndex.
 */
function accordionMenuRenderer($menuArray, $parentIndex, $childMenuHtml, $linkPrefix = '') {
	$menuHtml = '<ul class="sideMenu">';
	for ($i = 0; $i < count($menuArray); ++$i) {
		$menuHtml .= "<li";
		if ($i == $parentIndex)
			$menuHtml .= ' class="currentpage"';
		$menuHtml .= "><a href=\"{$linkPrefix}{$menuArray[$i][1]}\">{$menuArray[$i][2]}</a>";
		if ($i == $parentIndex)
			$menuHtml .= $childMenuHtml;
		$menuHtml .= "</li>\n";
	}
	$menuHtml .= "</ul>\n";

	return $menuHtml;
}

function imageMenuRenderer($menuArray, $currentIndex = -1, $linkPrefix = '') {
	$menuRows = array();
	$rowCount = -1;
	for ($i = 0; $i < count($menuArray); ++$i) {
		if ($i % 3 == 0) {
			if ($rowCount >= 0)
				$menuRows[$rowCount] .= '</div>';
			$menuRows[++$rowCount] = '<div class="menuitemrow">';
		}
		$menuRows[$rowCount] .= '<a href="' . $linkPrefix . $menuArray[$i][1] . '"><img src="' . $menuArray[$i][4] . '" alt="' . $menuArray[$i][2] . '"';
		if ($i == $currentIndex)
			$menuRows[$rowCount] .= ' class="currentpage"';
		$menuRows[$rowCount] .= ' /></a>';
	}

	if (count($menuRows))
		$menuRows[count($menuRows) - 1] .= "</div>";

	$menuHtml = '';
	for ($i = 0; $i < count($menuRows); ++$i) {
		if ($i % 3 == 0)
			$menuHtml .= '<div class="menuitemdescription">&nbsp;</div>';
		$menuHtml .= $menuRows[$i];
	}

	return $menuHtml;
}

/**
 * @return array Array of arrays of page id, page name, page title, large image and small image
 */
function getChildren($pageId, $userId) {
	$childrenQuery = 'SELECT `page_id`, `page_name`, `page_title`, `page_module`, `page_modulecomponentid`, `page_displayinmenu` FROM `' . MYSQL_DATABASE_PREFIX . 'pages` WHERE `page_parentid` = ' . $pageId . ' AND `page_id` != ' . $pageId . ' AND `page_displayinmenu` = 1 ORDER BY `page_menurank`';
	$childrenResult = mysql_query($childrenQuery);
	$children = array();
	while ($childrenRow = mysql_fetch_assoc($childrenResult))
		if ($childrenRow['page_displayinmenu'] == true && getPermissions($userId, $childrenRow['page_id'], 'view', $childrenRow['page_module']) == true)
			$children[] = array($childrenRow['page_id'], $childrenRow['page_name'], $childrenRow['page_title']);
	return $children;
}

?>
