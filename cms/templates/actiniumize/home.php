<?php get_header(); ?>

<div id="primary">
	<div id="content" role="main">


		<?php add_filter('excerpt_length', 'padd_theme_hook_excerpt_loop_length'); ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('loop', 'index'); ?>
		<?php endwhile; ?>
		<?php remove_filter('excerpt_length', 'padd_theme_hook_excerpt_loop_length'); ?>
		<div class="clear"></div>
		<?php Padd_PageNavigation::render(); ?>

	</div><!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
