
<div id="secondary" class="sidebar">
	<?php
		$args = array(
			'before_title' => '<h2 class="widget-title"><span>',
			'after_title'  => '</span></h2>'
		);
		the_widget('WP_Widget_Search' , array('title' => 'Search'), $args);
		if (!dynamic_sidebar('sidebar')) {
			the_widget('Padd_Theme_Widget_WPP'            , null, $args);
			the_widget('Padd_Theme_Widget_Ads'         , null, $args);
		}
	?>
</div>