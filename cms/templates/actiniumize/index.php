<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" dir="ltr" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" dir="ltr" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" dir="ltr" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html dir="ltr" lang="en-US">
<!--<![endif]-->
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width"/>
<title>Actiniumize | Just another Awesome WordPress site</title>
<script type="text/javascript">
//<![CDATA[
window.__CF=window.__CF||{};window.__CF.AJS={"ga_key":{"ua":"UA-2309554-11","ga_bs":"2"},"abetterbrowser":{"ie":"8"}};
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) { var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",mirage:{responsive:0,lazy:0},mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/abv=3242305479/"},atok:"9afbf5c3609342560a903c994629ec68",zone:"paddsolutions.com",rocket:"0",apps:{"ga_key":{"ua":"UA-2309554-11","ga_bs":"2"},"abetterbrowser":{"ie":"8"}}}];var a=document.createElement("script"),b=document.getElementsByTagName("script")[0];a.async=!0;a.src="//ajax.cloudflare.com/cdn-cgi/nexp/abv=1573736665/cloudflare.min.js";b.parentNode.insertBefore(a,b);}}catch(e){};
//]]>
</script>
<link rel="profile" href="http://gmpg.org/xfn/11"/>
<link rel="stylesheet" type="text/css" media="all" href="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/themes/actiniumize/style.css"/>
<link rel="pingback" href="http://www.paddsolutions.com/wpmag/actiniumize/xmlrpc.php"/>
<meta name='robots' content='noindex,nofollow'/>
<link rel="alternate" type="application/rss+xml" title="Actiniumize &raquo; Feed" href="http://www.paddsolutions.com/wpmag/actiniumize/feed/"/>
<link rel="alternate" type="application/rss+xml" title="Actiniumize &raquo; Comments Feed" href="http://www.paddsolutions.com/wpmag/actiniumize/comments/feed/"/>
<link rel='stylesheet' id='google-fonts-1-css' href='http://fonts.googleapis.com/css?family=Podkova&#038;ver=3.4' type='text/css' media='all'/>
<script type='text/javascript' src='http://www.paddsolutions.com/wpmag/actiniumize/wp-includes/js/jquery/jquery.js?ver=1.7.2'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.paddsolutions.com/wpmag/actiniumize/xmlrpc.php?rsd"/>
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.paddsolutions.com/wpmag/actiniumize/wp-includes/wlwmanifest.xml"/>
<style type="text/css">.recentcomments a{display:inline!important;padding:0!important;margin:0!important;}</style>
 
<link rel="stylesheet" href="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/plugins/wordpress-popular-posts/style/wpp.css" type="text/css" media="screen"/>
 
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-2309554-11']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

(function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
/* ]]> */
</script>
</head>
<body class="home blog front-page"><script type="text/javascript">
//<![CDATA[
try{(function(a){var b="http://",c="www.paddsolutions.com",d="/cdn-cgi/cl/",e="09e91d183fd38850db6f7531d99aa1439ad7e103.gif",f=new a;f.src=[b,c,d,e].join("")})(Image)}catch(e){}
//]]>
</script>
<div id="container" class="hfeed">
<div id="header-wrap">
<div id="header">
<div id="branding">
<h1 id="site-title"><a href="http://www.paddsolutions.com/wpmag/actiniumize/" title="Actiniumize" rel="home">Actiniumize</a></h1>
<h2 id="site-description">Just another Awesome WordPress site</h2>
</div> 
<div class="widget-area">
<div class="widget widget_socialnet">
<ul class="socialnet">
<li class="facebook">
<a href="http://www.facebook.com/paddsolutions" class="icon-facebook" title="Facebook">Add me on Facebook</a>
</li>
<li class="twitter">
<a href="http://www.twitter.com/paddsolutions" class="icon-twitter" title="Twitter">Follow me on Twitter</a>
</li>
<li class="linkedin">
<a href="paddsolutions" class="icon-linkedin" title="LinkedIn">Add me on LinkedIn</a>
</li>
<li class="flickr">
<a href="http://www.flickr.com/photos/paddsolutions" class="icon-flickr" title="Flickr">Add me on Flickr</a>
</li>
<li class="rss">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/feed/" class="icon-rss" title="RSS Feed">Subscribe in RSS</a>
</li>
</ul>
</div> 
<div id="menubar">
<ul id="menu-main-menu" class="menu"><li id="menu-item-327" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-327"><a href="http://www.paddsolutions.com/wpmag/actiniumize/">Home</a></li>
<li id="menu-item-317" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-317"><a href="http://www.paddsolutions.com/wpmag/actiniumize/about-2/">About</a>
<ul class="sub-menu">
<li id="menu-item-318" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-318"><a href="http://www.paddsolutions.com/wpmag/actiniumize/about-2/child-page/">Child Page</a></li>
</ul>
</li>
<li id="menu-item-323" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-323"><a href="http://www.paddsolutions.com/wpmag/actiniumize/services/">Services</a>
<ul class="sub-menu">
<li id="menu-item-320" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-320"><a href="http://www.paddsolutions.com/wpmag/actiniumize/services/desktop-services/">Desktop Services</a></li>
<li id="menu-item-325" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-325"><a href="http://www.paddsolutions.com/wpmag/actiniumize/services/web-services/">Web Services</a>
<ul class="sub-menu">
<li id="menu-item-322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322"><a href="http://www.paddsolutions.com/wpmag/actiniumize/services/web-services/search-engine-optimization/">SEO and Marketing</a></li>
<li id="menu-item-326" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-326"><a href="http://www.paddsolutions.com/wpmag/actiniumize/services/web-services/web-development/">Website Development</a></li>
<li id="menu-item-324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-324"><a href="http://www.paddsolutions.com/wpmag/actiniumize/services/web-services/soap-and-rpc/">SOAP and RPC</a></li>
</ul>
</li>
<li id="menu-item-321" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-321"><a href="http://www.paddsolutions.com/wpmag/actiniumize/services/photography-services/">Photography Services</a></li>
</ul>
</li>
<li id="menu-item-319" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-319"><a href="http://www.paddsolutions.com/wpmag/actiniumize/contact/">Contact</a></li>
</ul> </div> 
</div>
<div class="clear"></div>
</div> 
</div> 
<div id="intro-wrap">
<div id="intro">
<div id="slideshow-box">
<div class="list">
<div class="item">
<div class="image">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/scelerisque-scelerisque-amet-sit-augue/" title="Permanent Link to Scelerisque scelerisque amet sit augue?">
<img width="950" height="300" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/artroll-950x300.jpg" class="attachment-actiniumize-slideshow wp-post-image" alt="Colorful stripes" title="Amet ac. Urna risus augue, nunc placerat nunc rhoncus! Amet pid aliquet diam! Turpis sit, dapibus in facilisis nascetur? In mid, magna augue eu odio.&hellip;"/> </a>
</div>
</div>
<div class="item">
<div class="image">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/globetrotting-in-style/" title="Permanent Link to Globetrotting in Style">
<img width="950" height="300" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/luxuryliving-950x300.jpg" class="attachment-actiniumize-slideshow wp-post-image" alt="Where do you want to go next?" title="Etiam augue nunc? Ut natoque ridiculus egestas, vel ut nisi, proin odio rhoncus scelerisque est magna, mid, sagittis cursus penatibus vel facilisis scelerisque elementum et&hellip;"/> </a>
</div>
</div>
<div class="item">
<div class="image">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/lost-in-big-apple/" title="Permanent Link to Lost in Big Apple">
<img width="950" height="300" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/New_York_City-950x300.jpg" class="attachment-actiniumize-slideshow wp-post-image" alt="New_York_City" title="Risus nascetur? Cursus sed, in? Sed! Eu mattis montes tempor tristique, odio odio? Proin vel pulvinar ac nisi! Purus! Ut. Augue, augue nascetur lacus. Elit,&hellip;"/> </a>
</div>
</div>
</div>
</div>
</div> 
</div> 
<div id="home-top-wrap">
<div id="home-top">
<div class="bar bar-1">
<div class="widget widget_special widget_special_1-page">
<h3 class="title" style="background-image: url('http://www.paddsolutions.com/wpmag/actiniumize/wp-content/themes/actiniumize/images/thumbnail-column-1.png'); padding-left: 42px; "><a href="http://www.paddsolutions.com/wpmag/actiniumize/about-2/">About</a></h3>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.&hellip;</p>
</div>
</div>
<div class="bar bar-2">
<div class="widget widget_special widget_special_1-page">
<h3 class="title" style="background-image: url('http://www.paddsolutions.com/wpmag/actiniumize/wp-content/themes/actiniumize/images/thumbnail-column-2.png'); padding-left: 42px; "><a href="http://www.paddsolutions.com/wpmag/actiniumize/elements/">Elements</a></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.&hellip;</p>
</div>
</div>
<div class="bar bar-3">
<div class="widget widget_special widget_special_1-page">
<h3 class="title" style="background-image: url('http://www.paddsolutions.com/wpmag/actiniumize/wp-content/themes/actiniumize/images/thumbnail-column-3.png'); padding-left: 42px; "><a href="http://www.paddsolutions.com/wpmag/actiniumize/analytics/">Analytics</a></h3>
<p>Vivamus et enim sapien, ut pulvinar libero. Aenean&hellip;</p>
</div>
</div>
<div class="bar bar-4">
<div class="widget widget_special widget_special_1-page">
<h3 class="title" style="background-image: url('http://www.paddsolutions.com/wpmag/actiniumize/wp-content/themes/actiniumize/images/thumbnail-column-4.png'); padding-left: 42px; "><a href="http://www.paddsolutions.com/wpmag/actiniumize/contact/">Contact</a></h3>
<p>Sed metus lectus, vestibulum tincidunt fermentum eu, egestas&hellip;</p>
</div>
</div>
<div class="clear"></div>
</div>  
</div>  
<div id="main-wrap">
<div id="main">
<div id="primary-full">
<div id="content" role="main" class="clear-fix">
<div class="page-header">
<h2 class="page-title">Featured</h2>
</div>
<ul id="featured-projects">
<li id="entry-305">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/scelerisque-scelerisque-amet-sit-augue/" rel="bookmark" title="Permanent Link to Scelerisque scelerisque amet sit augue?">
<img width="150" height="130" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/artroll-150x130.jpg" class="attachment-actiniumize-term wp-post-image" alt="Colorful stripes" title="artroll"/> </a>
</li>
<li id="entry-314">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/globetrotting-in-style/" rel="bookmark" title="Permanent Link to Globetrotting in Style">
<img width="150" height="130" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/luxuryliving-150x130.jpg" class="attachment-actiniumize-term wp-post-image" alt="Where do you want to go next?" title="luxuryliving"/> </a>
</li>
<li id="entry-310">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/lost-in-big-apple/" rel="bookmark" title="Permanent Link to Lost in Big Apple">
<img width="150" height="130" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/New_York_City-150x130.jpg" class="attachment-actiniumize-term wp-post-image" alt="New_York_City" title="New_York_City"/> </a>
</li>
<li id="entry-294">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/2010/09/10/sit-sociis-elementum-cursus-ut/" rel="bookmark" title="Permanent Link to Sit Sociis Elementum Cursus Ut?">
<img width="150" height="130" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/09/goodvibrations-150x130.jpg" class="attachment-actiniumize-term wp-post-image" alt="goodvibrations" title="goodvibrations"/> </a>
</li>
<li id="entry-291">
<a href="http://www.paddsolutions.com/wpmag/actiniumize/2010/09/10/sit-amet-mid-magnis-elit-hac-cum/" rel="bookmark" title="Permanent Link to Sit Amet Mid Magnis Elit Hac Cum">
<img width="150" height="130" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/09/cutebaby-150x130.jpg" class="attachment-actiniumize-term wp-post-image" alt="cutebaby" title="cutebaby"/> </a>
</li>
</ul>
</div> 
</div> 
</div> 
</div> 
<div id="fwa-wrap">
<div id="fwa">
<div id="fwa-1" class="fwa-bar">
<div class="widget widget_links"><h2 class="widget-title">Blogroll</h2>
<ul class='xoxo blogroll'>
<li><a href="http://codex.wordpress.org/">Documentation</a></li>
<li><a href="http://wordpress.org/extend/plugins/">Plugins</a></li>
<li><a href="http://wordpress.org/extend/ideas/">Suggest Ideas</a></li>
<li><a href="http://wordpress.org/support/">Support Forum</a></li>
<li><a href="http://wordpress.org/extend/themes/">Themes</a></li>
<li><a href="http://wordpress.org/news/">WordPress Blog</a></li>
<li><a href="http://planet.wordpress.org/">WordPress Planet</a></li>
</ul>
</div>
</div> 
<div id="fwa-2" class="fwa-bar">
<div class="widget widget_socialnet">
<h2 class="widget-title">Connect</h2>
<ul class="socialnet">
<li class="twitter">
<a href="http://www.twitter.com/paddsolutions" class="icon-twitter" title="Twitter">Follow me on Twitter</a>
</li>
<li class="dribbble">
<a href="http://dribbble.com/paddsolutions" class="icon-dribbble" title="Dribbble">Follow me on Dribbble</a>
</li>
<li class="google+">
<a href="paddsolutions" class="icon-google+" title="Google+">Add me on Google+</a>
</li>
<li class="facebook">
<a href="http://www.facebook.com/paddsolutions" class="icon-facebook" title="Facebook">Add me on Facebook</a>
</li>
<li class="linkedin">
<a href="paddsolutions" class="icon-linkedin" title="LinkedIn">Add me on LinkedIn</a>
</li>
</ul>
</div>
</div> 
<div id="fwa-3" class="fwa-bar">
<div class="widget widget_recent_entries"> <h2 class="widget-title">Latest Posts</h2> <ul class="recent-posts">
<li>
<a class="title" href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/scelerisque-scelerisque-amet-sit-augue/" title="Scelerisque scelerisque amet sit augue?"><img width="120" height="104" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/artroll-120x104.jpg" class="attachment-actiniumize-recent wp-post-image" alt="Colorful stripes" title="artroll"/></a>
</li>
<li>
<a class="title" href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/globetrotting-in-style/" title="Globetrotting in Style"><img width="120" height="104" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/luxuryliving-120x104.jpg" class="attachment-actiniumize-recent wp-post-image" alt="Where do you want to go next?" title="luxuryliving"/></a>
</li>
<li>
<a class="title" href="http://www.paddsolutions.com/wpmag/actiniumize/2010/12/06/lost-in-big-apple/" title="Lost in Big Apple"><img width="120" height="104" src="http://www.paddsolutions.com/wpmag/actiniumize/wp-content/uploads/2010/12/New_York_City-120x104.jpg" class="attachment-actiniumize-recent wp-post-image" alt="New_York_City" title="New_York_City"/></a>
</li>
</ul>
</div> </div> 
<div class="clear"></div>
</div> 
</div> 
<div id="footer-wrap">
<div id="footer" class="clear-fix">
<p class="designers"><a target="_blank" title="SUV" href="http://suv.reviewitonline.net/">SUV</a>, <a target="_blank" title="Toyota SUV" href="http://suv.reviewitonline.net/toyota-suv/">Toyota SUV</a>, <a target="_blank" title="Ford SUV" href="http://suv.reviewitonline.net/ford-suv/">Ford SUV</a>, and <a target="_blank" title="Best SUVs" href="http://suv.reviewitonline.net/best-suvs/">Best SUVs</a>. </p><p class="copyright">Copyright &copy; 2013. Actiniumize. All rights reserved.</p> </div> 
</div> 
</div> 
<script type='text/javascript' src='http://www.paddsolutions.com/wpmag/actiniumize/wp-content/themes/actiniumize/js/frontend.php?ver=1.0'></script>
</body>
</html>
 