<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width" />
<?php
	global $page, $paged;
	$title  = '';
	$title .= wp_title('|', false, 'right');
	$title .= get_bloginfo('name');
	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) {
		$title .= ' | ' . $site_description;
	}
	if ($paged >= 2 || $page >= 2) {
		$title .= ' | ' . sprintf(__('Page %s', 'makintab'), max($paged, $page));
	}
?>
<title><?php echo $title; ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php
	if (is_singular() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
	wp_head();
?>
</head>
<?php
	$classes = array();
	if (is_singular() || is_404()) {
		$classes[] = 'singular';
	}
	if (is_front_page()) {
		$classes[] = 'front-page';
	}
?>
<body <?php body_class($classes); ?>>
<div id="container" class="hfeed">

	<div id="header-wrap">
		<div id="header">
			<div id="branding">
				<h1 id="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
				<h2 id="site-description"><?php bloginfo('description'); ?></h2>
			</div><!-- #branding -->

			<div class="widget-area">
				<div class="widget widget_socialnet">
					<?php padd_theme_widget_socialnet('B'); ?>
				</div><!-- .widget widget_search -->
				<div id="menubar">
					<?php
						wp_nav_menu(array(
							'theme_location' => 'primary',
							'container'      => false,
						));
					?>
				</div><!-- #menubar -->
			</div>

			<div class="clear"></div>
		</div><!-- #header -->
	</div><!-- #header-wrap -->

	<?php $slideshow = Padd_Option::get('slideshow_enable') == '1' ? true : false; ?>
	<?php if (is_front_page() && $slideshow) : ?>
	<div id="intro-wrap">
		<div id="intro">
			<?php padd_theme_post_slideshow(); ?>
		</div><!-- #intro -->
	</div><!-- #intro -->
	<?php endif; ?>

	<?php if (is_front_page()) : ?>
	<div id="home-top-wrap">
		<div id="home-top">
			<div class="bar bar-1">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(__('Home 1', PADD_THEME_SLUG))) :
					padd_theme_pagebox_special(Padd_Option::get('post_1_page_id',1),'1-page', 1);
				endif;
			?>
			</div>
			<div class="bar bar-2">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(__('Home 2', PADD_THEME_SLUG))) :
					padd_theme_pagebox_special(Padd_Option::get('post_2_page_id',1),'1-page', 2);
				endif;
			?>
			</div>
			<div class="bar bar-3">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(__('Home 3', PADD_THEME_SLUG))) :
					padd_theme_pagebox_special(Padd_Option::get('post_3_page_id',1),'1-page', 3);
				endif;
			?>
			</div>
			<div class="bar bar-4">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(__('Home 4', PADD_THEME_SLUG))) :
					padd_theme_pagebox_special(Padd_Option::get('post_4_page_id',1),'1-page', 4);
				endif;
			?>
			</div>
			<div class="clear"></div>

		</div> <!-- #hometop -->
	</div> <!-- #hometop-wrap -->
	<?php endif; ?>

	<div id="main-wrap">
		<div id="main">