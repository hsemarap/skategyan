<?php

/**
 * Checks if the value of the Facebook field is URL or username.
 *
 * @param <type> $string
 * @return boolean
 */
function padd_theme_check_facebook_url($string) {
	$url = false;
	if ('http://' == substr($string, 0, 7)) {
		$url = true;
	}
	return $url;
}

/**
 * Trim the title.
 *
 * @param int $length Number of letters.
 * @param string $after Ellipsis by default.
 */
function padd_theme_trim_title($length, $after='...') {
	$title = get_the_title();
	if (strlen($title) > $length) {
		$title = substr($title, 0, $length);
		echo $title . $after;
	} else {
		echo $title;
	}
}

function padd_theme_trim($text, $length, $after='...') {
	if (strlen($text) > $length) {
		$title = substr($title, 0, $length);
		echo $text . $after;
	} else {
		echo $text;
	}
}

/**
 * Renders the list of social networking websites.
 */
function padd_theme_widget_socialnet($position='A') {
	global $_PADD_SOCIALNET, $_PADD_SOCIALNET_B;
	$array = ($position == 'A') ? $_PADD_SOCIALNET : $_PADD_SOCIALNET_B;
?>
	<ul class="socialnet">
	<?php foreach ($array as $k => $v) : ?>
		<?php
			$url = '';
			$v->username = Padd_Option::get('sn_username_' . $k);
			if ('facebook' == $k) {
				$fburl = Padd_Option::get('sn_username_facebook');
				if (!padd_theme_check_facebook_url($fburl) && !empty($fburl)) {
					$v->username = $fburl;
					$url = (string) $v;
				} else if (!empty($fburl)) {
					$url = $fburl;
				}
			} else if ('rss' == $k) {
				if (empty($v->username)) {
					$url = get_bloginfo('rss2_url');
				} else {
					$url = (string) $v;
				}
			} else {
				$url = (string) $v;
			}
		?>
		<?php if (!empty($url)) : ?>
		<li class="<?php echo $k; ?>">
			<a href="<?php echo $url; ?>" class="icon-<?php echo $k; ?>" title="<?php echo $v->network; ?>"><?php echo $v->short_desc; ?></a>
		</li>
		<?php endif; ?>
	<?php endforeach; ?>
	</ul>
<?php
}

/**
 * Renders the content of the page.
 */
function padd_theme_widget_page($pid) {
	wp_reset_query();
	query_posts('page_id=' . $pid);
	the_post();
	the_content();
}

/**
 * Renders the twitter widget.
 */
function padd_theme_widget_twitter() {
	global $_PADD_SOCIALNET;
	$_PADD_SOCIALNET['twitter']->username = (Padd_Option::get('sn_username_twitter'));
	$padd_sb_twitter = Padd_Option::get('sn_username_twitter');
	$twitter = new Padd_Twitter($padd_sb_twitter, 5, true);
	$twitter->show_tweets();
	?>
	<p class="follow"><a href="'<?php echo $_PADD_SOCIALNET['twitter']; ?>'"><?php echo __('Follow us on Twitter', PADD_THEME_SLUG); ?></a></p>
	<?php
}

/**
 * Renders the banner advertisement
 */
function padd_theme_widget_banner() {
	$ad = Padd_Option::get('ads_728090_1');
	echo stripslashes($ad);
}

/**
 * Renders the advertisements.
 */
function padd_theme_widget_sponsors() {
	echo '<div class="ads-area clear-fix">';
	$ad = Padd_Option::get('ads_300250_1');
	echo stripslashes($ad);
	echo '</div>';
}

function padd_theme_widget_ads_area() {
	echo '<div class="ads-area">';
	for ($i=1;$i<=4;$i++) {
		$ad = Padd_Option::get('ads_125125_' . $i,'');
		echo '<span class="ads ads-',  $i, '">', stripslashes($ad), '</span>';
	}
	echo '<div class="clear"></div>';
	echo '</div>';
}

/**
 * Renders the Facebook Like Box.
 *
 * @paran string $id Facebook numerical ID
 * @param int $w Width of the box
 * @param int $h Height of the box
 * @param int $conn Number of connections
 * @param int $stream News feed streaming. 1 to enable, 0 to disable. The default value is 0.
 * @param int $header Like Box header. 1 to show, 0 to hide. The default value is 0.
 */
function padd_theme_widget_facebook_likebox($w=246,$h=300,$conn=10,$stream=0,$header=0) {
	global $_PADD_SOCIALNET;
	$fburl = Padd_Option::get('sn_username_facebook');
	if (!padd_theme_check_facebook_url($fburl)) {
		$_PADD_SOCIALNET['facebook']->username = $fburl;
		$fburl = (string) $_PADD_SOCIALNET['facebook'];
	}
?>
<iframe src="http://www.facebook.com/plugins/likebox.php?href=<?php echo urlencode($fburl); ?>&amp;width=<?php echo $w; ?>&amp;connections=<?php echo $conn; ?>&amp;stream=<?php echo $stream == 1 ? 'true' : 'false'; ?>&amp;header=<?php echo $header == 1 ? 'true' : 'false'; ?>&amp;height=<?php echo $h; ?>" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:<?php echo $w; ?>px; height:<?php echo $h; ?>px;"></iframe>
<?php
}

/**
 * Renders the list of comments.
 *
 * @param string $comment
 * @param string $args
 * @param string $depth
 */
function padd_theme_single_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
		<div class="comment-box">
			<div class="comment-interior append-clear">
				<div class="comment-author append-clear">
					<div class="comment-avatar"><?php echo get_avatar($comment,'33'); ?></div>
					<div class="comment-meta">
						<span class="author"><?php echo sprintf(__('%s says:', PADD_THEME_SLUG), get_comment_author_link()); ?></span>
						<span class="time"><?php echo get_comment_date(Padd_Option::get('date_format')); ?></span>
					</div>
				</div>
				<div class="comment-details">
					<div class="comment-details-interior">
						<?php comment_text(); ?>
						<?php if ($comment->comment_approved == '0') : ?>
						<p class="comment-notice"><?php _e('My comment is awaiting moderation.', PADD_THEME_SLUG) ?></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="comment-actions clear">
					<?php edit_comment_link(__('Edit', PADD_THEME_SLUG),'<span class="edit">','</span> | ') ?>
					<?php comment_reply_link(array('respond_id' => 'reply', 'add_below' => 'comment' , 'depth' => $depth, 'max_depth' => $args['max_depth'])) ; ?>
				</div>
			</div>
		</div>
	<?php
}

/**
 * Render the list of trackbacks.
 *
 * @param string $comment
 * @param string $args
 * @param string $depth
 */
function padd_theme_single_trackbacks($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="pings-<?php comment_ID() ?>">
		<?php comment_author_link(); ?>
	<?php
}

/**
 * Renders the featured posts in home page.
 */
function padd_theme_post_slideshow($exclude=array()) {
	wp_reset_query();
	$featured = Padd_Option::get('slideshow_cat_id');
	$count = Padd_Option::get('slideshow_cat_limit');
	query_posts('showposts=' . $count . '&cat=' . $featured);
	$padd_image_def = get_template_directory_uri() . '/images/thumbnail-slideshow.png';
	add_filter('excerpt_length', 'padd_theme_hook_slideshow_loop_length');
?>
<div id="slideshow-box">
	<div class="list">
	<?php while (have_posts()) : the_post(); ?>
		<?php
			$custom = get_post_custom();
		?>
		<div class="item">
			<div class="image">
				<a href="<?php the_permalink(); ?>" title="<?php printf(__('Permanent Link to %s', PADD_THEME_SLUG), the_title_attribute('echo=0')); ?>">
				<?php
					$exclude[] = get_the_ID();
					if (has_post_thumbnail()) {
						the_post_thumbnail(PADD_THEME_SLUG . '-slideshow', array('title' => get_the_excerpt()));
					} else {
						echo '<img class="thumbnail" src="' . $padd_image_def . '" />';
					}
				?>
				</a>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
</div>
<?php
	remove_filter('excerpt_length', 'padd_theme_hook_slideshow_loop_length');
	wp_reset_query();
	return $exclude;
}

/**
 * Renders the contact form.
 *
 * @param array $args
 */
function padd_contact_form($args = array()) {
	$req = true;
	$fields =  array(
		'author' => '<p class="contact-form-author">' . '<label for="name">' . __('Name :', PADD_THEME_SLUG) . ' ' . '</label> ' .
		            '<span class="field"><input id="name" name="name" type="text" value="" size="30" /></span></p>',
		'email'  => '<p class="contact-form-email"><label for="email">' . __('Email :', PADD_THEME_SLUG) . '</label> ' .
		            '<span class="field"><input id="email" name="email" type="text" value="" size="30"' . $aria_req . ' /></span></p>',
	);

	$required_text = '';
	$defaults = array(
		'fields'               => $fields,
		'contact_field'        => '<p class="contact-form-contact"><label for="contact">' . _x('Message :', 'noun', PADD_THEME_SLUG) . ' </label><span class="field"><textarea id="message" name="message" cols="45" rows="8" aria-required="true"></textarea></span><span class="clear"></span></p>',
		'contact_notes_before' => '',
		'contact_notes_after'  => '',
		'id_form'              => 'contactform',
		'id_submit'            => 'submit',
		'title_contact'          => __('Message Form', PADD_THEME_SLUG),
		'label_submit'         => __('Submit Message', PADD_THEME_SLUG),
	);

	$args = wp_parse_args($args, apply_filters('themecredible_contact_form_defaults', $defaults));

	?>
	<div class="contact_form">
		<form action="<?php echo get_template_directory_uri() . '/includes/contact-submit.php'; ?>" method="post" id="<?php echo esc_attr($args['id_form']); ?>">
			<?php echo $args['contact_notes_before']; ?>
			<?php
				foreach ((array) $args['fields'] as $name => $field) {
					echo apply_filters("padd_contact_form_field_{$name}", $field) . "\n";
				}
			?>
			<?php echo apply_filters('contact_form_field_message', $args['contact_field']); ?>
			<?php echo $args['contact_notes_after']; ?>
			<p class="form-submit">
				<button name="submit" type="submit" id="<?php echo esc_attr($args['id_submit']); ?>"><span><?php echo esc_attr($args['label_submit']); ?></span></button>
			</p>
		</form>
	</div><!-- #contact -->
	<?php
}


/**
 * Same as the function in <code>wp_dropdown_categories</code> but with multiple selection capability.
 *
 * @param string $args
 * @return type
 */
function padd_dropdown_categories($args = '') {
	$defaults = array(
		'show_option_all' => '', 'show_option_none' => '',
		'orderby' => 'id', 'order' => 'ASC',
		'show_last_update' => 0, 'show_count' => 0,
		'hide_empty' => 1, 'child_of' => 0,
		'exclude' => '', 'echo' => 1,
		'selected' => 0, 'hierarchical' => 0,
		'name' => 'cat', 'id' => '',
		'class' => 'postform', 'depth' => 0,
		'tab_index' => 0, 'taxonomy' => 'category',
		'hide_if_empty' => false, 'multiple' => false
	);

	$defaults['selected'] = (is_category()) ? get_query_var('cat') : 0;

	// Back compat.
	if (isset($args['type']) && 'link' == $args['type']) {
		_deprecated_argument(__FUNCTION__, '3.0', '');
		$args['taxonomy'] = 'link_category';
	}

	$r = wp_parse_args($args, $defaults);

	if (!isset($r['pad_counts']) && $r['show_count'] && $r['hierarchical']) {
		$r['pad_counts'] = true;
	}

	$r['include_last_update_time'] = $r['show_last_update'];
	extract($r);

	$tab_index_attribute = '';
	if ((int) $tab_index > 0)
		$tab_index_attribute = " tabindex=\"$tab_index\"";

	$categories = get_terms($taxonomy, $r);
	$name = esc_attr($name);
	$class = esc_attr($class);
	$id = $id ? esc_attr($id) : $name;

	if (! $r['hide_if_empty'] || ! empty($categories)) {
		if ($r['multiple']) {
			$multiple = "multiple='multiple' size='10'";
			$class = 'postform multiple';
			$name .= '[]';
		} else {
			$multiple = '';
			$class = esc_attr($class);
		}
		$output = "<select name='$name' id='$id' class='$class' $tab_index_attribute $multiple>\n";
	}
	else {
		$output = '';
	}

	if (empty($categories) && ! $r['hide_if_empty'] && !empty($show_option_none)) {
		$show_option_none = apply_filters('list_cats', $show_option_none);
		$output .= "\t<option value='-1' selected='selected'>$show_option_none</option>\n";
	}

	if (! empty($categories)) {

		if ($show_option_all) {
			$show_option_all = apply_filters('list_cats', $show_option_all);
			$selected = ('0' === strval($r['selected'])) ? " selected='selected'" : '';
			$output .= "\t<option value='0'$selected>$show_option_all</option>\n";
		}

		if ($show_option_none) {
			$show_option_none = apply_filters('list_cats', $show_option_none);
			$selected = ('-1' === strval($r['selected'])) ? " selected='selected'" : '';
			$output .= "\t<option value='-1'$selected>$show_option_none</option>\n";
		}

		if ($hierarchical) {
			$depth = $r['depth'];  // Walk the full depth.
		} else {
			$depth = -1; // Flat.
		}

		$output .= padd_walk_category_dropdown_tree($categories, $depth, $r);
	}
	if (! $r['hide_if_empty'] || ! empty($categories))
		$output .= "</select>\n";


	$output = apply_filters('wp_dropdown_cats', $output);

	if ($echo) {
		echo $output;
	}

	return $output;
}

/**
 * Walk the category dropdown tree.
 *
 * @return type
 */
function padd_walk_category_dropdown_tree() {
	$args = func_get_args();
	// the user's options are the third parameter
	if (empty($args[2]['walker']) || !is_a($args[2]['walker'], 'Walker'))
		$walker = new Padd_Walker_CategoryDropdown;
	else
		$walker = $args[2]['walker'];

	return call_user_func_array(array(&$walker, 'walk'), $args);
}

/**
 * Show featured category items.
 *
 * @param int $cat_id Category ID.
 * @param string $position Addition to classname for position of entry (float left or right).
 */
function padd_featured_categories_items($cat_id) {
	global $post;
	$tmp_post = $post;
	$limit = 3;
	$category = get_category($cat_id);
	$fcat_posts = get_posts('numberposts=' . $limit . '&category=' . $cat_id);
	$count = count($fcat_posts);
	$padd_image_def = get_template_directory_uri() . '/images/thumbnail-categories.jpg';
?>
<div id="tab-<?php echo $cat_id; ?>" class="box box-featured-category box-featured-category-<?php echo $cat_id; ?>">
	<h3 class="featured-category-title"><?php echo sprintf(__('%s', PADD_THEME_SLUG), $category->name); ?></h3>
	<?php if ($count > 0) : ?>
	<?php foreach ($fcat_posts as $post) : ?>
		<div class="item">
			<div class="image">
				<a href="<?php echo get_permalink($post->ID); ?>" title="<?php printf(__('Permanent Link to %s', PADD_THEME_SLUG), the_title_attribute('echo=0')); ?>">
				<?php
					$exclude[] = get_the_ID();
					if (has_post_thumbnail()) {
						the_post_thumbnail(PADD_THEME_SLUG . '-term', array('title' => get_the_excerpt()));
					} else {
						echo '<img class="thumbnail" src="' . $padd_image_def . '" />';
					}
				?>
				</a>
			</div>
		</div>
	<?php endforeach; ?>
	<?php endif; ?>
	<div class="clear"></div>
</div>
<?php
	$post = $tmp_post;
}

function padd_get_excerpt_by_id($post_or_id, $excerpt_length = 20, $excerpt_more = '...') {
	if (is_object($post_or_id)) {
		$postObj = $post_or_id;
	} else {
		$postObj = get_post($post_or_id);
	}
	$raw_excerpt = $text = $postObj->post_excerpt;
	if ('' == $text) {
		$text = $postObj->post_content;

		$text = strip_shortcodes($text);

		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		$text = strip_tags($text);

		$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
		if (count($words) > $excerpt_length) {
			array_pop($words);
			$text = implode(' ', $words);
			$text = $text . $excerpt_more;
		} else {
			$text = implode(' ', $words);
		}
	}
	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}

/**
 * Render the popular posts.
 */
function padd_theme_widget_wpp() {
	if (function_exists('get_mostpopular')) :
		get_mostpopular('pages=0&stats_comments=1&range=all&limit=5&thumbnail_selection=usergenerated&thumbnail_width=77&thumbnail_height=67&do_pattern=1&pattern_form={image}{title}');
	elseif (function_exists('wpp_get_mostpopular')) :
		wpp_get_mostpopular('pages=0&stats_comments=1&range=all&limit=5&thumbnail_selection=usergenerated&thumbnail_width=77&thumbnail_height=67&do_pattern=1&pattern_form={image}{title}');
	else :
	?>
		<p>Please install the <a href="http://wordpress.org/extend/plugins/wordpress-popular-posts/">Wordpress Popular Posts plugin</a>.</p>
	<?php
	endif;
}

function padd_recent_comments($limit=5, $w=57) {
	$limit = !empty($limit) ? $limit : 5;
	$comments = get_comments('status=approve&number=' . $limit);
	if ($comments) {
		echo '<ul class="recent-comments">';
		foreach ($comments as $comment) :
			echo '<li>';
			echo '<p class="meta">';
			comment_date(__('F j, Y', PADD_THEME_SLUG), $comment->comment_ID);
			echo ' | ', __('By: ', PADD_THEME_SLUG);
			comment_author($comment->comment_ID);
			echo '</p>';
			echo '<p class="content">';
			comment_excerpt($comment->comment_ID);
			echo '</p>';
			echo '</li>';
		endforeach;
		echo '</ul>';
	} else {
		echo '<p>', __('There are no comments in all entries.', PADD_THEME_SLUG), '</p>';
	}
}

/**
 * Renders the excerpt of the page.
 */
function padd_theme_pagebox_special($pid,$class,$col) {
?>
<div class="widget widget_special widget_special_<?php echo $class; ?>">
	<?php
		wp_reset_query();
		add_filter('excerpt_length','padd_theme_hook_excerpt_pages_length');
		query_posts('page_id=' . $pid);
		the_post();
		$padd_image_def = get_template_directory_uri() . '/images/thumbnail-column-' . $col . '.png';
		$style = '';
		if (has_post_thumbnail()) {
			$image_id = get_post_thumbnail_id();
			$image_url = wp_get_attachment_image_src($image_id);
			$image_url = $image_url[0];
			$sizes = getimagesize($image_url);
			$style = 'style="background-image: url(\'' . $image_url . '\'); padding-left: ' . ($sizes[0] + 10) . 'px; "';
		} else {
			$sizes = getimagesize($padd_image_def);
			$style = 'style="background-image: url(\'' . $padd_image_def . '\'); padding-left: ' . ($sizes[0] + 10) . 'px; "';
		}
	?>
	<h3 class="title" <?php echo $style; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	<p><?php echo wp_trim_excerpt(''); ?></p>
	<?php
		remove_filter('excerpt_length','padd_theme_hook_excerpt_pages_length');
		wp_reset_query();
	?>
</div>
<?php
}


/**
 * Renders the recent posts, optionally with dates.
 *
 * @global WP_DB $wpdb
 * @global string $wp_locale
 * @param array|string $args
 * @return string
 */
function padd_theme_widget_recent_posts($args = '') {
	global $wpdb, $wp_locale;

	$defaults = array(
		'limit' => '3',
		'format' => 'html', 'before' => '',
		'after' => '', 'show_post_count' => false,
		'echo' => 1, 'show_date' => true, 'date_format' => 'F j, Y'
	);

	$r = wp_parse_args($args,$defaults);
	extract($r, EXTR_SKIP);

	if ('' == $type) {
		$type = 'monthly';
	}

	if ('' != $limit) {
		$limit = absint($limit);
		$limit = ' LIMIT ' . $limit;
	}

	$where = apply_filters('getarchives_where',"WHERE post_type = 'post' AND post_status = 'publish'",$r);
	$join = apply_filters('getarchives_join',"", $r);

	$output = '';

	$orderby = "post_date DESC ";
	$query = "SELECT * FROM $wpdb->posts $join $where ORDER BY $orderby $limit";
	$key = md5($query);
	$cache = wp_cache_get('padd_recent_posts','general');
	if (!isset($cache[ $key ])) {
		$arcresults = $wpdb->get_results($query);
		$cache[$key] = $arcresults;
		wp_cache_set('padd_recent_posts',$cache,'general');
	} else {
		$arcresults = $cache[$key];
	}
	if ($arcresults) {
		add_filter('excerpt_length', 'padd_theme_hook_excerpt_recent_posts_length');
		foreach ((array) $arcresults as $arcresult) {
			if ($arcresult->post_date != '0000-00-00 00:00:00' ) {
				$url = get_permalink($arcresult);
				$arc_title = $arcresult->post_title;
				if ($arc_title) {
					$text = strip_tags(apply_filters('the_title', $arc_title));
				} else {
					$text = $arcresult->ID;
				}
				$img = trim(get_the_post_thumbnail($arcresult->ID, PADD_THEME_SLUG . '-thumbnail-recent-posts'));
				$def = get_template_directory_uri() . '/images/thumbnail-recent-posts.png';
				if (empty($img)) {
					$img = '<img src="' . $def . '" title="Thumbnail" alt="Thumbnail" />';
				}
				$output .= '<li>';
				$output .= '<a href="' . get_permalink($arcresult->ID) . '" title="Permalink to ' . $text . '" class="thumbnail">' . $img . '</a>';
				$output .= '<a href="' . get_permalink($arcresult->ID) . '" title="Permalink to ' . $text . '" class="post-title">' . $text . '</a> ';
				$output .= '<span class="content">';
				$output .= get_the_excerpt();
				$output .= '</span>';
				$output .= '<span class="read-more"><a href="' . get_permalink($arcresult->ID) . '" title="Permalink to ' . $text . '" class="read-more">' . __('Read More', PADD_THEME_SLUG) . '</a></span>';
				$output .= '<div class="clear"></div>';
				$output .= '</li>';
			}
		}
		remove_filter('excerpt_length', 'padd_theme_hook_excerpt_recent_posts_length');
	}

	if ($echo) {
		echo $output;
	} else {
		return $output;
	}
}


/*
Plugin Name: Nice Search
Version: 0.3
Plugin URI: http://txfx.net/wordpress-plugins/nice-search/
Description: Redirects ?s=query searches to /search/query, and converts %20 to +
Author: Mark Jaquith
Author URI: http://coveredwebservices.com/
*/
if (!function_exists('cws_nice_search_redirect')) :
	function cws_nice_search_redirect() {
		if (is_search() && strpos($_SERVER['REQUEST_URI'], '/wp-admin/') === false && strpos($_SERVER['REQUEST_URI'], '/search/') === false) {
			wp_redirect(home_url('/search/' . str_replace(array(' ', '%20'),  array('+', '+'), get_query_var('s'))));
			exit();
		}
	}
	add_action('template_redirect', 'cws_nice_search_redirect');
endif;