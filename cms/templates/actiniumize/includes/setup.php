<?php

if (!function_exists('padd_theme_setup')) {
	function padd_theme_setup() {
		remove_action('wp_head','wp_generator');

		add_theme_support('post-thumbnails');
		add_theme_support('automatic-feed-links');

		load_theme_textdomain(PADD_THEME_SLUG, PADD_THEME_PATH . PADD_DS . 'languages' );

		$locale = get_locale();
		$locale_file = PADD_THEME_PATH . PADD_DS . 'languages' . PADD_DS . $locale . '.php';
		if (is_readable($locale_file)) {
			require_once($locale_file);
		}

		register_nav_menus(array(
			'primary'   => __('Main Menu', PADD_THEME_SLUG),
		));

		add_image_size(PADD_THEME_SLUG . '-thumbnail', PADD_LIST_THUMB_W, PADD_LIST_THUMB_H, true);
		add_image_size(PADD_THEME_SLUG . '-term'     , PADD_TERM_THUMB_W, PADD_TERM_THUMB_H, true);
		add_image_size(PADD_THEME_SLUG . '-slideshow', PADD_GALL_THUMB_W, PADD_GALL_THUMB_H, true);
		add_image_size(PADD_THEME_SLUG . '-recent'   , PADD_RECE_THUMB_W, PADD_RECE_THUMB_H, true);

		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui-tabs');
		wp_enqueue_script('frontend', get_stylesheet_directory_uri() . '/js/frontend.php', array('jquery'), PADD_THEME_VERS, true);
		wp_enqueue_style('google-fonts-1','http://fonts.googleapis.com/css?family=Podkova');
	}
}
add_action('after_setup_theme', 'padd_theme_setup');

function padd_theme_widgets_init() {
	register_sidebar(array(
		'name'          => __('Sidebar', PADD_THEME_SLUG),
	     'id'            => 'sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar(array(
		'name'          => __('Footer 1', PADD_THEME_SLUG),
		'id'            => 'footer-1',
		'description'   => __('Footer widget area at the first column.', PADD_THEME_SLUG),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="title">',
		'after_title'   => '</h2>',
	));
	register_sidebar(array(
		'name'          => __('Footer 2', PADD_THEME_SLUG),
		'id'            => 'footer-2',
		'description'   => __('Footer widget area at the second column.', PADD_THEME_SLUG),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="title">',
		'after_title'   => '</h2>',
	));
	register_sidebar(array(
		'name'          => __('Footer 3', PADD_THEME_SLUG),
		'id'            => 'footer-3',
		'description'   => __('Footer widget area at the third column.', PADD_THEME_SLUG),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="title">',
		'after_title'   => '</h2>',
	));
}
add_action('widgets_init', 'padd_theme_widgets_init');

function padd_theme_head_favicon() {
	$icon = Padd_Option::get('favicon_url','');
	if (!empty($icon)) {
		echo '<link rel="shortcut icon" href="' . $icon . '" />' . "\n";
		echo '<link rel="icon" href="' . $icon . '" />' . "\n";
	}
}
add_action('wp_head', 'padd_theme_head_favicon');

function padd_theme_head_tracker() {
	$tracker = Padd_Option::get('tracker_body','');
	if (!empty($tracker)) {
		echo stripslashes($tracker);
	}
}
add_action('wp_footer', 'padd_theme_head_tracker');

add_filter('excerpt_length', 'padd_theme_hook_excerpt_loop_length');
add_filter('excerpt_more', 'padd_theme_hook_excerpt_loop_more');
add_filter('get_comments_number', 'padd_theme_hook_count_comments',0);
add_filter('wp_page_menu_args', 'padd_theme_hook_menu_args');

function padd_theme_queue_js(){
	if (!is_admin()){
		if ( is_singular() && comments_open() && (get_option('thread_comments') == 1)) {
			wp_enqueue_script('comment-reply');
		}
	}
}
add_action('wp_print_scripts', 'padd_theme_queue_js');