<?php

require '../../../../wp-load.php';

if ('POST' != $_SERVER['REQUEST_METHOD']) {
	header('Allow: POST');
	header('HTTP/1.1 405 Method Not Allowed');
	header('Content-Type: text/plain');
	exit;
}

$name      = (isset($_POST['name']))  ? trim(strip_tags($_POST['name'])) : null;
$email     = (isset($_POST['email'])) ? trim($_POST['email']) : null;
$message   = ( isset($_POST['message']) ) ? trim($_POST['message']) : null;

$to = Padd_Option::get('mail_address', '');
if (empty($to)) {
	$to = get_option('admin_email');
}
$subject = Padd_Option::get('mail_subject', 'Contact Message');
$resp = null;

if (6 > strlen($email) || '' == $name) {
	wp_die(__('Error: please fill the required fields (name, email).', PADD_THEME_SLUG));
}
else if (!is_email($email)) {
	wp_die(__('Error: please enter a valid email address.', PADD_THEME_SLUG));
}
if ('' == $message) {
	wp_die(__('Error: please type a message.', PADD_THEME_SLUG));
}

$sent = wp_mail($to, $subject, $message);
$page_id = Padd_Option::get('thank_you_page_id', '1');
$thank_you_page = get_permalink($page_id);

if ($sent) {
	wp_redirect($thank_you_page);
} else {
	wp_die(__('Error: Sending mail failed.', PADD_THEME_SLUG));
}