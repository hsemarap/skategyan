<?php

require '../../../../wp-load.php';

$out = '';

if (function_exists('ob_start') && function_exists('ob_end_flush')) {
	ob_start();
}

if (file_exists('fonts.css')) {
	include 'fonts.css';
}

include 'base.css';
include 'required.css';
include 'layout.css';
include 'widgets.css';
include 'header.css';
include 'navigation.css';
include 'content.css';
include 'content-front-page.css';
include 'content-comments.css';
include 'pagination.css';
include 'sidebar.css';
include 'sidebar-fwa.css';
include 'footer.css';

?>

<?php $bg_color = Padd_Option::get('bg_color',''); ?>
body {
	<?php if (empty($bg_color)) : ?>
	background: transparent url('../images/bg.jpg') 0 0 repeat;
	<?php else : ?>
	background: <?php echo $bg_color; ?> none;
	<?php endif; ?>
}

<?php $sitename = Padd_Option::get('sitename_mode','1'); ?>
/** Branding **/
<?php if ('0' == $sitename) : ?>
#branding {
	margin: 0;
	padding: 0;
	float: left;
}
#branding #site-title {
	display: block;
	margin: 0 0 0 0;
	padding: 0;
}
#branding #site-title a {
	display: block;
	margin: 0;
	padding: 0;
	color: #fff;
	font: normal 52px/108px 'Podkova', serif;
	text-decoration: none
}
#branding #site-title a:hover {
	text-decoration: none;
}
<?php else : ?>
<?php
	$image = Padd_Option::get('sitename_logo_url');
	$val = getimagesize($image);
?>
#branding {
	margin: 0;
	padding: 0;
	float: left;
}
#branding #site-title {
	display: block;
	margin: 0;
	padding: 0;
}
#branding #site-title a {
	display: block;
	margin: 0;
	padding: 0;
	width: <?php echo $val[0]; ?>px;
	height: <?php echo $val[1]; ?>px;
	text-indent: -99999px;
	background: transparent url('<?php echo $image; ?>') left center no-repeat;
}
<?php endif; ?>

<?php
if (function_exists('ob_start') && function_exists('ob_end_flush')) {
	$out = ob_get_clean();
}

$compress = (isset($_GET['c']) && $_GET['c']);
$force_gzip = ($compress && 'gzip' == $_GET['c']);
$expires_offset = 31536000;

header('Content-Type: text/css');
header('Expires: ' . gmdate( "D, d M Y H:i:s", time() + $expires_offset ) . ' GMT');
header("Cache-Control: public, max-age=$expires_offset");

if ( $compress && ! ini_get('zlib.output_compression') && 'ob_gzhandler' != ini_get('output_handler') && isset($_SERVER['HTTP_ACCEPT_ENCODING']) ) {
	header('Vary: Accept-Encoding'); // Handle proxies
	if ( false !== strpos( strtolower($_SERVER['HTTP_ACCEPT_ENCODING']), 'deflate') && function_exists('gzdeflate') && ! $force_gzip ) {
		header('Content-Encoding: deflate');
		$out = gzdeflate( $out, 3 );
	} elseif ( false !== strpos( strtolower($_SERVER['HTTP_ACCEPT_ENCODING']), 'gzip') && function_exists('gzencode') ) {
		header('Content-Encoding: gzip');
		$out = gzencode( $out, 3 );
	}
}

echo $out;
