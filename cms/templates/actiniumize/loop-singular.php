<?php
	$title = the_title_attribute('echo=0');
	$title = !empty($title) ? $title : __('(untitled)', PADD_THEME_SLUG);
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
	<div class="entry-header clear-fix">
		<h2 class="entry-title">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', PADD_THEME_SLUG), $title); ?>">
				<?php
					$title = get_the_title();
					if (!empty($title)) {
						echo $title;
					} else {
						_e('(untitled)', PADD_THEME_SLUG);
					}
				?>
			</a>
		</h2>
		<?php if (!is_page()) : ?>
		<p class="meta"><?php the_time('F j, Y'); ?></p>
		<?php endif; ?>
	</div>
	<div class="entry-content">
		<?php the_content(); ?>
		<div class="clear"></div>
		<?php wp_link_pages(array('before' => '<p class="pages"><strong>' . __('Pages:', PADD_THEME_SLUG). '</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
	</div>
</div>

