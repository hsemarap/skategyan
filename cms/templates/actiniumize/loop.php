<?php
	$title = the_title_attribute('echo=0');
	$title = !empty($title) ? $title : __('(untitled)', PADD_THEME_SLUG);
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
	<div class="entry-thumbnail">
		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', PADD_THEME_SLUG), $title); ?>">
			<?php
				$padd_image_def = get_template_directory_uri() . '/images/thumbnail.png';
				if (has_post_thumbnail()) {
					the_post_thumbnail(PADD_THEME_SLUG . '-thumbnail');
				} else {
					echo '<img class="image-thumbnail" alt="Default thumbnail." src="' . $padd_image_def . '" />';
				}
			?>
		</a>
	</div>
	<div class="entry-header">
		<h2 class="entry-title">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', PADD_THEME_SLUG), $title); ?>">
				<?php
					$title = get_the_title();
					if (!empty($title)) {
						echo $title;
					} else {
						_e('(untitled)', PADD_THEME_SLUG);
					}
				?>
			</a>
		</h2>
		<?php $type = get_post_type(); ?>
		<?php if ('post' == $type) : ?>
		<p class="meta"><?php the_time('F j, Y'); ?></p>
		<?php endif; ?>
	</div>
	<div class="entry-excerpt">
		<?php the_excerpt();?>
		<p class="read-more"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', PADD_THEME_SLUG), $title); ?>"><?php _e('Continue Reading', PADD_THEME_SLUG); ?></a></p>
	</div>
	<div class="clear"></div>
</div>