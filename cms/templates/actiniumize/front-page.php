<?php get_header(); ?>

<div id="primary-full">
	<div id="content" role="main" class="clear-fix">


	<?php
		$project = Padd_Option::get('project_cat_id', '1');
		query_posts('cat=' . $project . '&posts_per_page=5');
		$cat = get_category($project);
	?>

	<div class="page-header">
		<h2 class="page-title"><?php echo $cat->name; ?></h2>
	</div>

	<?php if (!have_posts()) : ?>

	<p><?php echo __('There are no featured projects.', PADD_THEME_SLUG); ?></p></div>

	<?php else : ?>

		<?php
			$thumbnail_type = PADD_THEME_SLUG . '-term';
			$padd_image_def = get_template_directory_uri() . '/images/thumbnail.png';
			$i = 1;
		?>
			<ul id="featured-projects">
			<?php while (have_posts()) : ?>
				<?php the_post(); ?>
				<li id="entry-<?php the_ID(); ?>">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
						<?php
							if (has_post_thumbnail()) {
								the_post_thumbnail($thumbnail_type);
							} else {
								echo '<img class="image-thumbnail" alt="Default thumbnail." src="' . $padd_image_def . '" />';
							}
						?>
					</a>
				</li>
			<?php endwhile; ?>
			</ul>
	<?php endif;

		wp_reset_query();
	?>


	</div><!-- #content -->
</div><!-- #primary-full -->

<?php get_footer(); ?>