<?php

define('PADD_THEME_NAME'  , 'Actiniumize');
define('PADD_THEME_VERS'  , '1.2');
define('PADD_THEME_SLUG'  , 'actiniumize');
define('PADD_GALL_THUMB_W', 950);
define('PADD_GALL_THUMB_H', 300);
define('PADD_TERM_THUMB_W', 150);
define('PADD_TERM_THUMB_H', 130);
define('PADD_LIST_THUMB_W', 150);
define('PADD_LIST_THUMB_H', 150);
define('PADD_RECE_THUMB_W', 120);
define('PADD_RECE_THUMB_H', 104);
define('PADD_DS'          , DIRECTORY_SEPARATOR);
define('PADD_NS'          , 'padd');
define('PADD_FW'          , '5.1');

define('PADD_THEME_PATH', get_template_directory());
define('PADD_FUNCT_PATH', PADD_THEME_PATH . PADD_DS . 'includes' . PADD_DS);

require ABSPATH . 'wp-includes' . PADD_DS . 'class-feed.php';
require ABSPATH . 'wp-includes' . PADD_DS . 'class-simplepie.php';

require PADD_FUNCT_PATH . 'class-option.php';
require PADD_FUNCT_PATH . 'class-walkers.php';
require PADD_FUNCT_PATH . 'class-socialnetwork.php';
require PADD_FUNCT_PATH . 'class-socialbookmark.php';
require PADD_FUNCT_PATH . 'class-twitter.php';
require PADD_FUNCT_PATH . 'class-pagination.php';
require PADD_FUNCT_PATH . 'class-adminpanel.php';
require PADD_FUNCT_PATH . 'class-input.php';
require PADD_FUNCT_PATH . 'class-widgets.php';

require PADD_THEME_PATH . PADD_DS . 'administration' . PADD_DS . 'options-functions.php';

require PADD_FUNCT_PATH . 'defaults.php';
require PADD_FUNCT_PATH . 'functions.php';
require PADD_FUNCT_PATH . 'hooks.php';
require PADD_FUNCT_PATH . 'setup.php';
require PADD_FUNCT_PATH . 'prelude.php';
