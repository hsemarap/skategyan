
<div id="fwa-wrap">
	<div id="fwa">
		<?php
			$args = array(
				'before_title' => '<h2 class="widget-title">',
				'after_title'  => '</h2>'
			);
		?>
		<div id="fwa-1" class="fwa-bar">
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1')) : ?>
			<?php the_widget('WP_Widget_Links', null, $args); ?>
			<?php endif; ?>
		</div><!-- #fwa-1 -->

		<div id="fwa-2" class="fwa-bar">
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2')) : ?>
			<?php the_widget('Padd_Theme_Widget_SocialNetwork', null, $args); ?>
			<?php endif; ?>
		</div><!-- #fwa-2 -->

		<div id="fwa-3" class="fwa-bar">
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3')) : ?>
			<?php the_widget('Padd_Theme_Widget_Recent_Posts', null, $args); ?>
			<?php endif; ?>
		</div><!-- #fwa-3 -->
		<div class="clear"></div>

	</div><!-- #fwa -->
</div><!-- #fwa-wrap -->
