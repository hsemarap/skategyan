<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
<head>
    <title>Vortex 2013</title>
    	<link rel="shortcut icon" href="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/favicon.ico" >
<!--	<link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/main.css" /> -->
        <link href="<?php echo "$TEMPLATEBROWSERPATH";?>/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo "$TEMPLATEBROWSERPATH";?>/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo "$TEMPLATEBROWSERPATH";?>/css/docs.css" rel="stylesheet">
        <link href="<?php echo "$TEMPLATEBROWSERPATH";?>/css/ribbon.css" rel="stylesheet">
        <link href="<?php echo "$TEMPLATEBROWSERPATH";?>/css/blue.css" rel="stylesheet">      		
          <script src="<?php echo "$TEMPLATEBROWSERPATH";?>/js/jquery.min.js"></script>
		<script src="<?php echo "$TEMPLATEBROWSERPATH";?>/js/module.js"></script>
        	
                
     
    <meta name="description" content="Vortex 2013" />
    <meta name="keywords" content="Vortex 2013" /> 
	<meta http-equiv="Last-Update" content="2013-02-03" />
 <!--   <link rel="index" href="./" title="Home" />
	<link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/adminui.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/other.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/header.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/gallery.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/menu.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/content.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/footer.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/error.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/breadcrumb.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo "$TEMPLATEBROWSERPATH";?>/styles/actionbar.css" />
    -->
    <script language="javascript" type="text/javascript" src="<?php echo "$TEMPLATEBROWSERPATH";?>/scripts/jquery-latest.js" ></script>
    <script type="text/javascript" src="<?php echo "$TEMPLATEBROWSERPATH";?>/scripts/script.js"></script>

    <script language="javascript" type="text/javascript">
		//defined here for use in javascript
		var templateBrowserPath = "/13/cms/templates/vortex";
		var urlRequestRoot = "/13";
	</script>

</head>

<body onload="" >
          		
        <a href="http://www.facebook.com/vortex.nitt" target="_blank"><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/facebook.png" border="0" alt="facebook_image" title="Follow us on facebook" class="facebook"></a>  
        <a href="http://www.twitter.com/vortex_nitt" target="_blank"><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/twitter.png" border="0" alt="twitter_image" title="Follow us on twitter" class="twitter"></a> 		
		<a href="https://plus.google.com/112235029158960636893/posts" target="_blank"><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/google.png" border="0" alt="google+_image" title="Follow us on google+" class="google"></a> 	

	    <div class="container">
    	<div class="row">
        <!-- BEGIN LEFT BLOCK-->
            <div class="span3 left_block fixed">
            	<div>
                	                    
	                                     

	                       <div class="tsc_ribbon_wrap edge">
                                 <a href="./+login"><div class="ribbon-wrap left-edge fork lblue" title="Click here to sign in"><span>Sign in</span></div></a></div>
	                       <div class="tsc_ribbon_wrap edge">
                                  <a href="./+login&subaction=register"><div class="ribbon-wrap right-edge point lred" title="Click here to register"><span>Register</span></div></a></div>                                                                 

                                                						                                                                                                                                                		                                  </div>
            	<div class="logo_block">
                    <div class="logo">
                        <a href="" class="mylogo"><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/logo1.jpg" alt="Vortex 13" /></a>
						
                    </div>
            	</div>
                <!-- BEGIN MENU-->
               <div><ul id="" class="nav"><li id=""><a href="http://vortex.org.in/13/home/">Home</a>
</li>
<li id=""><a href="http://vortex.org.in/13/home/events">Events</a>
<ul class="sub-menu">
	<li id=""><a href="http://vortex.org.in/13/home/events/c_surf/">C-Surf</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/events/clueless/">Clueless</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/events/quiz/">Vortex Main Quiz</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/events/triathlon">Triathlon</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/events/hack_the_shell">Hack the Shell</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/events/pradarshan">Pradarshan</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/events/compose">Compose</a></li>
</ul>
</li>
<li id=""><a href="http://vortex.org.in/13/home/workshops">Workshops</a>
<ul class="sub-menu">
	<li id=""><a href="http://vortex.org.in/13/home/workshops/win8_dev">Windows 8 App Dev</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/workshops/android">Android Dev Fundamentals</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/workshops/linux_patch">Writing Linux Patches</a></li>
</ul>
</li>
<li id=""><a href="http://vortex.org.in/13/home/guest_lectures">Guest Lectures</a>
<ul class="sub-menu">
	<li id="" ><a href="http://vortex.org.in/13/home/guest_lectures/sriram">Sriram V Iyer</a></li>
	<li id=""><a href="http://vortex.org.in/13/home/guest_lectures/mrinal">Mrinal Kumar</a></li>
        <li id=""><a href="http://vortex.org.in/13/home/guest_lectures/rangan">C.Pandu Rangan</a></li>
</ul>
</li>
<li id=""><a href="http://vortex.org.in/13/home/contacts">Contacts</a></li>
</ul></div>                     

<!-- END MENU-->
                
            </div>
<!-- END LEFT BLOCK-->
					            <div class="span9 offset3">	            
<!-- BEGIN Main content-->
   
<div id="inf" class="row-fluid"><div class='span12'>
			 <link href="<?php echo "$TEMPLATEBROWSERPATH";?>/css/slider.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo "$TEMPLATEBROWSERPATH";?>/js/slider.js" type="text/javascript"></script>
<div id="slider">
    <a href="http://vortex.org.in/13/home/events"><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/1.png" alt="Events" /></a>
    <a href="http://vortex.org.in/13/home/workshops"><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/4.jpg" alt="Workshops"/></a>
    <a href="http://vortex.org.in/13/home/guest_lectures"><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/2.jpg" alt="Guest Lectures" /></a>
    <a href=""><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/7.jpg" alt="Contact Us" /></a>
    </div>

	</div></div>
       <section class="demo">        
            <ul id="newsticker_demo_reveal" class="newsticker">
                <li>Entries for Compose and Pradarshan are closed...</li>
                <li>Registrations for workshops have closed...</li>
                <li>Contact us at info@vortex.org.in for further details...</li>
                <li>Triathlon event has started..</li>
		<li>Clueless has started..</li>
		<li>Details for Windows 8 AppDev workshop has been updated..</li>
                <li>Event details have been updated...</li>          
            </ul>                     
        </section> 
<div class="row-fluid"><h3>Click <a href="/13/home/Schedule.pdf">here</a> to get the Schedule.</h3></div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like" data-href="http://www.facebook.com/vortex.nitt" data-send="false" data-width="450" data-show-faces="true" data-font="segoe ui"></div>

<section class="none"><div class="row-fluid">
	
<div class='span4'><div class='my_block'><h3><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/i2.png" width="33" height="32" class="left-block" /> Events</h3>
				<hr class="" />
	
<p style="font-size:11px;">Events are the heart of the symposium. This time at vortex we offer a various varieties of it...</p>
<p style="margin-bottom:0px"><a href="http://vortex.org.in/13/home/events/" class="read_more">Read more.</a></p>
<p></div></div>

<div class='span4'><div class='my_block'><h3><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/i1.png" width="33" height="32" class="left-block" />Workshops</h3>
				<hr class="" />
	
<p style="font-size:11px;">Ready to learn something new? Bored of listening to lectures? Need an ac hall to rest?... </p>
<p style="margin-bottom:0px"><a href="http://vortex.org.in/13/home/workshops/" class="read_more">Read more.</a></p>
<p></div></div>

<div class='span4'><div class='my_block'><h3><img src="<?php echo "$TEMPLATEBROWSERPATH";?>/css/img/i3.png" width="33" height="32" class="left-block" />Guest Lectures</h3>
				<hr class="" />
	
<p style="font-size:11px;">Most of our learning occurs outside class rooms, through experience, and the best of it...</p>
<p style="margin-bottom:0px"><a href="http://vortex.org.in/13/home/guest_lectures/" class="read_more">Read more.</a></p>
</div></div></div></section>

<section class="none"><div class="row-fluid"><div class="my_block"><h1><strong>About Us</strong></h1>
<p>The Computer Science and Engineering Association of National Institute of Technology, Trichy (formerly REC Trichy) hosts an annual national level technical symposium Vortex. It has been providing a platform for the budding software engineers from across the country to display their prowess in the ever changing field of Information Technology. Vortex has seen its moments of triumph and failure, but the quest for knowledge is conspicuously noticed every year with the best brains from more than 50 premier institutes from all over the country locking horns. A general panorama of events for the two day festival will include guest lectures by leading names in the academic arena as well as by eminent persons from the software industry to enlighten the students and unravel the corporate world and a wave of events like the paper presentation contest, mind boggling quiz session and a host of side events.</p>
</div>
</div></section>
<section class="none"><div class="row-fluid"></div></section>
  
<!-- END Main content -->

                  
<!-- BEGIN FOOTER block -->
 
               
                <footer class="footer">
                	<div class="row">
                    	<div class="span9">
                        	<hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span3">
                        	<p><strong>Vortex 13</strong> All Rights Reserved.</p>
                        </div>
                        <div class="span6">
                                                <div><ul id="" class="footer-menu unstyled">
						<li id=""><a href="http://vortex.org.in/13/home">Home</a></li>
                                                <li id=""><a href="http://vortex.org.in/13/home/events">Events</a></li>
                                                <li id=""><a href="http://vortex.org.in/13/home/workshops">Workshops</a></li>
                                                <li id=""><a href="http://vortex.org.in/13/home/guest_lectures">Guest Lectures</a></li>
                                                <li id=""><a href="http://vortex.org.in/13/home/contacts">Contacts</a></li>
</ul></div></div>
                    </div>
                </footer>
                
<!-- END FOOTER block-->   
            </div> 
                         
        </div>
    </div>
	

<!-- END-->
<script type="text/javascript">

var isExist=document.getElementsByClassName("cms-info");
if(isExist.length==1) {
    document.getElementById("inf").style.display="none";
}
</script>
     
	</body>     
</html>
