<?php
if(!defined('__PRAGYAN_CMS'))
{ 
	header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
	echo "<h1>403 Forbidden<h1><h4>You are not authorized to access the page.</h4>";
	echo '<hr/>'.$_SERVER['SERVER_SIGNATURE'];
	exit(1);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
<head>
    <title><?php $cmstitle=$TITLE;echo $cmstitle; ?></title>
    <?php if(isset($WIDGETS[0])) echo $WIDGETS[0]; ?>
	<link rel="shortcut icon" href="<?php echo $TEMPLATEBROWSERPATH; ?>/images/logo_16.png" >
	<link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/main.css" />
    <meta name="description" content="<?php echo $SITEDESCRIPTION ?>" />
    <meta name="keywords" content="<?php echo $SITEKEYWORDS ?>" /> 
    <meta name="google-translate-customization" content="fe9332fbd573b0fa-2a194b1073e71e1f-gadc675c5cdefbc02-e"></meta>
	<?php global $urlRequestRoot;	global $PAGELASTUPDATED;
	if($PAGELASTUPDATED!="")
		echo '<meta http-equiv="Last-Update" content="'.substr($PAGELASTUPDATED,0,10).'" />'."\n";
	?>
    <link rel="index" href="./" title="Home" />
    <link rel="stylesheet" type="text/css" href="<?php echo  $TEMPLATEBROWSERPATH; ?>/styles/camera.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo  $TEMPLATEBROWSERPATH; ?>/styles/adminui.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo  $TEMPLATEBROWSERPATH; ?>/styles/other.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/header.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/gallery.css" />
    <!--<link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/menu.css" />-->
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/content.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/footer.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/error.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/breadcrumb.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/actionbar.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/newstyles.css" />
    <script language="javascript" type="text/javascript" src="<?php echo  $TEMPLATEBROWSERPATH; ?>/scripts/jquery-latest.js" ></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/jquery.easing.1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/jquery.easing.compatibility.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/camera.min.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/script.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/new.js"></script>

    <script language="javascript" type="text/javascript">
		//defined here for use in javascript
		var templateBrowserPath = "<?php echo $TEMPLATEBROWSERPATH ?>";
		var urlRequestRoot = "<?php echo $urlRequestRoot?>";
		var action="<?php echo $_GET['action'] ?>";
	</script>

</head>

<body onload="<?php echo $STARTSCRIPTS; ?>" >
<script language="JavaScript">
TargetDate = "06/02/2014 5:00 AM";
BackColor = "palegreen";
ForeColor = "navy";
CountActive = true;
CountStepper = -1;
LeadingZero = true;
DisplayFormat = "Coundown: %%D%% Days, %%H%% Hours, %%M%% Minutes, %%S%% Seconds.";
FinishMessage = "Happy Bday!!It is finally here!";
</script><style>#cntdwn{position:absolute;}</style>
<script type='text/javascript' src='<?php echo $TEMPLATEBROWSERPATH;?>/scripts/countdown.js'></script>
<div class="outercontainer">
	<div class="clearer"></div>
	<div class="innercontainer">
		
		<div class="clearer"></div>
		<div class="header">
		<img src="<?php echo $TEMPLATEBROWSERPATH; ?>/images/pragyancmslogo.png" style="padding-top: 6px; padding-left: 10px;">
		<div id='header_text'>
			<?php echo $cmstitle;?>
		</div>

		<?php if(isset($WIDGETS[1])) echo $WIDGETS[1]; ?>		
		</div>

		<!-- breadcrumb starts-->
		<div class="breadcrumb">
			<div id="breadcrumb">
			<?php echo $BREADCRUMB; ?>
			</div>
		</div>
		<!-- breadcrumb ends-->
		<div class="clearer"></div>
		<div class="actionbarcontainer">
			<div class="actionbar">
			<div id='lava2' class='lava' data_left="57"></div>
			<?php echo $ACTIONBARMODULE; ?>
			<?php echo $ACTIONBARPAGE; ?>
			</div>
		</div>
		<div class="clearer">
		</div>
		<div class="contentcontainer">
			<div id="cms-leftcontent">
				<div class="menucontainer">
					<div id='lava' class='lava'></div>
					<?php echo $MENUBAR; ?>
					
					<?php if(isset($WIDGETS[4])) echo $WIDGETS[4]; ?>
				</div>
				
			</div>
			<div id="cms-content">
				<div class='goog-te-men2' style="width:100px">
				<a class="goog-te-menu2-item" class='.tamiltranslate' href="javascript:void(0)">
				<div style="white-space: nowrap;"><span class="indicator">›</span>
				<span class="text">Tamil</span></div></a>
				<a class="goog-te-menu2-item" class='.hinditranslate' href="javascript:void(0)">
				<div style="white-space: nowrap;"><span class="indicator">›</span>
				<span class="text">Hindi</span></div></a>
				</div>
				<div id="google_translate_element"></div>
				<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
				<?php echo $INFOSTRING; ?>
				<?php echo $WARNINGSTRING;?>
				<?php echo $ERRORSTRING; ?>
				<?php if(isset($WIDGETS[2])) echo $WIDGETS[2]; ?>
				<?php echo $CONTENT; ?>
				<?php if(isset($WIDGETS[3])) echo $WIDGETS[3]; ?>
				
		<p align='right'>		Developed by <a href='http://www.facebook.com/pbornskater'>Parameswaran</a> </p>
		<p align='center' style='position:relative;bottom:15px;' >
		<a class='push_button' onclick="PrintElem('#printcontent')">PRINT THIS PAGE</a>
		</p>
			</div>
			<div class="bottomcontentbar"></div>
		</div>
		<div class="clearer"></div>
		<div class="footer">
			
			<?php echo $FOOTER;?>	
		</div>
	</div>
</div>
</body>
<!-- Begin TranslateThis Button -->
<script type="text/javascript" src="http://x.translateth.is/translate-this.js"></script>
<script type="text/javascript">
try{
TranslateThis();
}catch(err){}
</script>

<!-- End TranslateThis Button -->
<script type="text/javascript">
 $(function(){
 	try{
       $.getScript('//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit');
   }catch(err){}
   });
</script>
</html>
