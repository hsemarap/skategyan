function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
   /* var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
   */
    return age;
}
function getAgeGroup(age){
	if(age<6)return "Below 6";
	if(age<8)return "6 to 8";
	if(age<10)return "8 to 10";
	if(age<12)return "10 to 12";
	if(age<14)return "12 to 14";
	if(age<16)return "14 to 16";
	return "Above 16";
}
function getAgeGroupNum(age){
	if(age<6)return "06";
	if(age<8)return "68";
	if(age<10)return "810";
	if(age<12)return "1012";
	if(age<14)return "1214";
	if(age<16)return "1416";
	return "16";
}
$(function(){

	var normalcmsht=$("#cms-content").height(),maxht=$("window").height();

	$(".menucontainer > a,.topnav > li > a").hover(function(){
		//$(this).stop().animate({"padding-bottom":"120px"});
		//$(this).find(".subtext").css("display","inline");
	},function(){
		//$(this).stop().animate({"padding-bottom":"20px"});
		//$(this).find(".subtext").css("display","inline");
	});

	var mini_pos=$("#cms-content").css("position");
	var mini_top=$("#cms-content").css("top");
	var mini_left=$("#cms-content").css("left");
	var mini_width=$("#cms-content").css("width");
	$("#maximize").mouseup(function(){
		if($(this).attr("state")=="min")
			{
				$(this).attr("state","max");
				$(this).html("Minimize");
				$("#cms-content").css({visibility:"hidden",position:"static"});
				var end=$("#cms-content").position();
				var margin=$("#cms-content").css("margin-left");
				//$("#print_button").css({position:"absolute",bottom:"-200px",margin:"0 auto"});
				$("#cms-content").css({position:"absolute"}).css({top:end.top,left:margin}).css({visibility:"visible"});
				$("#cms-content").animate({
					top:'20px',
					left:'0px',
					width:'100%'
				});
				$(window).scrollTop(0);
				//$("#cms-content").addClass("fullscreendiv");
			}else{
				$(this).attr("state","min");
				$(this).html("Maximize");
				$("#cms-content").css({visibility:"hidden",position:"static"});
				var end=$("#cms-content").position();
				$("#cms-content").css({position:"relative"}).animate({top:end.top}).css({visibility:"visible"});
				$("#cms-content").animate({
					top:mini_top,
					left:mini_left,
					width:mini_width
				});
				$(window).scrollTop($("#cms-content").position().top-200);
				//$("#cms-content").removeClass("fullscreendiv");
			}

	});

	try{ 	$("#news_container > div > ul").ticker({
			speed: 0.10,        
	        ajaxFeed: false,    
	        feedUrl: false,   
	        feedType: 'xml',  
	        htmlFeed: true,   
	        debugMode: true,  
	        controls: true,   
	        titleText: 'Updates',  
	        displayType: 'reveal',
	        direction: 'ltr',     
	        pauseOnItems: 2000,   
	        fadeInSpeed: 600,     
	        fadeOutSpeed: 300
			 });
	   }catch(err){}
	//try{
		var loginlink=$(".cms-actionlogin").attr('href');
		
		if($('.cms-actionlogin').length == 1 && $('.cms-actionlogout').length == 0)
		if($('.topnav.depth1').length)
		$(".topnav.depth1").append("<li ><a href='"+loginlink+"' class='toplogin'><div class='cms-menuitem'>Login</div></a></li>");
		else $(".menucontainer").append("<li style='list-style:none'><a href='"+loginlink+"' class='toplogin cms-menuitem'>Login</a></li>");
		var loginlink=$(".cms-actionlogout").detach();
		$(".menucontainer").append(loginlink);
	//}catch(err){}
	try{
	/** Image Slider Effects */
if($(".sliderimage").length){
    $(".sliderimage").each(function(){
		var imgdiv=$(this);
		$(this).detach();
		$("#orbit-slider").append($(this).html());
	});
	
    $('#orbit-slider').camera({
	thumbnails: true,
		//height:'auto',
		//height:'40%',
       //height:parseInt($("body").height()*28.5/100)+'px',
	//fx:'scrollRight,simpleFade,scrollLeft',
	portrait:false,
	height:'50%',
	//fx:'random',
	fx:'curtainTopLeft,scrollBottom,scrollRight,curtainSliceRight',
	hover:true,
	thumbnails:true,
	overlayer: true,
	barPosition:'right'
    });
    setTimeout(function(){
    //console.log($('#orbit-slider').position().top+" "+$('#orbit-slider').css("height"));
    $(".site-content").css("margin-top",parseInt($('#orbit-slider').css("height"))+$('#orbit-slider').position().top-40+'px');
	},100);    
	}else $("#orbit-slider").css("display","none");
    }catch(err){}
    try{

	$('.cms-menuhead').append('<span class="subtext"><br/>Home page</span>');
	$('div.cms-menuitem:nth-of-type(1)').append('<span class="subtext"><br/>The Application</span>');
	$('div.cms-menuitem:nth-of-type(2)').append('<span class="subtext"><br/>To Submit Club List</span>');

	$('#menubar > h2').append('<span class="subtext">The Home page</span>');
	$('#menubar > ul > li ')[0].append('<span class="subtext">The Web Application</span>');
	$('#menubar > ul > li ')[1].append('<span class="subtext">To Submit Club List</span>');
	$('#menubar > ul > li ')[2].append('<span class="subtext">Test Page</span>');
	}catch(err){}
	try{
	$("#lava").animate({
			left:($(".cms-menuhead,#menubar > h2").position().left),
			width:(parseInt($(".cms-menuhead,#menubar > h2").width()))+'px'
		});
	}catch(err){}
	$('#menubar > ul > li,.topnav > li,.cms-menuhead,#menubar > h2').hover(function(){
		try{
		$("#lava").stop().	animate({
			left:($(this).position().left),
			width:(parseInt($(this).width()))+'px'
		});
		}catch(err){}
		//$(this).find("div.cms-menuhead").css("color","rgb(156,9,9)");
		//$(this).find(".cms-menuitem").css("color","rgb(156,9,9)");
	},function(){
		//$(this).find("div.cms-menuhead,.cms-menuitem").css("color","#ccc");
	});

	var lava2left=$('.cms-actionbarModuleItem,.cms-actionbarPageItem').first().position().left;
	var lava2width=$('.cms-actionbarModuleItem,.cms-actionbarPageItem').first().width();
	if(action=="")action="view";
		$('.cms-actionbarModuleItem > a,.cms-actionbarPageItem > a').each(function(){
			if($(this).attr("href")=="./+"+action){
			lava2left=$(this).position().left;
			lava2width=$(this).width()+10;
			}
		});

	//lava2left-=10;
	lava2width+=10;
	$("#lava2").css("left",lava2left);
	$('.cms-actionbarModuleItem,.cms-actionbarPageItem').hover(function(){
	//	console.log($(this).position().left);
		$("#lava2").stop().	animate({
			left:(parseInt($(this).position().left)+10)+"px",
			width:(parseInt($(this).width())-10)+'px'
		});
	$('.actionbar').mouseleave(function(){
			//console.log(lava2left+" "+lava2width);
			$("#lava2").stop().animate({
			left:(lava2left)+'px',
			width:(lava2width)+'px'
		});
			//console.log(lava2left+" "+lava2width);
		});
	});

	$("td > input").focusin(function(){
		var td=$(this).parent();
		//console.log(td.index());
		td.closest('table').find('th').eq(td.index()).css("background","#D3D6FF");
		console.log(td.parent().parent().children('td').eq(td.index()));
		//.css("background","#D3D6FF");
		
	});

	// Code to get corresponding header
	//var th = td.closest('table').find('th').eq(td.index());

});
function PrintElem(elem)
    {
    	if($(elem).length==0)elem=$('.printcontent')
    	$('.fg-toolbar.ui-widget-header.ui-corner-tl.ui-corner-tr.ui-helper-clearfix',elem).css('display','none');
    	$('.fg-toolbar.ui-widget-header.ui-corner-bl.ui-corner-br.ui-helper-clearfix',elem).css('display','none');
 		
 		$('.nospan',elem).each(function(){
					$(this).replaceWith($(this).html());
					});
    	$('table',elem).each(function(){
    		$(this).attr('border','2px');
    	});
    	$('input',elem).each(function(){
    		var check=($(this).is(':checked')?'&#9745;':'&#9744;');
    		if($(this).hasClass('blankcheckbox'))
    		check=(($(this).is(':checked'))?'x':' ');
    		if($(this).is(':checkbox'))
    		$(this,elem).replaceWith('<span class=\"'+this.className.split(/\s+/)+'\">'+check+'</span>');	
    		else if($(this).hasClass('hidethis'))$(this,elem).replaceWith('<span style=\"display:none\" class=\"'+this.className.split(/\s+/)+'\">'+this.value+'</span>');
    		else 
    		$(this,elem).replaceWith('<span class=\"'+this.className.split(/\s+/)+'\">'+this.value+'</span>');
    		//console.log(this.value);
    	});

        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'PRINT', 'height=600,width=1000');
        mywindow.document.write('<html><head><title>PRINT</title>');
        /*optional stylesheet*/ 
        mywindow.document.write('<link rel=\"stylesheet\" href=\"$basefolder/cms/modules/tournament/main.css\" type=\"text/css\" />');
        
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<script>$(\"table\").each(function(){this.attr(\"border\",\"1px\");});&lt/script>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        //mywindow.print();
        //mywindow.close();

        return true;
    }