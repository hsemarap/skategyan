<?php
if(!defined('__PRAGYAN_CMS'))
{ 
	header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
	echo "<h1>403 Forbidden<h1><h4>You are not authorized to access the page.</h4>";
	echo '<hr/>'.$_SERVER['SERVER_SIGNATURE'];
	exit(1);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
<head>
    <title><?php $cmstitle=$TITLE;echo $cmstitle; ?></title>
    <?php if(isset($WIDGETS[0])) echo $WIDGETS[0]; ?>
	<link rel="shortcut icon" href="<?php echo $TEMPLATEBROWSERPATH; ?>/images/logo_16.png" >
	<link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/main.css" />
    <meta name="description" content="<?php echo $SITEDESCRIPTION ?>" />
    <meta name="keywords" content="<?php echo $SITEKEYWORDS ?>" /> 
    <meta name="google-translate-customization" content="fe9332fbd573b0fa-2a194b1073e71e1f-gadc675c5cdefbc02-e"></meta>
	<?php global $urlRequestRoot;	global $PAGELASTUPDATED;
	if($PAGELASTUPDATED!="")
		echo '<meta http-equiv="Last-Update" content="'.substr($PAGELASTUPDATED,0,10).'" />'."\n";
	?>
    <link rel="index" href="./" title="Home" />
    <link rel="stylesheet" type="text/css" href="<?php echo  $TEMPLATEBROWSERPATH; ?>/styles/camera.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo  $TEMPLATEBROWSERPATH; ?>/styles/adminui.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo  $TEMPLATEBROWSERPATH; ?>/styles/other.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/header.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/gallery.css" />
    <!--<link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/menu.css" />-->
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/content.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/footer.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/error.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/breadcrumb.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/ticker-style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/actionbar.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $TEMPLATEBROWSERPATH; ?>/styles/newstyles.css" />
    <script language="javascript" type="text/javascript" src="<?php echo  $TEMPLATEBROWSERPATH; ?>/scripts/jquery-latest.js" ></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/jquery.easing.1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/jquery.easing.compatibility.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/camera.min.js"></script>
	<script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/jquery.leanModal.min.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/jquery.ticker.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/script.js"></script>
    <script type="text/javascript" src="<?php echo $TEMPLATEBROWSERPATH; ?>/scripts/new.js"></script>


    <script language="javascript" type="text/javascript">
		//defined here for use in javascript
		var templateBrowserPath = "<?php echo $TEMPLATEBROWSERPATH ?>";
		var urlRequestRoot = "<?php echo $urlRequestRoot?>";
		var action="<?php echo $_GET['action'] ?>";
	</script>

</head>

<body onload="<?php echo $STARTSCRIPTS; ?>" >
<script language="JavaScript">
TargetDate = "06/02/2014 5:00 AM";
BackColor = "palegreen";
ForeColor = "navy";
CountActive = true;
CountStepper = -1;
LeadingZero = true;
DisplayFormat = "Coundown: %%D%% Days, %%H%% Hours, %%M%% Minutes, %%S%% Seconds.";
FinishMessage = "Happy Bday!!It is finally here!";
</script>
<style>#cntdwn{position:absolute;top:0px;}</style>
<!--
<script type='text/javascript' src='<?php echo $TEMPLATEBROWSERPATH;?>/scripts/countdown.js'></script>
-->
<div class="outercontainer col-12 centerme" style='width:100%'>
	<div class="clearer"></div>
	<div class="innercontainer col-12 centerme">
		<div id='header' class='centerme' style='width:100%'>
		<div id='top-social' class='col-12'>
			<div class="pull-left top-contact">Call Us: 9444 905 298 - Mail:
				<a href="mailto:hsemarap@gmail.com">hsemarap@gmail.com</a>
			</div>
			<div class="pull-right icons-small">
				<ul class='no-style li-inline'>
					<li class="small-twitter">
						<a href="#" target="_blank" title="Twitter">Twitter</a>
					</li>
					<li class="small-dribbble">
						<a href="#" target="_blank" title="Dribbble">Dribbble</a>
					</li>
					<li class="small-flickr">
						<a href="#" target="_blank" title="Flickr">Flickr</a>
					</li>
					<li class="small-facebook">
						<a href="#" target="_blank" title="Facebook">Facebook</a>.
					</li>
					<li class="small-google">
						<a href="#" target="_blank" title="Google+">Google+</a>
					</li>
					<li class="small-linkedin">
						<a href="#" target="_blank" title="LinkedIn">LinkedIn</a>
					</li>
					<li class="small-yahoo">
						<a href="#" target="_blank" title="Yahoo">Yahoo</a>
					</li>
					<li class="small-youtube">
						<a href="#" target="_blank" title="YouTube">YouTube</a>
					</li>
					<li class="small-pinterest">
						<a href="#" target="_blank" title="Pinterest">Pinterest</a>
					</li>
					<li class="small-rss">
						<a href="#" target="_blank" title="RSS">RSS</a>
					</li>
				</ul>			
			</div>

		</div>
		<div id='header_container' float='center' class='col-10 centerme'> 
			<nav><a href="/skate/" id="site-logo">
				<img style="height:70px" src="<?php echo $TEMPLATEBROWSERPATH; ?>/images/skategyan_logo.png" alt=""></img>
			</a>
			<h1 id='site-title'><?php echo $cmstitle;?></h1>
			<h2 id="site-slogan"></h2>
			<div class="menucontainer">
					<!--<div id='lava' class='lava'></div>-->
					<?php echo $MENUBAR; ?>
					<?php if(isset($WIDGETS[4])) echo $WIDGETS[4]; ?>
			</div>
			</div>
			</nav>
		<div class='header-shadow'></div>
		</div>
		
		<div class='image-slider col-12 centerme' style='margin-left:2.5%;width:95%;' id='orbit-slider'></div>
		<div class="breadcrumb" style='position:absolute;margin-top:5px;'>
			<div id="breadcrumb" class='col-12'>
			<?php echo $BREADCRUMB; ?>
			</div>
		</div>

		<div class='site-content' class='col-12'>

		
		<div class='col-11 new-update centerme'>
					<div id='widgets'>	<?php if(isset($WIDGETS[1])) echo $WIDGETS[1]; ?>	</div>
		</div>
			<div class="actionbarcontainer">
			<div class="actionbar" style='position:relative;margin-bottom:18px;right:5%;'>
			<div id='lava2' class='lava' data_left="57"></div>
			<?php echo $ACTIONBARMODULE; ?>
			<?php echo $ACTIONBARPAGE; ?>
			</div>
			</div>
			<div class='clearer'></div>

		<div id="cms-content" class='col-12'>
			<div class='clearer'></div>
			<!--
				<span style='float:left'>TRANSLATE :</span><div id="google_translate_element" style='display:inline;float:left'></div><script type="text/javascript">function googleTranslateElementInit() {  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');}</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
			-->
				<div id='maximize' state='min' class='push_button small' style='float:right;border:1px solid black;position:relative;margin-top:30px;'>Maximize</div>
			
				<script>
				$(function(){
				$(".cms-actionlogin").click(function(){return false;});
				$(".cms-actionlogin").attr("href","#loginform");
				$(".cms-actionlogin").leanModal({overlay : 0.4, closeButton: ".modal_close"});
				$(".toplogin").click(function(){return false;});
				$(".toplogin").attr("href","#loginform");
				$(".toplogin").leanModal({overlay : 0.4, closeButton: ".modal_close"});
				});</script>
				<div id="loginform" style="display:none; position: fixed; opacity: 1; z-index: 11000; left: 50%; margin-left: -202px; top: 200px;">
							<div id="signup-ct">
								<div id="signup-header">
									<p>Hello There</p>
									<h2>Log In</h2>
									<a class="modal_close" href="#"></a>
								</div>
								<form action="./+login" method="POST">
								  <div class="txt-fld">
								    <label for="">Email</label>
								    <input id="" class="good_input" name="user_email" type="text">
								  </div>
								  <div class="txt-fld">
								    <label for="">Password</label>
								    <input id="" name="user_password" type="password">
								  </div>
								  <div class="btn-fld">
								  <button type="submit">Log In</button>
								  </div>
								 </form>
							</div>
				</div>		
				<div class='printcontent'>		
				<?php echo $INFOSTRING; ?>
				<?php echo $WARNINGSTRING;?>
				<?php echo $ERRORSTRING; ?>
				<?php if(isset($WIDGETS[2])) echo $WIDGETS[2]; ?>
				<?php echo $CONTENT; ?>
				<?php if(isset($WIDGETS[3])) echo $WIDGETS[3]; ?>

				<p align='right'>		Developed by <a href='http://www.facebook.com/pbornskater'>Parameswaran</a> </p>
		<p align='center' id='print_button' style='position:relative;bottom:15px;' >
		<a class='push_button' onmouseup="PrintElem('#printcontent')">PRINT THIS PAGE</a>
		</p>
				</div>
		</div>


		</div>
		<div class="footer">
			
			<?php echo $FOOTER;?>	
		</div>
	</div>
</div>
</body>
<!-- Begin TranslateThis Button -
<script type="text/javascript" src="x.translateth.is/translate-this.js"></script>
<script type="text/javascript">
try{
TranslateThis();
}catch(err){}
</script>

- End TranslateThis Button -->
<script type="text/javascript">
 $(function(){
 	try{
setTimout(function(){$.getScript('//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit');
   },5000);
   }catch(err){}
   });
</script>
</html>
