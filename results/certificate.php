<?php 
require('fpdf/fpdf.php'); 
//create a FPDF object
$pdf=new FPDF();
$filename=$_GET['filename'];

//set document properties
$pdf->SetAuthor('Parameswaran');
$pdf->SetTitle('Kanchipuram District Roller Skating Championship');

//set font for the entire document
$pdf->SetFont('Times','B',20);
$pdf->SetTextColor(50,60,100);

//set up a page
$pdf->AddPage('P'); 
$pdf->SetDisplayMode(real,'default');

//insert an image and make it a link
//$pdf->Image('logo.png',10,20,33,0,' ','http://www.fpdf.org/');

//display the title with a border around it
$pdf->SetXY(50,20);
//$pdf->SetDrawColor(50,60,100);
//$pdf->Cell(100,10,'hello',1,0,'C',0);

$pdf->Write(5,'Congratulations!');

//Set x and y position for the main text, reduce font size and write content
$pdf->AddPage();
$pdf->SetXY (10,50);
$pdf->SetFontSize(10);
$pdf->Write(5,'Congratulations! You have generated a PDF.');
$pdf->SetAutoPageBreak("false",00);
//Output the document
//$pdf->Output("$filename.pdf",'F');      // change to this to save file
 $pdf->Output("certificate/$filename.pdf",'I');
?>
